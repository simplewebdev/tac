import Cosmic from 'cosmicjs'
const api = Cosmic();

import getConfig from 'next/config'

const { publicRuntimeConfig } = getConfig();
const { WRITE_KEY } = publicRuntimeConfig;

// const bucket = api.bucket({
//   slug: process.env.COSMIC_BUCKET || 'tac',
//   read_key: process.env.COSMIC_READ_KEY,
//   write_key: WRITE_KEY
// });

const bucket = api.bucket({
  slug: process.env.COSMIC_BUCKET || 'tac',
  read_key: process.env.COSMIC_READ_KEY,
  write_key: "cIUbs0H3RnH3wAHlB2yX844i8x4ca6g49jUo6pJKu9FVve6Nrk"
});

export default bucket