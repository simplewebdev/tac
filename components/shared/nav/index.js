import { Link } from '../../../routes'
import React from 'react';
import { Element } from 'react-scroll';
import RequestQuote from '../../../components/views/request-quote-modal';
import Post from '../../../utils/post';
import Router from 'next/router';



export default class Nav extends React.Component {
  constructor(props) {
    super(props);
    this.requestRef = React.createRef();
    this.state = {
      firstName: "",
      lastName: "",
      company: "",
      states: "",
      email: "",
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalState: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalCaptcha: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    };
    this.handleOnClickArrow = this.handleOnClickArrow.bind(this);
  }

  componentDidMount() {
    if (/request-a-quote/.test(window.location.href)) {
      this.setState({ modalOpen: true });
    }
  }

  resetForm = () => {
    this.setState({
      firstName: "",
      lastName: "",
      company: "",
      email: "",
    })
  };

  handleFirstNameChange = (event) => {
    this.setState({ firstName: event.target.value });
  };

  handleLastNameChange = (event) => {
    this.setState({ lastName: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handleCompanyChange = (event) => {
    this.setState({ company: event.target.value });
  };
  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalState: "",
      modalPhone: "",
      modalComments: "",
    })
  };
  handleModalSubmit = (event) => {
    this.setState({ modalSubmitting: true });
    event.preventDefault();
    fetch('/api/energy-quote-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.modalEmail,
        name: this.state.modalName,
        phone: this.state.modalPhone,
        company: this.state.modalCompany,
        states: this.state.modalState,
        message: this.state.modalComments
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'Energy Quote Request - Main Page';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Company: ${this.state.modalCompany}</b><br>`
          + `<b>State: ${this.state.modalState}</b><br>`
          + `<b>Sent to: marketing@tacenergy.com, sales@tacenergy.com</b><br>` +
          `</div>`;
        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({ modalSubmitted: true });
      } else {
        this.setState({ modalSubmitted: false, modalSubmitting: false, modalFailedSumbit: true });
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({ modalName: event.target.value });
  };

  handleModalPhoneChange = (event) => {
    this.setState({ modalPhone: event.target.value });
  };

  handleModalEmailChange = (event) => {
    this.setState({ modalEmail: event.target.value });
  };

  handleModalCompanyChange = (event) => {
    this.setState({ modalCompany: event.target.value });
  };

  handleModalStateChange = (event) => {
    this.setState({ modalState: event.target.value });
  };

  handleModalCommentsChange = (event) => {
    this.setState({ modalComments: event.target.value });
  };

  handleModalCaptchaChange = (value) => {
    this.setState({ modalCaptcha: value });
  };

  handleOnClickArrow() {
    this.setState({ modalOpen: true });
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false,
      modalOpen: !this.state.modalOpen
    });
    //Router.push('/tac-energy');
  };
  render() {
    return (
      <>
        <ul className="menu-list accordion fixed">
          <li id="nav1" className="toggle accordion-toggle">
            <span className="icon-plus" />
            <a className="menu-link" href="/">TAC - The Arnold Companies</a>
          </li>
          <ul className="menu-submenu accordion-content">
            <li><a className="head" href="/about">About</a></li>
            <li><a className="head" href="/news-and-views">News & Views</a></li>
            <li><a className="head" href="/social">Social</a></li>
            <li><a className="head" target="_blank" href="http://taccareers.com/">Careers</a></li>
            <li><a className="head" href="/contact-us">Contact Us</a></li>
          </ul>
          <li id="nav2" className="toggle accordion-toggle">
            <span className="icon-plus" />
            <Link route="tac-energy"><a className="menu-link">TAC Energy</a></Link>
          </li>
          <ul className="menu-submenu accordion-content">
            <li><a href="/tac-energy#supply" className="head">Supply</a></li>
            <li><a className="head" href="/tac-energy#products-services">Products & Services</a></li>
            <li><a className="head" target="_blank" href="https://portal.tacenergy.com/app/index.html?page=page_login">Customer Login</a></li>
            <li><a className='head' target="_blank" href="https://cus.bectran.com/customer/MySecuredLogin?zid=DpdMB315G3Y1M388j6pfb">Apply For Credit</a></li>
            <li><a className="head" href="/tac-energy#market-talk-updates">Market Talk Updates</a></li>
            {/* <li><a className="head" href="/tac-energy#request-a-quote" onClick={e => location.reload()}>Request A Quote</a></li> */}
            <li><a className="head" onClick={this.handleOnClickArrow}>Request A Quote</a></li>
            <li><a className="head" href="/tac-energy/contacts">Contacts</a></li>
          </ul>
          <li id="nav3" className="toggle accordion-toggle">
            <span className="icon-plus" />
            <Link route="tac-air"><a className="menu-link">TAC Air</a></Link>
          </li>
          <ul className="menu-submenu accordion-content">
            <li><a className="head" href="/tac-air/locations">Locations</a></li>
            <li><a className="head" href="/tac-air#services">Services</a></li>
            <li><a className="head" href="/tac-air#air-get-in-touch">Send Comments</a></li>
          </ul>
          <li id="nav4" className="toggle accordion-toggle">
            <span className="icon-plus" />
            <Link route="keystone-aviation"><a className="menu-link">Keystone Aviation</a></Link>
          </li>
          <ul className="menu-submenu accordion-content">
            <li><a className="head" href="/keystone-aviation/charter-service">Charter</a></li>
            <li><a className="head" href="/keystone-aviation/management">Management</a></li>
            <li><a className="head" href="/keystone-aviation/maintenance">Maintenance</a></li>
            {/* <li><a className="head" href="/keystone-aviation/sales-and-brokerage">Sales & Brokerage</a></li> */}
            {/* <li><a className="head" href="/keystone-aviation/aircrafts-for-sale">Aircraft for Sale</a></li> */}
            <li><a className="head" href="/keystone-aviation/contacts">Contacts</a></li>
          </ul>
          <li id="nav5" className="toggle accordion-toggle">
            <span className="icon-plus" />
            <Link route="tac-investments"><a className="menu-link">TAC Investments</a></Link>
          </li>
          <ul className="menu-submenu accordion-content">
            <li><a className="head" href="/tac-investments">About</a></li>
          </ul>
          <li id="nav5" className="toggle accordion-toggle">
            <span className="icon-plus" />
            <a href="https://taccareers.com/" target="_blank" className="menu-link">Careers</a>
          </li>
        </ul>
        <RequestQuote
          modalOpen={this.state.modalOpen}
          onModalClose={this.handleModalClose.bind(this)}
          emailValue={this.state.modalEmail}
          handleSubmit={this.handleModalSubmit.bind(this)}
          onEmailChange={this.handleModalEmailChange.bind(this)}
          NameValue={this.state.modalName}
          onNameChange={this.handleModalNameChange.bind(this)}
          phoneValue={this.state.modalPhone}
          onPhoneChange={this.handleModalPhoneChange.bind(this)}
          companyValue={this.state.modalCompany}
          onCompanyChange={this.handleModalCompanyChange.bind(this)}

          stateValue={this.state.modalState}
          onStateChange={this.handleModalStateChange.bind(this)}

          commentsValue={this.state.modalComments}
          onCommentsChange={this.handleModalCommentsChange.bind(this)}
          submitted={this.state.modalSubmitted}
          submitting={this.state.modalSubmitting}
          failedToSumbit={this.state.modalFailedSubmit}
          onCaptchaChange={this.handleModalCaptchaChange.bind(this)}
        />
      </>
    )
  }
}
