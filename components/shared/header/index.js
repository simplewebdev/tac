import Nav from '../nav';
import React from 'react';
import { Link } from '../../../routes';
import Head from 'next/head';
import SVG from 'react-inlinesvg';
const nextRoutes = require('next-routes');
const routes = nextRoutes();
export default class Index extends React.Component {
  getRoutes() {
    let qoutes;
    if (routes.Router.router) {
      if (routes.Router.router.route) {
       // console.log("yessss" + routes.Router.router.route)
        
        switch (routes.Router.router.route) {
          case "/ipcnowtacenergy":
            qoutes = "default";
            break;
            default:
              qoutes = "TAC";
        }
        console.log(qoutes)
        return qoutes;
      } else {
      console.log("nooooo")
      }
    } else {
      qoutes = "investmentss";
      console.log("no")
      //console.log(qoutes)
      return qoutes;
    }
  }

  render() {
    const { logo, favicon, logoHeight, company } = this.props;
    return (
      
      <div className="header">
        <Head>
          <link rel="icon" href={favicon ? favicon : 'https://cosmic-s3.imgix.net/fe5ad3f0-a232-11e8-8bed-9d1c5ce694f3-TAC_A.png?w=250'} type="image/png" />
          
          {
            this.getRoutes() ==="default" ? <meta name="robots" content="noindex" />
            : ""
          }
        </Head>
        <div style={{ minHeight: 100 }} className="header-inner header-container">
          <a href={`/${company}`}>
            <img
              style={{ paddingTop: 5, cursor: 'pointer' }}
              src={logo ? logo : 'https://cosmic-s3.imgix.net/30210740-966e-11e8-ab08-31f1ed4cd97d-tac-full-logo.svg?w=250'}
              alt=""
              height={logoHeight ? logoHeight : "50"}
            />
          </a>
          <div className={"menu"}>
            <div id="menu-container">
              <div id="menu-wrapper">
                <div id="hamburger-menu"><span /><span /><span /></div>
              </div>
              <Nav />
            </div>
            {company === 'tac-energy' &&
              <a className={"button customer-login"} href="https://portal.tacenergy.com/app/index.html?page=page_login" target="_blank">
                <i className={"login-icon"}>
                  <SVG src="../static/images/login.svg" />
                </i>
                <span className={"customer-login-text"}>
                  <em>Customer</em>
                  <strong>LOGIN</strong>
                </span>
              </a>
            }
          </div>
        </div>
      </div>
    )
  }
}