import React from 'react';
import Request from "../../../utils/request";
import { withRouter } from 'next/router';
import _ from 'lodash';

export default class Index extends React.Component 
{
	constructor(props)
	{
		super(props);
	}
	
	
	
	render() 
	{
    const { posts, archives, pageName, functionCheck } = this.props;

    const archivesItems = () => {
      return _.map(archives, (item, idx) => {
        return (
          <li key={idx} style={{padding: 5}}>
            <a href={`/news-and-views/archive/${item.year}/${item.month.toLowerCase()}`}>
              {item.month} {item.year}
            </a>
          </li>
        );
      })
    }

    let currDate = new Date();
    let currMonth = currDate.getMonth();
    let currYear = currDate.getFullYear();
    let postLength = posts.length;
    
    
    const hrefElements = (
		  <div>
			<a className="button-cat" style={this.props.pageName == "newsroom"?{background:"#0b559c"}:{}} 
            onClick={() => functionCheck("newsroom")}>View All</a>
            <a className="button-cat" style={this.props.pageName == "tac-the-arnold-companies-news"?{background:"#0b559c"}:{}} 
            onClick={() => functionCheck("tac-the-arnold-companies-news")}>TAC - The Arnold Companies</a>
            <a className="button-cat" style={this.props.pageName == "tac-energy-news"?{background:"#0b559c"}:{}} 
            onClick={() => functionCheck("tac-energy-news")}>TACenergy</a>
            <a className="button-cat" style={this.props.pageName == "tac-air-news"?{background:"#0b559c"}:{}} 
            onClick={() => functionCheck("tac-air-news")}>TAC Air</a>
            <a className="button-cat" style={this.props.pageName == "keystone-aviation-news"?{background:"#0b559c"}:{}} 
            onClick={() => functionCheck("keystone-aviation-news")}>Keystone Aviation</a>
            <a className="button-cat" style={this.props.pageName == "media"?{background:"#0b559c"}:{}} 
            onClick={() => functionCheck("media")}>Logos</a>
            <a className="button-cat" style={this.props.pageName == "market-talk-news"?{background:"#0b559c"}:{}} 
            onClick={() => functionCheck("market-talk-news")}>Market Talk</a>
          </div>
          );
    
    return(
      <section style={{padding: 20}}>
        <div style={{marginBottom: 50}}>
          <h2 style={{fontWeight: 600}}>Categories</h2>
          {hrefElements}
        </div>
        <div style={{marginBottom: 50}}>
          <h2 style={{fontWeight: 600}}>Latest Posts</h2>
          <div>
          {posts.map((element, index) => {
              return (
                <div key={index} style={{
                  display: 'flex',
                  alignItems: 'center',
                  borderBottom: postLength !== index + 1 && '1px solid #e3e3e3',
                  padding: '15px 0px'
                }}>
                  <div>
                    {element.metadata.featured_image.url !== 'https://s3-us-west-2.amazonaws.com/cosmicjs/' && <img style={{borderRadius: 100, width: 76, height: 76, objectPosition: 'center',
                      objectFit: 'cover'}} src={element.metadata.featured_image.url}/>}
                  </div>
                  <div>
                    <a href={`/news-and-views/${element.slug}`}><h6 style={{fontSize: 14, margin: 20, fontWeight: 500, lineHeight: '100%'}}>{element.title}</h6>
                    </a>
                  </div>
                </div>
              )
          })}
          </div>
        </div>
        <div style={{marginBottom: 50}}>
          <h2 style={{fontWeight: 600}}>Archive</h2>
          <ul>{archivesItems()}</ul>
        </div>
      </section>
    )
  }
}
