import React from 'react';
import { Link } from '../../../routes'
import Grid from '@material-ui/core/Grid';
import RequestQuote from '../../../components/views/request-quote-modal';
import Post from '../../../utils/post';
import Router from 'next/router';



export default class FooterTop extends React.Component {

  constructor(props) {
    super(props);
    this.requestRef = React.createRef();
    this.state = {
      firstName: "",
      lastName: "",
      company: "",
      states: "",
      email: "",
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalState: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalCaptcha: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    };
    this.handleOnClickArrow = this.handleOnClickArrow.bind(this);
  }

  componentDidMount() {
    if (/request-a-quote/.test(window.location.href)) {
      this.setState({ modalOpen: true });
    }
  }

  resetForm = () => {
    this.setState({
      firstName: "",
      lastName: "",
      company: "",
      email: "",
    })
  };

  handleFirstNameChange = (event) => {
    this.setState({ firstName: event.target.value });
  };

  handleLastNameChange = (event) => {
    this.setState({ lastName: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handleCompanyChange = (event) => {
    this.setState({ company: event.target.value });
  };
  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalState: "",
      modalPhone: "",
      modalComments: "",
    })
  };
  handleModalSubmit = (event) => {
    this.setState({ modalSubmitting: true });
    event.preventDefault();
    fetch('/api/energy-quote-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.modalEmail,
        name: this.state.modalName,
        phone: this.state.modalPhone,
        company: this.state.modalCompany,
        states: this.state.modalState,
        message: this.state.modalComments
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'Energy Quote Request - Main Page';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Company: ${this.state.modalCompany}</b><br>`
          + `<b>State: ${this.state.modalState}</b><br>`
          + `<b>Sent to: marketing@tacenergy.com, sales@tacenergy.com</b><br>` +
          `</div>`;
        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({ modalSubmitted: true });
      } else {
        this.setState({ modalSubmitted: false, modalSubmitting: false, modalFailedSumbit: true });
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({ modalName: event.target.value });
  };

  handleModalPhoneChange = (event) => {
    this.setState({ modalPhone: event.target.value });
  };

  handleModalEmailChange = (event) => {
    this.setState({ modalEmail: event.target.value });
  };

  handleModalCompanyChange = (event) => {
    this.setState({ modalCompany: event.target.value });
  };

  handleModalStateChange = (event) => {
    this.setState({ modalState: event.target.value });
  };

  handleModalCommentsChange = (event) => {
    this.setState({ modalComments: event.target.value });
  };

  handleModalCaptchaChange = (value) => {
    this.setState({ modalCaptcha: value });
  };

  handleOnClickArrow() {
    this.setState({ modalOpen: true });
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false,
      modalOpen: !this.state.modalOpen
    });
    // Router.push('/tac-energy');
  };

  render() {
    return (
      <div className="footer-container">
        <Grid style={{ margin: '60px 0px', justifyContent: 'space-around', textAlign: 'center' }} container>
          <Grid className="footer-top-nav-item" item xs={12} sm={4} md={2}>
            <a href={'/'}>
              <img src="https://cosmic-s3.imgix.net/93755960-758d-11e8-8644-65ee6ed1055a-TAC.png?w=250" height="30px" style={{ maxWidth: 150 }} />
            </a>
            <ul>
              <li><Link route="/about"><a>About</a></Link></li>
              <li><Link route="/news-and-views"><a>News & Views</a></Link></li>
              <li><Link route="/social"><a>Social</a></Link></li>
              <li><a target="_blank" href="http://taccareers.com/">Careers</a></li>
              <li><Link route="/contact-us"><a>Contact Us</a></Link></li>
            </ul>
          </Grid>
          <Grid className="footer-top-nav-item" item xs={12} sm={4} md={2}>
            <a href={'/tac-energy'}>
              <img src="https://cosmic-s3.imgix.net/9376e000-758d-11e8-9c42-75c5f9571e6d-TACenergy.png?w=250" height="30px" style={{ width: 'auto' }} />
            </a>
            <ul >
              <li><a href="/tac-energy#supply">Supply</a></li>
              <li><a href="/tac-energy#products-services">Products & Services</a></li>
              <li><a target="_blank" href="https://cus.bectran.com/customer/MySecuredLogin?zid=DpdMB315G3Y1M388j6pfb">Apply For Credit</a></li>
              <li><a target="_blank" href="https://portal.tacenergy.com/app/index.html?page=page_login">Customer Login</a></li>
              <li><a href="/news-and-views/category/market-talk-news">Market Talk Updates</a></li>
              <li>
                {/* <a href="/tac-energy#request-a-quote"  >Request A Quote</a> */}
                <a onClick={this.handleOnClickArrow}>
                  Request A Quote
                </a>
              </li>
              {/* <li><a href="/tac-energy#request-a-quote" onClick={e => document.location.reload()}>Request A Quote</a></li> */}
              <li><a href="/tac-energy/contacts">Contacts</a></li>
            </ul>
          </Grid>
          <Grid className="footer-top-nav-item" item xs={12} sm={4} md={2}>
            <a href={'/tac-air'}>
              <img src="https://cosmic-s3.imgix.net/65379b10-74d1-11e8-ac47-fd553e6e3fed-download.jpg?w=250" height="40px" style={{ width: 'auto' }} />
            </a>
            <ul >
              <li><a href="/tac-air/locations">Locations</a></li>
              <li><a href="/tac-air#services">Services</a></li>
              <li><a href="/tac-air#air-get-in-touch">Send Comments</a></li>
            </ul>
          </Grid>
          <Grid className="footer-top-nav-item" item xs={12} sm={6} md={2}>
            <a href={'/keystone-aviation'}>
              <img src="https://cosmic-s3.imgix.net/93a67b80-758d-11e8-9c42-75c5f9571e6d-Keystone%20Aviation.png?w=250" height="45px" style={{ maxWidth: 150, marginBottom: '-5px' }} />
            </a>
            <ul>
              <li><a href="/keystone-aviation/charter-service">Charter</a></li>
              <li><a href="/keystone-aviation/management">Management</a></li>
              <li><a href="/keystone-aviation/maintenance">Maintenance</a></li>
              {/* <li><a href="/keystone-aviation/sales-and-brokerage/">Sales & Brokerage</a></li> */}
              {/* <li><a href="/keystone-aviation/aircrafts-for-sale">Aircraft for Sale</a></li> */}
              <li><a href="/keystone-aviation/contacts">Contacts</a></li>
            </ul>
          </Grid>
          <Grid className="footer-top-nav-item" item xs={12} sm={6} md={2}>
            <a href={'/tac-investments'}>
              <img src="https://cosmic-s3.imgix.net/9f66e8f0-86be-11e8-93b4-d5cc057540a7-investments-logo.svg?w=250" height="25px" style={{ maxWidth: 185, marginTop: 15 }} />
            </a>
            <ul>
              <li><Link route="tac-investments"><a>About</a></Link></li>
            </ul>
          </Grid>
        </Grid>
        <RequestQuote
          modalOpen={this.state.modalOpen}
          onModalClose={this.handleModalClose.bind(this)}
          emailValue={this.state.modalEmail}
          handleSubmit={this.handleModalSubmit.bind(this)}
          onEmailChange={this.handleModalEmailChange.bind(this)}
          NameValue={this.state.modalName}
          onNameChange={this.handleModalNameChange.bind(this)}
          phoneValue={this.state.modalPhone}
          onPhoneChange={this.handleModalPhoneChange.bind(this)}
          companyValue={this.state.modalCompany}
          onCompanyChange={this.handleModalCompanyChange.bind(this)}

          stateValue={this.state.modalState}
          onStateChange={this.handleModalStateChange.bind(this)}

          commentsValue={this.state.modalComments}
          onCommentsChange={this.handleModalCommentsChange.bind(this)}
          submitted={this.state.modalSubmitted}
          submitting={this.state.modalSubmitting}
          failedToSumbit={this.state.modalFailedSubmit}
          onCaptchaChange={this.handleModalCaptchaChange.bind(this)}
        />
      </div>
    )
  }
}