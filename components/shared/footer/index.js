import React from 'react';
import { Link } from '../../../routes'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
const nextRoutes = require('next-routes');
const routes = nextRoutes();
import ArrowButton from '../../buttons/ArrowButton';



class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    // this.routeParam = props.match;
    // var currentLocation = this.props.location.pathname;
    // console.log(currentLocation)

  }

  getRoutes() {
    let qoutes;
    if (routes.Router.router) {
      if (routes.Router.router.route) {
        console.log("yessss" + routes.Router.router.route)
        
        switch (routes.Router.router.route) {
          case "/":
            qoutes = "TAC";
            break;
          case "/newsroom":
            qoutes = "TAC";
            break;
          case "/social-media":
            qoutes = "TAC";
            break;
          case "/tac-energy":
            qoutes = "TACenergy";
            break;
          case "/category-default":
            qoutes = "TAC";
            break;
          case "/post-default":
            qoutes = "TAC";
            break;
          case "/tac-energy-contacts":
            qoutes = "TACenergy";
            break;
          case "/subpage-default":
            qoutes = "TACenergy";
            break;
          case "/tac-air":
            qoutes = "TAC Air";
            break;
          case "/tac-air-military":
            qoutes = "TAC Air";
            break;  
          case "/tac-air-locations":
            qoutes = "TAC Air";
            break;
          case "/keystone-aviation":
            qoutes = "Keystone Aviation";
            break;
          case "/keystone-charter":
            qoutes = "Keystone Aviation";
            break;
          case "/keystone-management":
            qoutes = "Keystone Aviation";
            break;
          case "/keystone-contacts":
            qoutes = "Keystone Aviation";
            break;
          case "/keystone-empty-legs":
            qoutes = "Keystone Aviation";
            break;
          case "/keystone-maintenance":
            qoutes = "Keystone Aviation";
            break;
          case "/keystone-safety-programs":
            qoutes = "Keystone Aviation";
            break;
          case "/aircraft-default":
            qoutes = "Keystone Aviation";
            break;
          case "/tac-investments":
              qoutes = "investments";
              break;
          default:
            qoutes = "TAC";
        }
        console.log(qoutes)
        return qoutes;
      } else {
        console.log("nooooo")
      }
    } else {
      qoutes = "investmentss";
      console.log("no")
      console.log(qoutes)
      return qoutes;
    }
  }
  

  render() {

    return (
      <>
          <div className="bggry" >
          <span className="spam-text">Find Your Next Great Role With {this.getRoutes()}. 
          <a href="https://taccareers.com/" target="_blank">
          <button className="careers-btn-new">Explore Careers <svg  width="0.597in" height="0.306in">
          <path fill={'white'} d="M1.151,11.511 L40.649,11.511 L31.604,20.568 C31.349,20.824 31.349,21.242 31.604,21.498 C31.860,21.753 32.277,21.753 32.532,21.498 L42.701,11.320 C42.704,11.314 42.710,11.307 42.717,11.300 C42.730,11.286 42.743,11.272 42.753,11.255 C42.762,11.246 42.772,11.232 42.778,11.223 C42.788,11.206 42.798,11.192 42.808,11.176 C42.814,11.163 42.821,11.148 42.826,11.135 C42.833,11.125 42.837,11.116 42.844,11.106 C42.844,11.099 42.846,11.093 42.846,11.088 C42.853,11.073 42.856,11.059 42.862,11.045 C42.865,11.028 42.872,11.012 42.876,10.996 C42.878,10.981 42.882,10.967 42.882,10.951 C42.885,10.934 42.889,10.918 42.889,10.902 C42.892,10.886 42.892,10.870 42.892,10.853 C42.892,10.837 42.892,10.823 42.889,10.806 C42.889,10.790 42.885,10.772 42.882,10.756 C42.882,10.743 42.878,10.727 42.876,10.712 C42.872,10.695 42.865,10.678 42.862,10.663 C42.856,10.647 42.853,10.633 42.849,10.620 C42.846,10.614 42.844,10.607 42.844,10.602 C42.837,10.591 42.833,10.581 42.826,10.571 C42.821,10.559 42.814,10.546 42.808,10.533 C42.798,10.517 42.788,10.501 42.778,10.487 C42.772,10.475 42.762,10.463 42.753,10.452 C42.743,10.437 42.730,10.423 42.717,10.410 C42.710,10.403 42.707,10.396 42.701,10.388 L32.532,0.209 C32.406,0.083 32.238,0.018 32.070,0.018 C31.902,0.018 31.733,0.083 31.604,0.209 C31.349,0.467 31.349,0.883 31.604,1.139 L31.604,1.141 L40.649,10.196 L1.151,10.196 C0.789,10.196 0.495,10.491 0.495,10.853 C0.495,11.216 0.789,11.511 1.151,11.511 Z"/>
        </svg>
        </button>
        </a> 
        </span>
      </div>
      <div style={{ borderTop: '1px solid #f3f3f3', padding: 25 }}>
        <div className="container footer-bottom" >
          <ul>
            <li><Link route="/privacy-policy"><a>Privacy Policy</a></Link></li>
            <li><Link route="/terms-of-use"><a>Terms of Use</a></Link></li>
            <li><a target="_blank" href="https://nw12.ultipro.com/Login.aspx">Employee Login</a></li>
          </ul>
          <span style={{ alignSelf: 'flex-end', fontSize: 12 }}>TAC - The Arnold Companies &copy; {new Date().getFullYear()}. All Rights Reserved.</span>
        </div>
      </div>
      </>
    );
    
   
  }
}

export default MyComponent;
