import React from 'react';
import Recaptcha from 'react-recaptcha';

// define a variable to store the recaptcha instance
let recaptchaInstance;

// site key
const sitekey = "6Lern2sUAAAAAJL9E2ujWAonZShHS6VSFKg553hk";

const executeCaptcha = function () {
  recaptchaInstance.execute();
};


export default class ContactCTA extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isKeystone: false,
      isInvestments: false,
    }
  }

  componentDidMount() {
    if (/keystone-aviation/.test(window.location.href)) {
      this.setState({
        isKeystone: true
      })
    } else if (/tac-investments/.test(window.location.href)) {
      this.setState({
        isInvestments: true
      })
    }
  }

  verifyCallback = (response) => {
    if (response) {
      this.props.handleSubmit();
    }
  };

  render() {
    const {
      cta,
      bgColor,
      logoHeight,
      btnColor,
      onEmailChange,
      emailValue,
      onNameChange,
      nameValue,
      onPhoneChange,
      phoneValue,
      onMessageChange,
      messageValue,
      submitting,
      submitted,
      failedToSubmit,
      handleSubmit
    } = this.props;

    return (
      <div style={{ backgroundColor: bgColor ? bgColor : '#0b559c', display: 'flex', alignItems: 'center', flexDirection: 'column', clear: 'both' }}>
        <h2 style={{ color: 'white', margin: '10px 60px', padding: 0, textTransform: 'uppercase', letterSpacing: 1, fontWeight: 100, fontSize: 35, paddingTop: 50, textAlign: 'center', maxWidth: 800 }}>
          {submitted ? 'Thank you for contacting TAC - The Arnold Companies. We will reply to your message as soon as possible.'
            : submitting ? 'Submitting Message...'
              : failedToSubmit ? 'Failed To Send Message'
                : this.state.isInvestments ? 'THE ARNOLD COMPANIES' : 'Get In Touch'}
        </h2>
        {submitting !== true ?
          <form onSubmit={handleSubmit} style={{ width: '75%', maxWidth: 850 }}>
            <div className="cta-wrapper container ">
              <div className="cta-pad-right" style={{ flexBasis: '50%' }}>
                <label>
                  <input type="text" placeholder="Name" value={nameValue} onChange={onNameChange}
                    required />
                  <span>Name</span>
                </label>
                <label>
                  <input type="email" placeholder="Email" value={emailValue} onChange={onEmailChange}
                    required />
                  <span>Email</span>
                </label>
                <label>
                  <input type="tel" placeholder="Phone" value={phoneValue} required
                    onChange={onPhoneChange} />
                  <span>Phone</span>
                </label>
              </div>
              <div className="cta-pad-left" style={{ flexBasis: '50%' }}>
                <label>
                  <textarea id="message" placeholder="Your Message" rows="8" value={messageValue}
                    onChange={onMessageChange} required />
                  <span>Message</span>
                </label>
              </div>
            </div>
            <div className={"container keystone-contact-meta"} style={{ alignItems: 'center', justifyContent: this.state.isKeystone ? 'space-between' : 'center', display: 'flex', padding: '0px 0px 50px 0px' }}>
              {this.state.isKeystone &&
                <div style={{ paddingBottom: 25 }}>
                  <div style={{ display: 'flex', alignItems: 'center', marginBottom: 10 }}>
                    <a style={{ dispay: 'inline-block' }} href="tel:+1801-933-7500"><img style={{ marginRight: 20 }} height="30px" src={"https://cosmic-s3.imgix.net/ad9f0a50-abbd-11e8-8def-8171c3d33a2e-call-answer.svg"} /></a>
                    <a href="tel:+1801-933-7500">
                      <span style={{ color: 'white', fontSize: 16 }}>888.900.6070</span>
                    </a>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <a style={{ dispay: 'inline-block' }} href="https://goo.gl/maps/L9SVekuP5VM2" target="_blank"><img style={{ marginRight: 20 }} height="30px" src={"https://cosmic-s3.imgix.net/433b2420-abc0-11e8-8def-8171c3d33a2e-facebook-placeholder-for-locate-places-on-maps.svg"} /></a>
                    <span style={{ color: 'white', fontSize: 16 }}>
                      <a className="keystone-address" href="https://goo.gl/maps/L9SVekuP5VM2" target="_blank">Keystone Aviation <br /> Salt Lake City International Airport <br /> 303 North 2370 West <br /> Salt Lake City, UT 84116</a>
                    </span>
                  </div>
                </div>
              }
              <input style={{ background: btnColor ? btnColor : '#e2383f', alignSelf: this.state.isKeystone && 'flex-start' }} type="submit" value="SUBMIT" />
            </div>
          </form> :
          <div style={{ textAlign: 'center' }}>
            <div className={submitted ? "circle-loader load-complete" : "circle-loader"}>
              <div className="checkmark draw" style={{ display: submitted && 'block' }} />
            </div>
          </div>
        }
      </div>
    )
  }
}