import React from 'react';
import Grid from '@material-ui/core/Grid';

export default class Fleet extends React.Component {
  render() {
    const { charter } = this.props;
    return (
      <Grid container spacing={8}>
        {!!charter && charter.map((aircraft, index) =>
          <Grid key={index} item xs={12} sm={6} md={3}>
            <article style={{maxWidth: 400, padding: 10}}>
              <a href={`/keystone-aviation/${aircraft.slug}`}>
                <div  className="card">
                  {aircraft.metadata.photo_gallery.featured_image.url !== 'https://s3-us-west-2.amazonaws.com/cosmicjs/' && <img width="100%" src={aircraft.metadata.photo_gallery.featured_image.url} sizes="(max-width: 1024px) 100vw, 1024px"/>}
                  <h2 className="aircraft-title">{aircraft.title}</h2>
                </div>
              </a>
            </article>
          </Grid>
        )}
      </Grid>
    )
  }
}