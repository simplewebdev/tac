import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import { Element } from 'react-scroll';
import Button from '@material-ui/core/Button';
import Recaptcha from 'react-recaptcha';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

// define a variable to store the recaptcha instance
let recaptchaInstance;

// site key
const sitekey = "6Lern2sUAAAAAJL9E2ujWAonZShHS6VSFKg553hk";

const executeCaptcha = function () {
  recaptchaInstance.execute();
};

export default class RequestQuote extends React.Component {
  constructor(props) {
    super(props);
  }

  verifyCallback = (response) => {
    if (response) {
      this.props.handleSubmit();
    }
  };

  render() {
    const {
      modalOpen,
      onModalClose,
      onEmailChange,
      emailValue,
      onNameChange,
      nameValue,
      onPhoneChange,
      phoneValue,
      onCompanyChange,
      companyValue,
      onStateChange,
      stateValue,
      onCommentsChange,
      commentsValue,
      submitting,
      submitted,
    } = this.props;



    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={modalOpen}
        onClose={onModalClose}
        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
      >
        <Element name="description" className="request-quote-element" id="containerElement">
          <div>
            <h1>Request a Quote</h1>
            {submitting !== true ?
              <form onSubmit={this.props.handleSubmit}>
                <div className={'request-form'} >
                  <TextField
                    id="with-placeholder"
                    label="Name"
                    type="name"
                    placeholder="Name"
                    margin="normal"
                    required
                    value={nameValue}
                    onChange={onNameChange}
                    style={{ margin: '15px 0px' }}
                  />
                  <TextField
                    id="with-placeholder"
                    type="email"
                    label="Email"
                    placeholder="Email"
                    margin="normal"
                    required
                    value={emailValue}
                    onChange={onEmailChange}
                    style={{ margin: '15px 0px' }}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Company"
                    placeholder="Company"
                    margin="normal"
                    required
                    value={companyValue}
                    onChange={onCompanyChange}
                    style={{ margin: '15px 0px' }}
                  />
                  <FormControl>
                    <InputLabel htmlFor="fbo-native-simple">Select State</InputLabel>
                    <Select
                      placeholder="Select State"
                      native
                      required
                      value={stateValue}
                      onChange={onStateChange}
                      inputProps={{
                        name: 'Select FBO',
                        id: 'fbo-native-simple',
                      }}
                    >
                      <option value="" />
                      <option value={"Alabama"} >Alabama</option>
                      <option value={"Alaska"} >Alaska</option>
                      <option value={"Arizona"} >Arizona</option>
                      <option value={"Arkansas"} >Arkansas</option>
                      <option value={"California"} >California</option>
                      <option value={"Colorado"} >Colorado</option>
                      <option value={"Connecticut"} >Connecticut</option>
                      <option value={"Delaware"} >Delaware</option>
                      <option value={"District Of Columbia"} >District Of Columbia</option>
                      <option value={"Florida"} >Florida</option>
                      <option value={"Georgia"} >Georgia</option>
                      <option value={"Hawaii"} >Hawaii</option>
                      <option value={"Idaho"} >Idaho</option>
                      <option value={"Illinois"} >Illinois</option>
                      <option value={"Indiana"} >Indiana</option>
                      <option value={"Iowa"} >Iowa</option>
                      <option value={"Kansas"} >Kansas</option>
                      <option value={"Kentucky"} >Kentucky</option>
                      <option value={"Louisiana"} >Louisiana</option>
                      <option value={"Maine"} >Maine</option>
                      <option value={"Maryland"} >Maryland</option>
                      <option value={"Massachusetts"} >Massachusetts</option>
                      <option value={"Michigan"} >Michigan</option>
                      <option value={"Minnesota"} >Minnesota</option>
                      <option value={"Mississippi"} >Mississippi</option>
                      <option value={"Missouri"} >Missouri</option>
                      <option value={"Montana"} >Montana</option>
                      <option value={"Nebraska"} >Nebraska</option>
                      <option value={"Nevada"} >Nevada</option>
                      <option value={"New Hampshire"} >New Hampshire</option>
                      <option value={"New Jersey"} >New Jersey</option>
                      <option value={"New Mexico"} >New Mexico</option>
                      <option value={"New York"} >New York</option>
                      <option value={"North Carolina"} >North Carolina</option>
                      <option value={"North Dakota"} >North Dakota</option>
                      <option value={"Ohio"} >Ohio</option>
                      <option value={"Oklahoma"} >Oklahoma</option>
                      <option value={"Oregon"} >Oregon</option>
                      <option value={"Pennsylvania"} >Pennsylvania</option>
                      <option value={"Rhode Island"} >Rhode Island</option>
                      <option value={"South Carolina"} >South Carolina</option>
                      <option value={"South Dakota"} >South Dakota</option>
                      <option value={"Tennessee"} >Tennessee</option>
                      <option value={"Texas"} >Texas</option>
                      <option value={"Utah"} >Utah</option>
                      <option value={"Vermont"} >Vermont</option>
                      <option value={"Virginia"} >Virginia</option>
                      <option value={"Washington"} >Washington</option>
                      <option value={"West Virginia"} >West Virginia</option>
                      <option value={"Wisconsin"} >Wisconsin</option>
                      <option value={"Wyoming"} >Wyoming</option>
                    </Select>
                  </FormControl>
                  <TextField
                    id="with-placeholder"
                    label="Phone"
                    placeholder="Phone"
                    margin="normal"
                    required
                    value={phoneValue}
                    onChange={onPhoneChange}
                    style={{ margin: '15px 0px' }}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Comments"
                    placeholder="Comments"
                    margin="normal"
                    multiline
                    rows="4"
                    value={commentsValue}
                    onChange={onCommentsChange}
                    style={{ margin: '15px 0px' }}
                  />
                  {/*<Recaptcha*/}
                  {/*ref={(e) => recaptchaInstance = e}*/}
                  {/*sitekey={sitekey}*/}
                  {/*size="invisible"*/}
                  {/*verifyCallback={this.verifyCallback}*/}
                  {/*/>*/}
                </div>
                <div className="btn-wrapper">
                  <Button onClick={onModalClose} style={{ color: 'rgba(0,0,0,0.66)' }}>Cancel</Button>
                  <Button type="submit" style={{ color: '#0b559c' }}>Submit</Button>
                </div>
              </form> :
              <div style={{ textAlign: 'center' }}>
                <div className={submitted ? "circle-loader load-complete" : "circle-loader"}>
                  <div className="checkmark draw" style={{ display: submitted && 'block' }} />
                </div>
                <div style={{ float: submitted ? 'none' : submitting ? 'none' : 'right' }} className="btn-wrapper">
                  <Button onClick={onModalClose} style={{ color: 'rgba(0,0,0,0.66)' }}>Close</Button>
                </div>
              </div>
            }
          </div>
        </Element>
      </Modal>
    )
  }
}