import React from 'react';
import Recaptcha from 'react-recaptcha';

// define a variable to store the recaptcha instance
let recaptchaInstance;

// site key
const sitekey = "6Lern2sUAAAAAJL9E2ujWAonZShHS6VSFKg553hk";

const executeCaptcha = function () {
  recaptchaInstance.execute();
};


export default class ContactAirCTA extends React.Component {
  constructor(props) {
    super(props);
  }

  verifyCallback = (response) => {
    if (response) {
      this.props.handleSubmit();
    }
  };

  render() {
    const {
      cta,
      bgColor,
      logoHeight,
      btnColor,
      onEmailChange,
      emailValue,
      onNameChange,
      nameValue,
      onPhoneChange,
      phoneValue,
      onMessageChange,
      messageValue,
      companyValue,
      onCompanyChange,
      titleValue,
      onTitleChange,
      tailNumberValue,
      onTailNumberChange,
      fboValue,
      onFBOChange,
      submitting,
      submitted,
      failedToSubmit,
      handleSubmit,
      isFBO,
      title
    } = this.props;

    return(
      <div style={{backgroundColor: bgColor ? bgColor : '#0b559c', display: 'flex', alignItems: 'center', flexDirection: 'column', clear: 'both'}}>
        <h2 style={{color: 'white', margin: '10px 60px', padding: 0, textTransform: 'uppercase', letterSpacing: 1, fontWeight: 100, fontSize: 35, paddingTop: 50, textAlign: 'center', maxWidth: 800}}>
          {submitted ? 'Thank you for contacting TAC - The Arnold Companies. We will reply to your message as soon as possible.'
            : submitting ? 'Submitting Message...'
              : failedToSubmit ? 'Failed To Send Message'
                : title ? title : 'Get In Touch'}
        </h2>
        {submitting !== true ?
          <form onSubmit={handleSubmit} style={{width: '75%', maxWidth: 850}}>
            <div className="cta-wrapper container ">
              <div className="cta-pad-right" style={{flexBasis: '33%'}}>
                <label>
                  <input type="text" placeholder="Name" value={nameValue} onChange={onNameChange}
                         required/>
                  <span>Name</span>
                </label>
                <label>
                  <input type="email" placeholder="Email" value={emailValue} onChange={onEmailChange}
                         required/>
                  <span>Email</span>
                </label>
                <label>
                  <input type="tel" placeholder="Phone" value={phoneValue}
                         onChange={onPhoneChange} required/>
                  <span>Phone</span>
                </label>
              </div>
              <div className="cta-pad-right cta-pad-left" style={{flexBasis: '33%'}}>
                <label>
                  <input type="text" placeholder="Company" value={companyValue} onChange={onCompanyChange}
                         required/>
                  <span>Company</span>
                </label>
                <label>
                  <input type="text" placeholder="Title" value={titleValue} onChange={onTitleChange}
                         required/>
                  <span>Title</span>
                </label>
                <label>
                  <input type="text" placeholder="Tail Number" value={tailNumberValue}
                         onChange={onTailNumberChange} required/>
                  <span>Tail Number</span>
                </label>
              </div>
              <div className="cta-pad-left" style={{flexBasis: '33%'}}>
                <label>
                    <textarea id="message" placeholder="Your Message" rows="8" value={messageValue}
                              onChange={onMessageChange} required/>
                  <span>Message</span>
                </label>
              </div>
            </div>
            <div className={'container'} style={{padding: '5px 0px'}}>
            <label style={{ flexBasis: '33%', width: '33%'}}>
              <select value={fboValue} onChange={onFBOChange} required>
                <option value="" disabled>Select FBO Location</option>
                <option value="ama">AMA</option>
                <option value="apa">APA</option>
                <option value="bdl">BDL</option>
                <option value="fsm">FSM</option>
                <option value="lex">LEX</option>
                <option value="lit">LIT</option>
                <option value="oma">OMA</option>
                <option value="pvu">PVU</option>
                <option value="rdu">RDU</option>
                <option value="shv">SHV</option>
                <option value="slc">SLC</option>
                <option value="sus">SUS</option>
                <option value="tys">TYS</option>
                <option value="txk">TXK</option>
              </select>
            </label>
            </div>
            <div className={'container contact-air-cta'} style={{alignItems: 'center', justifyContent: isFBO ? 'center' : 'space-between', display: 'flex', padding: '0px 0px 50px 0px'}}>
              {!isFBO &&
              <div style={{paddingBottom: 25}}>
                <div style={{display: 'flex', alignItems: 'center', marginBottom: 10}}>
                  <img style={{marginRight: 20}} height="30px"
                       src={"https://cosmic-s3.imgix.net/670c3f40-abe0-11e8-97a6-57aff3ccd6d6-call-answer%20(1).svg"}/>
                  <a href="tel:1+801-933-7500">
                    <span style={{color: 'white', fontSize: 16}}>(972) 807-7879</span>
                  </a>
                </div>
                <div style={{display: 'flex', alignItems: 'center'}}>
                  <img style={{marginRight: 20}} height="30px"
                       src={"https://cosmic-s3.imgix.net/406190c0-abe0-11e8-bc2e-5788e85c3092-facebook-placeholder-for-locate-places-on-maps%20(1).svg"}/>
                  <span
                    style={{color: 'white', fontSize: 16}}>
                    100 Crescent Court, Suite 1600 <br/> Dallas, TX 75201
                  </span>
                </div>
              </div>
              }
              <input style={{background: btnColor ? btnColor : '#e2383f', margin: 0, alignSelf: 'flex-start'}} type="submit" value="SUBMIT"/>
            </div>
          </form> :
          <div style={{textAlign: 'center'}}>
            <div className={submitted ? "circle-loader load-complete" : "circle-loader"}>
              <div className="checkmark draw" style={{display: submitted && 'block'}}/>
            </div>
          </div>
        }
      </div>
    )
  }
}