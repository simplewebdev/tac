import React from 'react';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import { Element } from 'react-scroll';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Recaptcha from 'react-recaptcha';

// define a variable to store the recaptcha instance
let recaptchaInstance;

// site key
const sitekey = "6Lern2sUAAAAAJL9E2ujWAonZShHS6VSFKg553hk";

const executeCaptcha = function () {
  recaptchaInstance.execute();
};

export default class RequestFBO extends React.Component {
  constructor(props) {
    super(props);
  }


  verifyCallback = (response) => {
    if (response) {
      this.props.handleSubmit();
    }
  };

  render() {
    const {
      modalOpen,
      onModalClose,
      onEmailChange,
      emailValue,
      onNameChange,
      nameValue,
      onPhoneChange,
      phoneValue,
      onCommentsChange,
      commentsValue,
      submitting,
      submitted,
      failedToSumbit,
      depDateValue,
      onDepDateChange,
      arrDateValue,
      onArrDateChange,
      tailNumberValue,
      onTailNumberChange,
      fboValue,
      onFboChange

    } = this.props;


    return(
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={modalOpen}
        onClose={onModalClose}
        style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}
      >
        <Element name="description" className="request-quote-element" id="containerElement">
          <div>

            <h1>Service Request</h1>
            { submitting !== true ?
              <form onSubmit={this.props.handleSubmit}>
                <div className={'request-form'} >
                  <TextField
                    id="with-placeholder"
                    label="Name"
                    type="name"
                    placeholder="Name"
                    margin="normal"
                    required
                    value={nameValue}
                    onChange={onNameChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="number"
                    label="Tail Number"
                    placeholder="Tail Number"
                    value={tailNumberValue}
                    onChange={onTailNumberChange}
                    type="text"
                    required
                    margin="normal"
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Phone"
                    type="number"
                    placeholder="Phone"
                    margin="normal"
                    required
                    value={phoneValue}
                    onChange={onPhoneChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    type="email"
                    label="Email"
                    placeholder="Email"
                    margin="normal"
                    required
                    value={emailValue}
                    onChange={onEmailChange}
                    style={{margin: '15px 0px'}}
                  />
                  <FormControl>
                    <InputLabel htmlFor="fbo-native-simple">Select Contact</InputLabel>
                    <Select
                      placeholder="Select Contact"
                      native
                      required
                      value={fboValue}
                      onChange={onFboChange}
                      inputProps={{
                        name: 'Select FBO',
                        id: 'fbo-native-simple',
                      }}
                    > 
                      <option value="" />
                      <option value={"ama"} data-tacair-email={"amafbo@tacair.com"}>AMA-FBO</option>
                      <option value={"apa"} data-tacair-email={"apafbo@tacair.com"}>APA-FBO</option>
                      <option value={"bdl"} data-tacair-email={"bdlfbo@tacair.com"}>BDL-FBO</option>
                      <option value={"dal"} data-tacair-email={"jgibney@tacair.com"}>DAL-FBO</option>
                      <option value={"fsm"} data-tacair-email={"fsmfbo@tacair.com"}>FSM-FBO</option>
                      <option value={"lex"} data-tacair-email={"lexfbo@tacair.com"}>LEX-FBO</option>
                      <option value={"lit"} data-tacair-email={"litfbo@tacair.com"}>LIT-FBO</option>
                      <option value={"oma"} data-tacair-email={"omafbo@tacair.com"}>OMA-FBO</option>
                      <option value={"pvu"} data-tacair-email={"pvufbo@tacair.com"}>PVU-FBO</option>
                      <option value={"rdu"} data-tacair-email={"rdufbo@tacair.com"}>RDU-FBO</option>
                      <option value={"shv"} data-tacair-email={"shvfbo@tacair.com"}>SHV-FBO</option>
                      <option value={"slc"} data-tacair-email={"slcfbo@tacair.com"}>SLC-FBO</option>
                      <option value={"sus"} data-tacair-email={"susfbo@tacair.com"}>SUS-FBO</option>
                      <option value={"tys"} data-tacair-email={"tysfbo@tacair.com"}>TYS-FBO</option>
                      <option value={"txk"} data-tacair-email={"txkfbo@tacair.com"}>TXK-FBO</option>
                      <option value={"marketing"} data-tacair-email={"marketing@tacenergy.com"}>Marketing</option>
                      <option value={"general"} data-tacair-email={"marketing@tacenergy.com"}>General</option>
                    </Select>
                  </FormControl>
                  <TextField
                    id="datetime-local"
                    label="Arrival Date/Time"
                    type="datetime-local"
                    value={arrDateValue}
                    onChange={onArrDateChange}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="datetime-local"
                    label="Departure Date/Time"
                    type="datetime-local"
                    value={depDateValue}
                    onChange={onDepDateChange}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Comments"
                    placeholder="Enter details here..."
                    margin="normal"
                    multiline
                    rows="4"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    value={commentsValue}
                    onChange={onCommentsChange}
                    style={{margin: '15px 0px'}}
                  />
                  {/*<Recaptcha*/}
                    {/*ref={(e) => recaptchaInstance = e}*/}
                    {/*sitekey={sitekey}*/}
                    {/*size="invisible"*/}
                    {/*verifyCallback={this.verifyCallback}*/}
                  {/*/>*/}
                </div>
                <div className="btn-wrapper">
                  <Button onClick={onModalClose} style={{color: 'rgba(0,0,0,0.66)'}}>Cancel</Button>
                  <Button type="submit" style={{color: '#0b559c'}}>Submit</Button>
                </div>
              </form> :
              <div style={{textAlign: 'center'}}>
                <div className={submitted ? "circle-loader load-complete" : "circle-loader"}>
                  <div className="checkmark draw" style={{display: submitted && 'block'}}/>
                </div>
                <div style={{float: submitted ? 'none' : submitting ? 'none' : 'right' }} className="btn-wrapper">
                  <Button onClick={onModalClose} style={{color: 'rgba(0,0,0,0.66)'}}>Close</Button>
                </div>
              </div>
            }
          </div>
        </Element>
      </Modal>
    )
  }
}