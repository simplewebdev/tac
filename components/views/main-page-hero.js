import React from 'react';
import ArrowButton from '../buttons/ArrowButton';
import ReactPlayer from 'react-player';

export default class MainPageHero extends React.Component {
  render() {
    const { hero, headingWidth, excerptWidth } = this.props;

    let heroTitle = hero.value;
    let heroImage = `${hero.children[1].imgix_url}?w=2000`;
    let heroExcerpt = hero.children[3].value;
    let heroLink = hero.children[4].value;
    let heroVideo = hero.children[2].url;
    let isVideo = hero.children[2].value !== '';


    return(
      <div style={{ height: '60vh', width: '100%', position: 'relative', overflow: 'hidden' }}>
        <style jsx>{`
          .hero-img {
            animation-name: heroZoomIn;
            animation-duration: 20s;
            animation-iteration-count: infinite;
            animation-direction: alternate;
          }

          @keyframes heroZoomIn {
            0%{ transform:scale(1) }
            100% { transform:scale(1.1) }
          }
        `}</style>
        <div
          className="hero-img"
          style={{
            //display: isVideo && 'none',
            background: isVideo ? 'rgba(0,0,0,0.15)' : `url(${heroImage})`,
            zIndex: isVideo && 1,
            backgroundSize:'cover',
            backgroundPosition:'center'
          }}
        />
        {
          isVideo &&
          <ReactPlayer
            url={heroVideo}
            playing
            playsinline
            autoPlay
            muted
            loop
            width="100%"
            height="100%"
            className='player'
          />
        }
        <div className="header-container hero-wrapper">
          <div>
            <div className="hero-title" style={{maxWidth: headingWidth ? headingWidth : 660}} dangerouslySetInnerHTML={{ __html: heroTitle }}/>
            {
              heroExcerpt === '' ? null : <p className="hero-excerpt" style={{maxWidth: excerptWidth ? excerptWidth : 390, margin: '10px 0px'}}>{heroExcerpt}</p>
            }
            {
              heroLink === '' ? null :
                <ArrowButton link={heroLink} color='white' phrase='learn more'/>
            }
          </div>
        </div>
      </div>
    )
  }
}
