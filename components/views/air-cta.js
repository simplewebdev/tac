import React from 'react';
import PropTypes from 'prop-types';


export default class AirCTA extends React.Component {
  render() {
    const {
      phone,
      handleOnClickArrow,
      handleOnClickArrowComments,
    } = this.props;

    return(
      <section style={{backgroundColor: '#e2383f'}}>
        <div className={'container'}>
          <h2 style={{color: 'white', margin: 0, padding: 0, textTransform: 'uppercase', letterSpacing: 1, fontWeight: 100, fontSize: 35, paddingTop: 50, paddingBottom: 30}}>
            GET IN TOUCH
          </h2>
          <div className="air-button-container">
            <p>Have a question or just want to tell us something? Call Us.</p>
            <a className="button-air button-air-white" style={{fontWeight: '700', color: 'black', background: 'white', letterSpacing: 0.6}} href={`${`tel:+1${phone ? phone : '972-807-7879'}`}`}>{phone ? phone : '972-807-7879'}</a>
          </div>
          <div className="air-button-container">
            <p>Need a service request at one of our locations? Send us all the details and we’ll take care of the rest!</p>
            <a className="button-air" style={{fontWeight: '700', color: 'white', background: '#666', letterSpacing: 0.6}} onClick={handleOnClickArrow}>SERVICE REQUEST</a>
          </div>
          <div className="air-button-container">
            <p>Want to tell us about your recent experience at one of our locations? Send us a comment.</p>
            <a className="button-air" style={{fontWeight: '700', color: 'white', background: '#0b559c', letterSpacing: 0.6}} onClick={handleOnClickArrowComments}>SEND COMMENTS</a>
          </div>
        </div>
      </section>
    )
  }
}