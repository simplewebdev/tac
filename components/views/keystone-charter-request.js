import React from 'react';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import { Element } from 'react-scroll';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Recaptcha from 'react-recaptcha';

// define a variable to store the recaptcha instance
let recaptchaInstance;

// site key
const sitekey = "6Lern2sUAAAAAJL9E2ujWAonZShHS6VSFKg553hk";

const executeCaptcha = function () {
  recaptchaInstance.execute();
};

export default class RequestCharter extends React.Component {
  constructor(props) {
    super(props);
  }


  verifyCallback = (response) => {
    if (response) {
      this.props.handleSubmit();
    }
  };

  render() {
    const {
      modalOpen,
      onModalClose,
      onEmailChange,
      emailValue,
      onNameChange,
      nameValue,
      onPhoneChange,
      phoneValue,
      onCommentsChange,
      commentsValue,
      submitting,
      submitted,
      failedToSumbit,
      depCityValue,
      onDepCityChange,
      depDateValue,
      onDepDateChange,
      arrCityValue,
      onArrCityChange,
      arrDateValue,
      onArrDateChange,
      numOfPassValue,
      onNumOfPassChange,
      prefAircraftValue,
      onPrefAircraftChange,
      cityValue,
      onCityChange,
      addressValue,
      onAddressChange,
      zipCodeValue,
      onZipCodeChange,
      USStateValue,
      onUSStateChange,

    } = this.props;


    let states = [ "AK - Alaska",
      "AL - Alabama",
      "AR - Arkansas",
      "AS - American Samoa",
      "AZ - Arizona",
      "CA - California",
      "CO - Colorado",
      "CT - Connecticut",
      "DC - District of Columbia",
      "DE - Delaware",
      "FL - Florida",
      "GA - Georgia",
      "GU - Guam",
      "HI - Hawaii",
      "IA - Iowa",
      "ID - Idaho",
      "IL - Illinois",
      "IN - Indiana",
      "KS - Kansas",
      "KY - Kentucky",
      "LA - Louisiana",
      "MA - Massachusetts",
      "MD - Maryland",
      "ME - Maine",
      "MI - Michigan",
      "MN - Minnesota",
      "MO - Missouri",
      "MS - Mississippi",
      "MT - Montana",
      "NC - North Carolina",
      "ND - North Dakota",
      "NE - Nebraska",
      "NH - New Hampshire",
      "NJ - New Jersey",
      "NM - New Mexico",
      "NV - Nevada",
      "NY - New York",
      "OH - Ohio",
      "OK - Oklahoma",
      "OR - Oregon",
      "PA - Pennsylvania",
      "PR - Puerto Rico",
      "RI - Rhode Island",
      "SC - South Carolina",
      "SD - South Dakota",
      "TN - Tennessee",
      "TX - Texas",
      "UT - Utah",
      "VA - Virginia",
      "VI - Virgin Islands",
      "VT - Vermont",
      "WA - Washington",
      "WI - Wisconsin",
      "WV - West Virginia",
      "WY - Wyoming"];

    return(
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={modalOpen}
        onClose={onModalClose}
        style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}
      >
        <Element name="description" className="request-quote-element" id="containerElement">
          <div>

            <h1>Request a Quote</h1>
            { submitting !== true ?
              <form onSubmit={this.props.handleSubmit}>
                <div className={'request-form'} >
                  <TextField
                    id="with-placeholder"
                    label="Departure City"
                    type="text"
                    placeholder="Departure City"
                    margin="normal"
                    value={depCityValue}
                    onChange={onDepCityChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="date"
                    label="Departure Date"
                    type="date"
                    value={depDateValue}
                    onChange={onDepDateChange}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Arrival City"
                    type="text"
                    placeholder="Arrival City"
                    margin="normal"
                    value={arrCityValue}
                    onChange={onArrCityChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="date"
                    label="Arrival Date"
                    type="date"
                    value={arrDateValue}
                    onChange={onArrDateChange}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="number"
                    label="Number of Passengers"
                    placeholder="Number of Passengers"
                    value={numOfPassValue}
                    onChange={onNumOfPassChange}
                    type="number"
                    required
                    margin="normal"
                    style={{margin: '15px 0px'}}
                  />
                  <FormControl>
                    <InputLabel htmlFor="aircraft-native-simple">Preferred Aircraft</InputLabel>
                    <Select
                      placeholder="preferred Aircraft"
                      native
                      value={prefAircraftValue}
                      onChange={onPrefAircraftChange}
                      inputProps={{
                        name: 'Preferred Aircraft',
                        id: 'aircraft-native-simple',
                      }}
                    >
                      <option value="" />
                      <option value={"Gulfstream G550"}>Gulfstream G550</option>
                      <option value={"Gulfstream G200"}>Gulfstream G200</option>
                      <option value={"Gulfstream G150"}>Gulfstream G150</option>
                      <option value={"Hawker 800"}>Hawker 800</option>
                      <option value={"Citation Ultra"}>Citation Ultra</option>
                      <option value={"Citation Excel"}>Citation Excel</option>
                      <option value={"Embraer Phenom"}>Embraer Phenom</option>
                      <option value={"Pilatus PC-12"}>Pilatus PC-12</option>
                      <option value={"No Preference"}>No Preference</option>
                    </Select>
                  </FormControl>
                  <TextField
                    id="with-placeholder"
                    label="Name"
                    type="name"
                    placeholder="Name"
                    margin="normal"
                    required
                    value={nameValue}
                    onChange={onNameChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    type="email"
                    label="Email"
                    placeholder="Email"
                    margin="normal"
                    required
                    value={emailValue}
                    onChange={onEmailChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Phone"
                    placeholder="Phone"
                    margin="normal"
                    required
                    value={phoneValue}
                    onChange={onPhoneChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Your Address"
                    placeholder="Your Address"
                    margin="normal"
                    required
                    value={addressValue}
                    onChange={onAddressChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Your City"
                    placeholder="Your City"
                    margin="normal"
                    required
                    value={cityValue}
                    onChange={onCityChange}
                    style={{margin: '15px 0px'}}
                  />
                  <FormControl>
                    <InputLabel htmlFor="state-native-simple">Your State</InputLabel>
                    <Select
                      placeholder="Your State"
                      native
                      value={USStateValue}
                      onChange={onUSStateChange}
                      inputProps={{
                        name: 'Your State',
                        id: 'state-native-simple',
                      }}
                    >
                      <option value="" />
                      {
                        states.map((state, index) => {
                          return (
                            <option key={index} value={state}>{state}</option>
                            )
                        })
                      }
                    </Select>
                  </FormControl>
                  <TextField
                    id="with-placeholder"
                    label="Your Zipcode"
                    placeholder="Your Zipcode"
                    margin="normal"
                    required
                    value={zipCodeValue}
                    onChange={onZipCodeChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Comments"
                    placeholder="If you require ground transportation, catering or any other assistance, please enter it here."
                    margin="normal"
                    multiline
                    rows="4"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    value={commentsValue}
                    onChange={onCommentsChange}
                    style={{margin: '15px 0px'}}
                  />
                  {/*<Recaptcha*/}
                    {/*ref={(e) => recaptchaInstance = e}*/}
                    {/*sitekey={sitekey}*/}
                    {/*size="invisible"*/}
                    {/*verifyCallback={this.verifyCallback}*/}
                  {/*/>*/}
                </div>
                <div className="btn-wrapper">
                  <Button onClick={onModalClose} style={{color: 'rgba(0,0,0,0.66)'}}>Cancel</Button>
                  <Button type="submit" style={{color: '#0b559c'}}>Submit</Button>
                </div>
              </form> :
              <div style={{textAlign: 'center'}}>
                <div className={submitted ? "circle-loader load-complete" : "circle-loader"}>
                  <div className="checkmark draw" style={{display: submitted && 'block'}}/>
                </div>
                <div style={{float: submitted ? 'none' : submitting ? 'none' : 'right' }} className="btn-wrapper">
                  <Button onClick={onModalClose} style={{color: 'rgba(0,0,0,0.66)'}}>Close</Button>
                </div>
              </div>
            }
          </div>
        </Element>
      </Modal>
    )
  }
}