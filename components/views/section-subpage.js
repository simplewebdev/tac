import React from 'react';

export default class SectionSubpage extends React.Component {
  render() {
    const { title, sectionSubpage, float, bgColor } = this.props;


    let cardColor =
      bgColor === 'blue' ? 'rgba(0, 83, 160, 0.96)' :
        bgColor === 'red' ? 'rgba(226, 56, 63, 0.96)' : 'rgba(255, 255, 255, 0.96)';

    return (
      <section style={{
      position: 'relative',
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        height: '100%',
        justifyContent: float === 'right' ? 'flex-start' : 'flex-end'
    }}>
        <div className="subpageImg" style={{
            background: `url(${sectionSubpage.image.imgix_url ? sectionSubpage.image.imgix_url + '?w=2000' : 'https://cosmic-s3.imgix.net/3790ec30-7be2-11e8-8a7c-ed8ed617634d-1522.jpg'})`,
              backgroundSize:'cover',
              backgroundPosition:'center',
          }}/>
          <div
            style={{
              background: 'rgba(255, 255, 255, 0.77)',
            }}
            className={float === 'right' ? 'subpage-card subpage-right' : 'subpage-card'}
          >
            <div className="subpage-inner">
              <div>
                <h1>{ title }</h1>
                <div dangerouslySetInnerHTML={{ __html: sectionSubpage.content }}/>
              </div>
            </div>
          </div>
    </section>
    )
  }
}