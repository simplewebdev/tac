import React from 'react';
import ArrowButton from '../buttons/ArrowButton';
import ReactPlayer from 'react-player';

export default class Hero extends React.Component {
  render() {
    const { hero, headingWidth, excerptWidth } = this.props;

    let heroTitle = hero.value;
    let heroImage = `${hero.children[1].imgix_url}?w=2000`;
    let heroExcerpt = hero.children[3].value;
    let heroLink = hero.children[4].value;
    let heroVideo = hero.children[2].url;
    let isVideo = hero.children[2].value !== '';


    return(
      <div style={{ height: '60vh', width: '100%', position: 'relative' }}>
        <div
          className="hero-img"
          style={{
            background: isVideo ? 'rgba(0,0,0,0.15)' : `url(${heroImage})`,
            zIndex: isVideo && 1,
            backgroundSize:'cover',
            backgroundPosition:'center'
          }}
        />
        {
          isVideo &&
          <ReactPlayer
            url={heroVideo}
            playing
            playsinline
            autoPlay
            muted
            loop
            width="100%"
            height="100%"
            className='player'
          />
        }
        <div className="header-container hero-wrapper">
          <div>
          <h1 className="hero-title" style={{color: 'white', maxWidth: headingWidth ? headingWidth : 660}}>{heroTitle}</h1>
          {
            heroExcerpt === '' ? null : <p className="hero-excerpt" style={{maxWidth: excerptWidth ? excerptWidth : 390, margin: '10px 0px'}}>{heroExcerpt}</p>
          }
          {
            heroLink === '' ? null :
              <ArrowButton link={heroLink} color='white' phrase='learn more'/>
          }
          </div>
        </div>
      </div>
    )
  }
}
