import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import { Element } from 'react-scroll';
import Button from '@material-ui/core/Button';
import Recaptcha from 'react-recaptcha';
import Grid from '@material-ui/core/Grid';

// define a variable to store the recaptcha instance
let recaptchaInstance;

// site key
const sitekey = "6Lern2sUAAAAAJL9E2ujWAonZShHS6VSFKg553hk";

const executeCaptcha = function () {
  recaptchaInstance.execute();
};

export default class RequestSales extends React.Component {
  constructor(props) {
    super(props);
  }

  verifyCallback = (response) => {
    if (response) {
      this.props.handleSubmit();
    }
  };

  render() {
    const {
      modalOpen,
      onModalClose,
      onEmailChange,
      emailValue,
      onNameChange,
      nameValue,
      onPhoneChange,
      phoneValue,
      onCompanyChange,
      companyValue,
      onCommentsChange,
      commentsValue,
      submitting,
      submitted,
      salesman
    } = this.props;



    return(
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={modalOpen}
        onClose={onModalClose}
        style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}
      >
        <Element name="description" className="request-quote-element" id="containerElement">
          <div>
            <h1>Start A Conversation</h1>
            { salesman &&
            <div style={{display: 'flex', flexDirection: 'column', margin: 30, width: '100%'}}>
              <h4>
                Sales Representative:
              </h4>
              <b>
                {salesman.name}
              </b>
              <span>
                    {salesman.position}
                  </span>
              <span>
                    {salesman.phone}
                  </span>
              <span>
                    {salesman.email}
                  </span>
            </div>
            }
            { submitting !== true ?
              <form onSubmit={this.props.handleSubmit}>
                <div className={'request-form'} >
                  <TextField
                    id="with-placeholder"
                    label="Name"
                    type="name"
                    placeholder="Name"
                    margin="normal"
                    required
                    value={nameValue}
                    onChange={onNameChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    type="email"
                    label="Email"
                    placeholder="Email"
                    margin="normal"
                    required
                    value={emailValue}
                    onChange={onEmailChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Company"
                    placeholder="Company"
                    margin="normal"
                    required
                    value={companyValue}
                    onChange={onCompanyChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Phone"
                    placeholder="Phone"
                    margin="normal"
                    required
                    value={phoneValue}
                    onChange={onPhoneChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    label="Comments"
                    placeholder="Comments"
                    margin="normal"
                    multiline
                    rows="4"
                    value={commentsValue}
                    onChange={onCommentsChange}
                    style={{margin: '15px 0px'}}
                  />
                  {/*<Recaptcha*/}
                    {/*ref={(e) => recaptchaInstance = e}*/}
                    {/*sitekey={sitekey}*/}
                    {/*size="invisible"*/}
                    {/*verifyCallback={this.verifyCallback}*/}
                  {/*/>*/}
                </div>
                <div className="btn-wrapper">
                  <Button onClick={onModalClose} style={{color: 'rgba(0,0,0,0.66)'}}>Cancel</Button>
                  <Button type="submit" style={{color: '#0b559c'}}>Submit</Button>
                </div>
              </form> :
              <div style={{textAlign: 'center'}}>
                <div className={submitted ? "circle-loader load-complete" : "circle-loader"}>
                  <div className="checkmark draw" style={{display: submitted && 'block'}}/>
                </div>
                <div style={{float: submitted ? 'none' : submitting ? 'none' : 'right' }} className="btn-wrapper">
                  <Button onClick={onModalClose} style={{color: 'rgba(0,0,0,0.66)'}}>Close</Button>
                </div>
              </div>
            }
          </div>
        </Element>
      </Modal>
    )
  }
}