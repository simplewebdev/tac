import React from 'react';
import Link from 'next/link';

export default class SectionHead extends React.Component {
  render() {
    const { sectionHead, float, bgColor, cta } = this.props;

    let sectionHeadBackground = sectionHead[2].imgix_url + '?w=2000';
    let sectionHeadTitle = sectionHead[0].value;
    let sectionHeadExc = sectionHead[1].value;

    let cardColor =
      bgColor === 'blue' ? 'rgba(0, 83, 160, 0.96)' :
      bgColor === 'red' ? 'rgba(226, 56, 63, 0.96)' : 'rgba(255, 255, 255, 0.96)';

    return (
      <section style={{
            position: 'relative',
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
            height: '100%',
            justifyContent: float === 'right' ? 'flex-start' : 'flex-end'
      }}>
        <div
          style={{background: cardColor }}
          className={float === 'right' ? 'banner-card right' : 'banner-card'}
        >
          <div className="banner-card-inner">
            <h1 style={{
              color:  bgColor === 'blue' ? 'white' : bgColor === 'red' ? 'white' : '#4c4b4b'
            }}>{sectionHeadTitle}</h1>
            <div style={{
              color: bgColor === 'blue' ? '#f3f3f3' : bgColor === 'red' ? '#f3f3f3' : '#626262',
            }}>
              {sectionHeadExc}
              { cta && cta }
            </div>
          </div>
        </div>
        <div className="sectionHeadImg" style={{
          background: `url(${sectionHeadBackground})`,
          backgroundSize:'cover',
          backgroundPosition:'center'
        }}/>
      </section>
    )
  }
}