import React from 'react';
import PropTypes from 'prop-types';


export default class CTA extends React.Component {
  render() {
    const {
      cta,
      bgColor,
      logoHeight,
      btnColor,
      onEmailChange,
      emailValue,
      onFirstNameChange,
      firstNameValue,
      onLastNameChange,
      lastNameValue,
      onCompanyChange,
      companyValue,
      submitting,
      submitted,
      failedToSubmit,
      handleSubscribe
    } = this.props;

    return (
      <div style={{ backgroundColor: bgColor ? bgColor : '#0b559c' }}>
        <div className="cta-wrapper container">
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <img style={{ marginBottom: 35 }} src={cta.children[1].url} alt="" height={logoHeight} />
            <div style={{ padding: 20 }}>
              <div style={{ display: 'flex', alignItems: 'center', marginBottom: 10 }}>
                <img style={{ marginRight: 20 }} height="30px" src={"https://cosmic-s3.imgix.net/670c3f40-abe0-11e8-97a6-57aff3ccd6d6-call-answer%20(1).svg"} />
                <span style={{ color: 'white', fontSize: 16 }}>
                  <a style={{ color: 'white' }} href="tel:800-375-FUEL">SALES: (800) 375-FUEL</a>
                  <br />
                  <a style={{ color: 'white' }} href="tel:800-808-6500">SUPPLY & LOGISTICS: (800) 808-6500</a>
                </span>
              </div>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <img style={{ marginRight: 20 }} height="30px" src={"https://cosmic-s3.imgix.net/406190c0-abe0-11e8-bc2e-5788e85c3092-facebook-placeholder-for-locate-places-on-maps%20(1).svg"} />
                <span style={{ color: 'white', fontSize: 16 }}>
                  <a style={{ color: 'white' }} href="https://goo.gl/maps/wzS9rsqjyaw" target="_blank">100 Crescent Court, Suite 1600 <br /> Dallas, TX 75201</a>
                </span>
              </div>
            </div>
          </div>
          <div className="cta-content">
            {submitted ? <div><h2 style={{ textAlign: 'center' }}>Subscription Successful. Thank you for signing up to receive Market Talk emails!</h2></div> :
              submitting ? <div><h2 style={{ textAlign: 'center' }}>Subscribing to Market Talk...</h2></div> :
                failedToSubmit ? <div><h2 style={{ textAlign: 'center' }}>Subscription failed... please retry</h2></div> :
                  <div>
                    <h2>{cta.children[0].value}</h2>
                    <p>{cta.children[2].value}</p>
                  </div>
            }
            {submitting !== true ?
              <form onSubmit={handleSubscribe} style={{ width: '100%' }}>
                <label>
                  <input type="name" placeholder="First Name" value={firstNameValue} onChange={onFirstNameChange} required />
                  <span>First Name</span>
                </label>
                <label className={'last-name-field'}>
                  <input type="last-name" placeholder="Last Name" value={lastNameValue} onChange={onLastNameChange} required />
                  <span>Last Name</span>
                </label>
                <label style={{ width: '100%' }}>
                  <input type="company" placeholder="Company" value={companyValue} onChange={onCompanyChange} required />
                  <span>Company</span>
                </label>
                <div style={{ width: '100%' }}>
                  <label style={{ width: '64%' }}>
                    <input type="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" placeholder="Email" value={emailValue} onChange={onEmailChange} required />
                    <span>Email</span>
                  </label>
                  <input style={{ background: btnColor ? btnColor : '#e2383f' }} type="submit" value="SUBSCRIBE" />
                </div>
              </form> :
              <div style={{ textAlign: 'center' }}>
                <div className={submitted ? "circle-loader load-complete" : "circle-loader"}>
                  <div className="checkmark draw" style={{ display: submitted && 'block' }} />
                </div>
              </div>
             }
          </div>
        </div>
      </div>
    )
  }
}