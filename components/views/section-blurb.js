import React from 'react';
import Grid from '@material-ui/core/Grid';
import ArrowButton from '../buttons/ArrowButton';

export default class SectionBlurb extends React.Component {
  render() {
    const { sectionBlurb, logoHeight } = this.props;
    return (
      <section className="container">
        <div className="section-blurb">
          <Grid container spacing={40}>
            <Grid item sm={12} md={4} style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
              <img style={{margin: 35}} src={sectionBlurb[0].url} alt=""  height={logoHeight ? logoHeight : "50px"} />
            </Grid>
            <Grid item sm={12} md={4} style={{display: 'flex', justifyContent: 'center', flexDirection: 'column'}}>
              <em style={{fontWeight: 600, letterSpacing: '0.8px', lineHeight: '28px'}}>{sectionBlurb[2].value}</em>
              { sectionBlurb[3].value ? <ArrowButton link={sectionBlurb[3].value} color='#e2383f' phrase='Request a Quote'/> : null }
            </Grid>
            <Grid item sm={12} md={4} style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
              <p style={{fontSize: '14px', lineHeight: '21px',letterSpacing: '1px'}}>
                {sectionBlurb[1].value}
              </p>
            </Grid>
          </Grid>
        </div>
      </section>
    )
  }
}