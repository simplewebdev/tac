import React from 'react';
import Link from 'next/link';
import ArrowButton from '../../components/buttons/ArrowButton';

export default class SectionHead extends React.Component {
  render() {
    const { sectionHead, float, bgColor, cta } = this.props;

    console.log(sectionHead)
    let sectionHeadTitle = sectionHead[0].value;
    let sectionHeadExc = sectionHead[1].value;
    let sectionHeadExc2 = sectionHead[2].value;
    let sectionHeadBackground = sectionHead[3].imgix_url + '?w=2000';

    let cardColor =
      bgColor === 'blue' ? 'rgba(0, 83, 160, 0.96)' :
      bgColor === 'red' ? 'rgba(226, 56, 63, 0.96)' : 'rgba(255, 255, 255, 0.96)';

    return (
      <section style={{
            position: 'relative',
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
            height: '100%',
            justifyContent: float === 'right' ? 'flex-start' : 'flex-end'
      }}>
        <div
          style={{background: 'cardColor',top:'18%' }}
          className={float === 'right' ? 'banner-card right' : 'banner-card'}
        >
          <div style={{paddingBottom:"20px",paddingTop:"20px"}} className="banner-card-inner">
            <h1 style={{
              color:  bgColor === 'blue' ? 'white' : bgColor === 'red' ? 'white' : '#4c4b4b'
            }}>{sectionHeadTitle}</h1>
            <div style={{
              color: bgColor === 'blue' ? '#f3f3f3' : bgColor === 'red' ? '#f3f3f3' : '#626262',
              width:'100%'
            }}>
              {sectionHeadExc}
              <br/>
              <br/>
              {sectionHeadExc2}
              <a href="tac-energy">
              <ArrowButton color='#e2383f' phrase='EXPLORE TACENERGY.COM' />
              </a>
              { cta && cta }
            </div>
          </div>
        </div>
        <div className="sectionHeadImg" style={{
          background: `url(${sectionHeadBackground})`,
          backgroundSize:'cover',
          backgroundPosition:'right',
          marginBottom:'200px'
       }}/>
      </section>
    )
  }
}