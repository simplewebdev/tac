import React from 'react';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import { Element } from 'react-scroll';
import Button from '@material-ui/core/Button';
import Recaptcha from 'react-recaptcha';

// define a variable to store the recaptcha instance
let recaptchaInstance;

// site key
const sitekey = "6Lern2sUAAAAAJL9E2ujWAonZShHS6VSFKg553hk";

const executeCaptcha = function () {
  recaptchaInstance.execute();
};

export default class RequestEmptyLegs extends React.Component {
  constructor(props) {
    super(props);
  }


  verifyCallback = (response) => {
    if (response) {
      this.props.handleSubmit();
    }
  };

  render() {
    const {
      modalOpen,
      onModalClose,
      onEmailChange,
      emailValue,
      onNameChange,
      nameValue,
      onPhoneChange,
      phoneValue,
      onCommentsChange,
      commentsValue,
      submitting,
      submitted,
      failedToSumbit,
    } = this.props;

    return(
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={modalOpen}
        onClose={onModalClose}
        style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}
      >
        <Element name="description" className="email-update-request" id="containerElement">
          <div>
            <h1>EMAIL SIGNUP</h1>
            { submitting !== true ?
              <form onSubmit={this.props.handleSubmit}>
                <div className={'request-form'} >
                  <TextField
                    id="with-placeholder"
                    label="Name"
                    type="name"
                    placeholder="Name"
                    margin="normal"
                    required
                    value={nameValue}
                    onChange={onNameChange}
                    style={{margin: '15px 0px'}}
                  />
                  <TextField
                    id="with-placeholder"
                    type="email"
                    label="Email"
                    placeholder="Email"
                    margin="normal"
                    required
                    value={emailValue}
                    onChange={onEmailChange}
                    style={{margin: '15px 0px'}}
                  />
                </div>
                <div className="btn-wrapper">
                  <Button onClick={onModalClose} style={{color: 'rgba(0,0,0,0.66)'}}>Cancel</Button>
                  <Button type="submit" style={{color: '#0b559c'}}>Submit</Button>
                </div>
              </form> :
              <div style={{textAlign: 'center'}}>
                <div className={submitted ? "circle-loader load-complete" : "circle-loader"}>
                  <div className="checkmark draw" style={{display: submitted && 'block'}}/>
                </div>
                <div style={{float: submitted ? 'none' : submitting ? 'none' : 'right' }} className="btn-wrapper">
                  <Button onClick={onModalClose} style={{color: 'rgba(0,0,0,0.66)'}}>Close</Button>
                </div>
              </div>
            }
          </div>
        </Element>
      </Modal>
    )
  }
}