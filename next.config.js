const webpack = require('webpack');
const { parsed: localEnv } = require('dotenv').config();

module.exports = {
  webpack: function (cfg) {
    //cfg.plugins.push(new webpack.EnvironmentPlugin(localEnv));
    const originalEntry = cfg.entry;
    if (process.env.NODE_ENV === 'production') {
      cfg.entry = async () => {
        const entries = await originalEntry();

        if (entries['main.js']) {
          entries['main.js'].unshift('./utils/polyfills.js')
        }

        return entries
      };
    }

    return cfg
  },
  publicRuntimeConfig: {
    WRITE_KEY: process.env.COSMIC_WRITE_KEY
  }
};