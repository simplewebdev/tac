import bucket from "../config";

function addFormSubmission(title, html) {
  const params = {
    "title": title,
    "type_slug": "form-submissions",
    "content": html,
    "options": {
      "slug_field": false,
      "content_editor": true
    }
  };
    return bucket.addObject(params);
}

export default {
  addFormSubmission
}