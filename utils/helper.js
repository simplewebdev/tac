export const mapGlobals = (globals) => {
  let response = {};
  globals.map((global) => {
    switch(global.slug){
      case 'contact-form':
        response.contact_form = global;
        break;
      case 'header':
        response.header = global;
        break;
      case 'nav':
        response.nav = global;
        break;
      case 'social':
        response.social = global;
        break;
      case 'contact-info':
        response.contact_info = global;
        break;
      case 'footer':
        response.footer = global;
        break;
    }
  });
  return response;
};

export const mapAirLocations = (locations) => {
  let response = {};
  locations.map((location) => {
    switch(location.slug){
      case 'ama':
        response.tac_air_ama = location;
        break;
      case 'apa':
        response.tac_air_apa = location;
        break;
      case 'bdl':
        response.tac_air_bdl = location;
        break;
      case 'fsm':
        response.tac_air_fsm = location;
        break;
      case 'lex':
        response.tac_air_lex = location;
        break;
      case 'lit':
        response.tac_air_lit = location;
        break;
      case 'oma':
        response.tac_air_oma = location;
        break;
      case 'pvu':
        response.tac_air_pvu = location;
        break;
      case 'rdu':
        response.tac_air_rdu = location;
        break;
      case 'shv':
        response.tac_air_shv = location;
        break;
      case 'slc':
        response.tac_air_slc = location;
        break;
      case 'sus':
        response.tac_air_sus = location;
        break;
      case 'txk':
        response.tac_air_txk = location;
        break;
      case 'tys':
        response.tac_air_tys = location;
        break;
      case 'dal':
        response.tac_air_dal = location;
        break;
    }
  });
  return response;
};

export const mapPage = (data) => {
  const page = {};
  data.metafields.map((obj) => {
    if(obj.key === 'cards'){
      page[obj.key] = obj.children
    } else
      page[obj.key] = obj;
  });
  return page;
};