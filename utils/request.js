import bucket from "../config";

function getGlobals() {
  const params = {
    type_slug: 'globals'
  };
  return bucket.getObjectsByType(params);
}

function getMilitaryMedia() {
  const params = {
    folder: 'tac-air-military'
  };
  return bucket.getMedia(params);
}

function getPages() {
  const params = {
    type_slug: 'pages'
  };
  return bucket.getObjectsByType(params);
}

function getCharterFleet(fleetLimit, sort) {
  const params = {
    type: 'charter-aircrafts',
    limit: fleetLimit,
    sort: sort
  };
  return bucket.getObjects(params);
}

function getAircraftsForSale(limit, sort) {
  const params = {
    type: 'for-sale-aircrafts',
    limit: limit,
    sort: sort
  };
  return bucket.getObjects(params);
}

function getAirLocations() {
  const params = {
    type_slug: 'air-locations'
  };
  return bucket.getObjectsByType(params);
}

function getTACEnergyContacts() {
  const params = {
    type: 'energy-contacts'
  };
  return bucket.getObjects(params);
}

function getKeystoneContacts() {
  const params = {
    type: 'keystone-contacts'
  };
  return bucket.getObjects(params);
}

function getPosts(postLimit) {
  const params = {
    type_slug: 'newsroom-posts',
    limit: postLimit
  };
  return bucket.getObjectsByType(params);
}

function filterPostByCat(category) {
  const params = {
    type: 'newsroom-posts',
    filters: {
      connected_to: category === 'tac-the-arnold-companies-news' ? '5b554e23a359c52e8548f99c' :
        category === 'keystone-aviation-news' ? '5b55eebd61759039675456e3' :
          category === 'tac-energy-news' ? '5b55ee7bc4da3f31bda1ce6f' :
            category === 'market-talk-news' ? '5bed72b7a71e805136ff0c8d' :
              category === 'tac-air-news' ? '5b554c7bc759e5363d0fb1f3' : null
    }
  };
  return bucket.getObjects(params);
}

function getObject(slug, status, revision) {
  const params = {
    slug: slug,
    status: status,
    revision: revision
  };
  return bucket.getObject(params);
}


function getObjects() {
  const params = {
  };
  return bucket.getObjects(params);
}

export default {
  getGlobals,
  getMilitaryMedia,
  getPages,
  getObject,
  getObjects,
  getCharterFleet,
  getAircraftsForSale,
  getAirLocations,
  getPosts,
  filterPostByCat,
  getTACEnergyContacts,
  getKeystoneContacts
}