import _ from 'lodash';
import moment from 'moment';

const filterPostsDateYear = (posts) => {
  const archives = _.map(posts, (post) => {
    const date = moment(post.published_at, 'YYYY/MM/DD');
    return {
      month: date.format('MMMM'),
      year: date.format('Y')
    }
  });
  
  return _.uniqBy(archives, (e) => e.year && e.month);
};

export default {
  filterPostsDateYear
};