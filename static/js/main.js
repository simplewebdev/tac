$(document).ready(function() {

  let qString = document.location.href.split("?");

  let param = "?" + qString[1];

  if (qString[1]){
    $("a").each(function() {
      let val = $(this).attr("href");
      if (val.indexOf("#") === -1) {
        $(this).attr("href", val+param);
      }});
  }

  $("#gallery-frame").contents().find("video").attr("style","max-height: 600px");

  $(function () {
    $(document).scroll(function () {
      let $nav = $(".header");
      $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });
  });

  $(function() {
    function slideMenu() {
      let menuContainer = $("#menu-container .menu-list");
      let activeState = menuContainer.hasClass("active");
      menuContainer.animate(
        {
          right: activeState ? "0%" : "-100%",
        },
        400
      );
      menuContainer.toggleClass("absolute");
    }

    $(".menu-link").click(function(event) {
      event.stopPropagation();
      $("#hamburger-menu").toggleClass("open");
      $("#menu-container .menu-list").toggleClass("active");
      slideMenu();

      $("body").toggleClass("overflow-hidden");
    });


    $("#menu-wrapper").click(function(event) {
      event.stopPropagation();
      if($(window).height() <= 600) {
        $("#menu-container .menu-list").css("height", `${$(window).height()}` - 100);
      }

      $("#hamburger-menu").toggleClass("open");
      $("#menu-container .menu-list").toggleClass("active");
      slideMenu();

      $("body").toggleClass("overflow-hidden");
    });

    $(".menu-list").find(".accordion-toggle").click(function() {
      $(this).next().toggleClass("open").slideToggle("fast");
      $(this).toggleClass("active-tab").find(".menu-link").toggleClass("active");

      $(".menu-list .accordion-content")
        .not($(this).next())
        .slideUp("fast")
        .removeClass("open");
      $(".menu-list .accordion-toggle")
        .not(jQuery(this))
        .removeClass("active-tab")
        .find(".menu-link")
        .removeClass("active");
    });
  });
});

