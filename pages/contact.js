import React from 'react';
import PageNotFound from '../components/views/404';
import Request from '../utils/request';
import { mapPage } from "../utils/helper";
import Head from 'next/head';
import Grid from '@material-ui/core/Grid';
import Post from '../utils/post';

export default class ContactPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject('contact-us', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }

    return { page }
  }

  constructor(props) {
    super(props);
    this.state = {
      page: props.page,
      name: "",
      email: "",
      phone: "",
      message: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    };

  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/tac-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'TAC Contact Form - Contact Us';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>`
          + `<b>Sent to: marketing@tacenergy.com</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePhoneChange = (event) => {
    this.setState({ phone: event.target.value });
  };

  handleMessageChange = (event) => {
    this.setState({ message: event.target.value });
  };


  render() {
    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'home contact container'}>
            <Head>
              <title>TAC - Contact Us</title>
            </Head>
            <h1 style={{ fontWeight: 500, paddingTop: 50, }}>Contact Us</h1>
            <Grid container spacing={8} >
              <Grid item sm={12} md={6}>
                <div style={{ padding: '45px 0' }}>
                  <h2 style={{ fontWeight: 500 }}>GENERAL INQUIRIES</h2>
                  <h3>
                    <a href="tel:214-884-2660">CALL 214-884-2660</a>
                  </h3>
                  <a href="https://goo.gl/maps/QAqSaPhRQ322"
                    target="_blank"
                    style={{ display: 'inline-block', textDecoration: 'none' }}>
                    <p>The Arnold Companies
                    <br />100 Crescent Court
                      <br />Suite 1600
                        <br />Dallas, TX 75201</p>
                  </a>
                </div>
              </Grid>
              <Grid item sm={12} md={6}>
                <h2 style={{ color: '#333', margin: '10px 60px', padding: 0, textTransform: 'uppercase', letterSpacing: 1, fontWeight: 100, fontSize: 35, paddingTop: 50, textAlign: 'center', maxWidth: 800 }}>
                  {this.state.submitted ? 'Thank you for contacting TAC - The Arnold Companies. We will reply to your message as soon as possible.'
                    : this.state.submitting ? 'Submitting Message...'
                      : this.state.failedSubmit ? 'Failed To Send Message'
                        : null}
                </h2>
                {this.state.submitting !== true ?
                  <form onSubmit={this.handleSubmit.bind(this)}>
                    <div>
                      <div >
                        <label>
                          <input type="text" placeholder="Name" value={this.state.name} onChange={this.handleNameChange.bind(this)}
                            required />
                          <span>Name</span>
                        </label>
                        <label>
                          <input type="email" placeholder="Email" value={this.state.email} onChange={this.handleEmailChange.bind(this)}
                            required />
                          <span>Email</span>
                        </label>
                        <label>
                          <input type="phone" placeholder="Phone" value={this.state.phone}
                            onChange={this.handlePhoneChange.bind(this)} required />
                          <span>Phone</span>
                        </label>
                      </div>
                      <div >
                        <label>
                          <textarea id="message" placeholder="Your Message" rows="8" value={this.state.message}
                            onChange={this.handleMessageChange.bind(this)} required />
                          <span>Message</span>
                        </label>
                      </div>
                    </div>
                    <div style={{ alignItems: 'flex-start', justifyContent: 'flex-start', display: 'flex', paddingBottom: 50 }}>
                      <input style={{ background: '#e2383f', margin: 5 }} type="submit" value="SUBMIT" />
                    </div>
                  </form> :
                  <div style={{ textAlign: 'center' }}>
                    <div className={this.state.submitted ? "circle-loader load-complete" : "circle-loader"}>
                      <div className="checkmark draw" style={{ display: this.state.submitted && 'block' }} />
                    </div>
                  </div>
                }
              </Grid>
            </Grid>
          </article>
        )
    );
  }
}