import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import { mapPage } from "../utils/helper";
import ContactCTA from '../components/views/contact-cta';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Head from 'next/head';
import ArrowButton from '../components/buttons/ArrowButton';
import RequestModal from '../components/views/keystone-emptylegs-request';
import RequestModalReceiveEmail from '../components/views/keystone-emptylegs-email-update-request';
import Post from '../utils/post';

export default class KeystoneEmptyLegsPage extends React.Component {

  static async getInitialProps({req, query}) {
    let page;

    try {
      const pageResponse = await Request.getObject('empty-legs', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }

    return { page }
  }

  constructor(props){
    super(props);
    this.state = {
      page: props.page,
      cta: props.page.cta,
      about: props.page.about,
      name: "",
      email: "",
      phone: "",
      message: "",
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalOpenEmailUpdate: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      submitting: false,
      submitted: false,
      failedSubmit: false,
    }
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({submitting: true});
    event.preventDefault();

    fetch('/api/tac-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
      })
    }).then((res)=>{
      if (res.status === 200){
        let title = 'Keystone Aviation Contact - Empty Legs';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({submitted: true});
      }else {
        this.setState({submitted: false, submitting: false, failedSubmit: true});
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({name: event.target.value});
  };

  handleEmailChange = (event) => {
    this.setState({email: event.target.value});
  };

  handlePhoneChange = (event) => {
    this.setState({phone: event.target.value});
  };

  handleMessageChange = (event) => {
    this.setState({message: event.target.value});
  };

  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
    })
  };

  handleModalSubmit = (event) => {
    this.setState({modalSubmitting: true});
    event.preventDefault();

    fetch('/api/keystone-emptylegs-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.modalEmail,
        name: this.state.modalName,
        phone: this.state.modalPhone,
        message: this.state.modalComments
      })
    }).then((res)=>{
      if (res.status === 200){
        let title = 'Keystone Quote Request - Empty Legs';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>` 
          + `<b>Sent to: marketing@tacenergy.com, ops@keystoneaviation.com, dchamberlain@keystoneaviation.com</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({modalSubmitted: true});
      }else {
        this.setState({modalSubmitted: false, modalSubmitting: false, modalFailedSubmit: true});
      }
    })
  };

  handleModalSubmitForEmailUpdate = (event) => {
    this.setState({modalSubmitting: true});
    event.preventDefault();

    fetch('/api/keystone-emptylegs-request-receive-email', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.modalEmail,
        name: this.state.modalName,
      })
    }).then((res)=>{
      if (res.status === 200){
        let title = 'Keystone Receive Email Update - Empty Legs';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.modalName}</b><br><br>`
          + `<b>Name: ${this.state.modalName}</b><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Sent to: dchamberlain@keystoneaviation.com</b><br>`+
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({modalSubmitted: true});
      }else {
        this.setState({modalSubmitted: false, modalSubmitting: false, modalFailedSubmit: true});
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({modalName: event.target.value});
  };

  handleModalPhoneChange = (event) => {
    this.setState({modalPhone: event.target.value});
  };

  handleModalEmailChange = (event) => {
    this.setState({modalEmail: event.target.value});
  };

  handleModalCommentsChange = (event) => {
    this.setState({modalComments: event.target.value});
  };

  handleOnClickArrow = () => {
    this.setState({modalOpen: true});
  };
  handleOnClickArrowReceiveEmail = () => {
    this.setState({modalOpenEmailUpdate: true});
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false
    });
    this.setState({modalOpen: !this.state.modalOpen});
  };
  handleModalCloseEmailUpdate = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false
    });
    this.setState({modalOpenEmailUpdate: !this.state.modalOpenEmailUpdate});
  };

  render() {
    let sectionHead=this.state.about.children;
    let sectionHeadBackground = sectionHead[3].imgix_url + '?w=2000';
    let sectionHeadTitle = sectionHead[0].value;
    let sectionHeadExc = sectionHead[2].value;
    let float = 'right';
    let bgColor = 'white';

    let cardColor =
      bgColor === 'blue' ? 'rgba(0, 83, 160, 0.96)' :
        bgColor === 'red' ? 'rgba(226, 56, 63, 0.96)' : 'rgba(255, 255, 255, 0.96)';

 
    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
        <article className={'keystone'}>
          <Head>
            <title>{`Keystone Aviation - Empty Legs`}</title>
          </Head>
          <section style={{
            position: 'relative',
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
            height: '100%',
            justifyContent: float === 'right' ? 'flex-start' : 'flex-end'
          }}>
            <div
              style={{background: cardColor}}
              className={float === 'right' ? 'banner-card right' : 'banner-card'}
            >
              <div className="banner-card-inner">
                <h1 style={{
                  color:  bgColor === 'blue' ? 'white' : bgColor === 'red' ? 'white' : '#4c4b4b'
                }}>{sectionHeadTitle}</h1>
                <div style={{
                  color: bgColor === 'blue' ? '#f3f3f3' : bgColor === 'red' ? '#f3f3f3' : '#626262',
                }}>
                  {sectionHeadExc}
                  <a onClick={this.handleOnClickArrow.bind(this)} style={{ marginTop: 15}}>
                    <ArrowButton color='#e2383f' phrase='Request A Quote'/>
                  </a>
                </div>
              </div>
            </div>
            <div className="sectionHeadImg" style={{
              background: `url(${sectionHeadBackground})`,
              backgroundSize:'cover',
              backgroundPosition:'center',
              height: '70vh'
            }}/>
          </section>
          <section style={{marginTop: 100}} className={'empty-legs container'}>
            <h1 style={{marginBottom: 5}}>Empty Legs Currently Available &ndash; Call 888.600.1070</h1>
            <a onClick={this.handleOnClickArrowReceiveEmail.bind(this)} style={{ marginTop: 15}}>
                    <ArrowButton color='#e2383f' phrase='Receive Email Updates'/>
                  </a>
            <p style={{fontSize: 12}}>Occasionally, Keystone Aviation will book a charter trip that requires a reposition of the aircraft without passengers. These empty leg charter trips are then offered to the general public and other private charter operators/brokers at discounted rates.</p>
            <Paper style={{overflowX: 'auto', margin: '50px 0px'}}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Date</TableCell>
                    <TableCell>Aircraft</TableCell>
                    <TableCell>Departure</TableCell>
                    <TableCell>Arrival</TableCell>
                    <TableCell numeric>Seats</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.page.empty_legs.children.map((emptyLeg, index) => {
                    let date = new Date(emptyLeg.children[0].value);
                    return (
                      <TableRow key={index}>
                        <TableCell component="th" scope="row">
                          {/* {date.getMonth() + 1 + '/' + date.getUTCDate() + '/' + date.getFullYear()} */}
                          {emptyLeg.children[0].value}
                        </TableCell>
                        <TableCell>{emptyLeg.children[1].value}</TableCell>
                        <TableCell>{emptyLeg.children[2].value}</TableCell>
                        <TableCell>{emptyLeg.children[3].value}</TableCell>
                        <TableCell numeric>{emptyLeg.children[4].value} seats</TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </Paper>
          </section>
          <section id="contact" style={{clear: 'both'}}>
          <ContactCTA
            cta={this.state.cta}
            emailValue={this.state.email}
            handleSubmit={this.handleSubmit.bind(this)}
            onEmailChange={this.handleEmailChange.bind(this)}
            nameValue={this.state.name}
            onNameChange={this.handleNameChange.bind(this)}
            phoneValue={this.state.phone}
            onPhoneChange={this.handlePhoneChange.bind(this)}
            messageValue={this.state.message}
            onMessageChange={this.handleMessageChange.bind(this)}
            bgColor='#8b8b8b'
            btnColor='#e2383f'
            submitted={this.state.submitted}
            submitting={this.state.submitting}
            failedToSumbit={this.state.failedSubmit}
          />
          </section>
          <RequestModal
            modalOpen={this.state.modalOpen}
            onModalClose={this.handleModalClose.bind(this)}
            emailValue={this.state.modalEmail}
            handleSubmit={this.handleModalSubmit.bind(this)}
            onEmailChange={this.handleModalEmailChange.bind(this)}
            NameValue={this.state.modalName}
            onNameChange={this.handleModalNameChange.bind(this)}
            phoneValue={this.state.modalPhone}
            onPhoneChange={this.handleModalPhoneChange.bind(this)}
            commentsValue={this.state.modalComments}
            onCommentsChange={this.handleModalCommentsChange.bind(this)}
            submitted={this.state.modalSubmitted}
            submitting={this.state.modalSubmitting}
            failedToSumbit={this.state.modalFailedSubmit}
          />
           <RequestModalReceiveEmail
            modalOpen={this.state.modalOpenEmailUpdate}
            onModalClose={this.handleModalCloseEmailUpdate.bind(this)}
            emailValue={this.state.modalEmail}
            handleSubmit={this.handleModalSubmitForEmailUpdate.bind(this)}
            onEmailChange={this.handleModalEmailChange.bind(this)}
            NameValue={this.state.modalName}
            onNameChange={this.handleModalNameChange.bind(this)}
            phoneValue={this.state.modalPhone}
            onPhoneChange={this.handleModalPhoneChange.bind(this)}
            commentsValue={this.state.modalComments}
            onCommentsChange={this.handleModalCommentsChange.bind(this)}
            submitted={this.state.modalSubmitted}
            submitting={this.state.modalSubmitting}
            failedToSumbit={this.state.modalFailedSubmit}
          />
        </article>
      )
    );
  }
}