import React from 'react'
import Request from '../utils/request';
import Masonry from 'react-masonry-component';
import Grid from '@material-ui/core/Grid';
import Sidebar from '../components/shared/sidebar';
import Head from 'next/head';
import ArrowButton from '../components/buttons/ArrowButton';
import _ from 'lodash';
import Archives from '../utils/archives';
import moment from 'moment';
import timezone from 'moment-timezone';

export default class NewsroomArchivePage extends React.Component {

  static async getInitialProps({ req, query }) {
    let posts;
    let page;
    let newsroomMeta;
    let sidebarPosts = [];

    try {
      const ResponseAllPosts = await Request.getPosts(null);
      const ResponseSidebarPosts = await Request.getPosts(4);
      const ResponseNewsroomMeta = await Request.getObject('news-and-views');
      posts = ResponseAllPosts.objects;
      sidebarPosts = ResponseSidebarPosts.objects;
      newsroomMeta = ResponseNewsroomMeta.object;
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return {
      posts,
      sidebarPosts,
      newsroomMeta,
      query
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      posts: props.posts,
      sidebarPosts: props.sidebarPosts,
      newsroom: props.newsroomMeta,
      archiveMonth: props.month,
      query: props.query,
      pageOption: "archieve",
      loading: false,
      filteredPosts: [],
      archives: []
    }
    this.functionCheck = this.functionCheck.bind(this);
  }

  componentDidMount() {
    const { query, posts } = this.state;

    const filteredPosts = _.filter(posts, (item) => {
      const date = moment(item.published_at, 'YYYY/MM/DD');
      return (date.format('MMMM').toLowerCase() === query.month) && (date.format('Y') === query.year);
    });

    this.setState(state => {
      return {
        filteredPosts: filteredPosts,
        archives: Archives.filterPostsDateYear(posts)
      };
    });
  }

  async functionCheck(pagename) {
    this.setState(state => {
      return { loading: true }
    });
    if (pagename == "media") {
      this.setState(state => {
        return { pageOption: pagename, loading: false }
      });
    }
    else if (pagename != "archieve") {
      let AllPostData;
      try {
        AllPostData = await Request.filterPostByCat(pagename);
      }
      catch (e) {
        page =
          {
            title: 'Page not found',
            component: '404',
          }
      }
      this.setState(state => {
        return { posts: AllPostData.objects, pageOption: pagename, loading: false }
      });
    }
  }


  render() {
    let heroTitle = this.state.newsroom.metadata.hero_title;
    let heroImage = this.state.newsroom.metadata.hero_image.imgix_url + '?w=2000';
    let heroExcerpt = '';
    let heroLink = '';
    let heroVideo = '';
    let isVideo = false;
    let childElements;

    if (this.state.pageOption == "media") {
      childElements = (
        <div>
          <Head>
            <title>{`TAC - Media Graphics`}</title>
          </Head>
          <h2 style={{ fontWeight: 600, marginBottom: 25 }}>Click on a graphic below to download in PDF format </h2>
          <a target={"_blank"} href={'https://cosmic-s3.imgix.net/30210740-966e-11e8-ab08-31f1ed4cd97d-tac-full-logo.svg'}>
            <img style={{ margin: 10, marginBottom: 50 }} width={'45%'} src={'https://cosmic-s3.imgix.net/30210740-966e-11e8-ab08-31f1ed4cd97d-tac-full-logo.svg'} />
          </a>
          <a target={"_blank"} href={'https://s3-us-west-2.amazonaws.com/cosmicjs/04a36f60-9fef-11e8-a649-bbc58f269540-Tac_Energy_Logo.pdf'}>
            <img style={{ margin: 10, marginBottom: 50 }} width={'45%'} src={'https://cosmic-s3.imgix.net/9376e000-758d-11e8-9c42-75c5f9571e6d-TACenergy.png'} />
          </a>
          <a target={"_blank"} href={'https://s3-us-west-2.amazonaws.com/cosmicjs/079cd940-9fef-11e8-baa7-297ee0900870-TAC_Air_Logo.pdf'}>
            <img style={{ margin: 10, marginBottom: 50 }} width={'45%'} src={'https://cosmic-s3.imgix.net/937384a0-758d-11e8-ac9f-85d733f58489-TAC%20Air.png'} />
          </a>
          <a target={"_blank"} href={'https://cosmic-s3.imgix.net/93a67b80-758d-11e8-9c42-75c5f9571e6d-Keystone%20Aviation.png'}>
            <img style={{ margin: 10, marginBottom: 50 }} width={'45%'} src={'https://cosmic-s3.imgix.net/93a67b80-758d-11e8-9c42-75c5f9571e6d-Keystone%20Aviation.png'} />
          </a>
        </div>
      );
    }
    else if (this.state.pageOption == "archieve") {

      childElements = this.state.filteredPosts.map((element, index) => {
        let postDate;
        if (element.metadata.date_posted) {
          postDate = moment(element.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
        } else {
          postDate = moment(element.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
        }
        let catLength = element.metadata.category.length;

        return (
          <article key={index} style={{ padding: 10 }} className="post-element-class">
            <Head>
              <title>{`TAC - Newsroom`}</title>
            </Head>
            <div className="card">
              {element.metadata.featured_image.url !== 'https://s3-us-west-2.amazonaws.com/cosmicjs/' &&
                <a href={`/news-and-views/${element.slug}`}>
                  <img width="100%" src={element.metadata.featured_image.imgix_url + '?w=2000'} sizes="(max-width: 1024px) 100vw, 1024px" />
                </a>
              }
              <div style={{ paddingLeft: '10%', paddingRight: '10%', paddingTop: 32, marginTop: '-5px' }}>
                <div className="post-date">{postDate}</div>
                <a href={`/news-and-views/${element.slug}`}><h2 className="post-title">{element.title}</h2></a>
                <div className="post-teaser">{element.metadata.teaser}</div>
                <div className="post-categories">
                  {element.metadata.category.map((cat, index) => {
                    if (catLength === index + 1) {
                      return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}</a>)
                    } else {
                      return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}, </a>)
                    }
                  })}
                </div>
              </div>
            </div>
          </article>
        );
      });

    }
    else {
      childElements = this.state.posts.map((element, index) => {
        let postDate;
        if (element.metadata.date_posted) {
          postDate = moment(element.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
        } else {
          postDate = moment(element.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
        }
        let catLength = element.metadata.category.length;
        return (
          <article key={index} style={{ padding: 10, cursor: 'pointer' }} className="post-element-class">
            <Head>
              <title>{`TAC - ${this.state.pageOption}`}</title>
            </Head>
            <a href={`/news-and-views/${element.slug}`} style={{ display: 'block' }}>
              <img width="100%" style={{ verticalAlign: 'middle' }} src={`${element.metadata.featured_image.imgix_url}?w=500`} />
            </a>
            <div style={{ paddingLeft: '10%', paddingRight: '10%', paddingTop: 32, marginTop: '-5px' }} className="card">
              <div className="post-date">{postDate}</div>
              <a href={`/news-and-views/${element.slug}`}><h2 className="post-title">{element.title}</h2></a>
              <div className="post-teaser">{element.metadata.teaser}</div>
              <div className="post-categories">
                {element.metadata.category.map((cat, index) => {
                  if (catLength === index + 1) {
                    return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}</a>)
                  } else {
                    return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}, </a>)
                  }
                })}
              </div>
            </div>
          </article>
        );
      });
    }
    return (
      <div>
        <div class="loading_page" style={this.state.loading ? { display: "block" } : { display: "none" }}>
          <div class="loading-img"><img src="../static/images/loading.gif" alt="" /></div>
        </div>
        <div style={{ height: '60vh', width: '100%', position: 'relative' }}>
          <div
            className="hero-img"
            style={{
              //display: isVideo && 'none',
              background: isVideo ? 'rgba(0,0,0,0.15)' : `url(${heroImage})`,
              //zIndex: isVideo && 1,
              backgroundSize: 'cover',
              backgroundPosition: '50% 35%'
            }}
          />
          {
            isVideo &&
            <ReactPlayer
              url={heroVideo}
              playing
              playsinline
              autoPlay
              muted
              loop
              width="100%"
              height="100%"
              className='player'
            />
          }
          <div className="header-container hero-wrapper">
            <div>
              <h1 className="hero-title" style={{ color: 'white', maxWidth: 660 }}>{heroTitle}</h1>
              {
                heroExcerpt === '' ? null :
                  <p className="hero-excerpt" style={{ maxWidth: 390, margin: '10px 0px' }}>{heroExcerpt}</p>
              }
              {
                heroLink === '' ? null :
                  <ArrowButton link={heroLink} color='white' phrase='learn more' />
              }
            </div>
          </div>
        </div>
        <div style={{ padding: '60px 0px' }} className="container">
          <Grid container spacing={8}>

            <Grid item xs={12} sm={12} md={3} id="mobile_views">
              <Sidebar
                posts={this.state.sidebarPosts}
                archives={this.state.archives}
                pageName={this.state.pageOption}
                functionCheck={this.functionCheck}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={9} >
              <Masonry className={'my-gallery-class'}>
                {childElements}
              </Masonry>
            </Grid>
            <Grid item xs={12} sm={12} md={3} id="desktop_views">
              <Sidebar
                posts={this.state.sidebarPosts}
                archives={this.state.archives}
                pageName={this.state.pageOption}
                functionCheck={this.functionCheck}
              />
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}
