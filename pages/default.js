import React from 'react'
import Page from '../components/views/page'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import { mapGlobals } from '../utils/helper';
import Head from 'next/head';

export default class DefaultPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject(query.pagename, query.status, query.revision);
      page = pageResponse.object;
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }
    return { page }
  }

  constructor(props){
    super(props);

    this.state = {
      page: props.page
    }
  }

	render() {
    return (
      this.props.page.component && this.props.page.component==='404' ? (
        <PageNotFound />
      ) : (
        <section className={'container'} style={{paddingTop: 75}}>
          <Head>
            <title>{`TAC - ` + this.state.page.title}</title>
          </Head>
          <Page page={this.props.page} />
        </section>
      )
		);
	}
}