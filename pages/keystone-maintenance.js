import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import { mapPage } from "../utils/helper";
import Hero from '../components/views/hero';
import ContactCTA from '../components/views/contact-cta';
import ArrowButton from '../components/buttons/ArrowButton';
import Grid from '@material-ui/core/Grid';
import Head from 'next/head';
import RequestModal from '../components/views/keystone-maintenance-request';
import Post from '../utils/post';

export default class KeystoneMaintenancePage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject('maintenance', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }

    return { page }
  }

  constructor(props) {
    super(props);
    this.state = {
      page: props.page,
      hero: props.page.hero_banner,
      cta: props.page.cta,
      about: props.page.about,
      name: "",
      email: "",
      phone: "",
      message: "",
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalAircraftMake: "",
      modalAircraftModel: "",
      modalTailNumber: "",
      modalAddress: "",
      modalZipCode: "",
      modalUSState: "",
      modalCity: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    }
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/tac-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'Keystone Aviation Contact - Maintenance';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>`
          + `<b>Sent to: marketing@tacenergy.com </b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePhoneChange = (event) => {
    this.setState({ phone: event.target.value });
  };

  handleMessageChange = (event) => {
    this.setState({ message: event.target.value });
  };

  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalAircraftMake: "",
      modalAircraftModel: "",
      modalTailNumber: "",
      modalAddress: "",
      modalZipCode: "",
      modalUSState: "",
      modalCity: "",
    })
  };

  handleModalSubmit = (event) => {
    this.setState({ modalSubmitting: true });
    event.preventDefault();

    fetch('/api/keystone-maintenance-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.modalName,
        email: this.state.modalEmail,
        phone: this.state.modalPhone,
        message: this.state.modalComments,
        aircraftMake: this.state.modalAircraftMake,
        aircraftModel: this.state.modalAircraftModel,
        tailNumber: this.state.modalTailNumber,
        address: this.state.modalAddress,
        zipcode: this.state.modalZipCode,
        state: this.state.modalUSState,
        city: this.state.modalCity,
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'Keystone Request - Maintenance';
        let html =
          `<div>`
          + `<b>New Quote request from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Aircraft Make: ${this.state.modalAircraftMake}</b><br>`
          + `<b>Aircraft Model: ${this.state.modalAircraftModel}</b><br>`
          + `<b>Tail Number: ${this.state.modalTailNumber}</b><br>`
          + `<b>Address: ${this.state.modalAddress}</b><br>`
          + `<b>Zipcode: ${this.state.modalZipCode}</b><br>`
          + `<b>City: ${this.state.modalCity}</b><br>`
          + `<b>Sent to: marketing@tacenergy.com, btanis@keystoneaviation.com, cwalker@keystoneaviation.com, jray@keystoneaviation.com, bhoddenbach@keystoneaviation.com, crobertson@keystoneaviation.com, ssobczak@keystoneaviation.com, ARichins@tacenergy.com</b><br>` +
          `</div>`;
        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({ modalSubmitted: true });
      } else {
        this.setState({ modalSubmitted: false, modalSubmitting: false, modalFailedSumbit: true });
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({ modalName: event.target.value });
  };

  handleModalPhoneChange = (event) => {
    this.setState({ modalPhone: event.target.value });
  };

  handleModalEmailChange = (event) => {
    this.setState({ modalEmail: event.target.value });
  };

  handleModalCommentsChange = (event) => {
    this.setState({ modalComments: event.target.value });
  };

  handleModalAircraftMakeChange = (event) => {
    this.setState({ modalAircraftMake: event.target.value });
  };

  handleModalAircraftModelChange = (event) => {
    this.setState({ modalAircraftModel: event.target.value });
  };

  handleModalTailNumberChange = (event) => {
    this.setState({ modalTailNumber: event.target.value });
  };

  handleModalAddressChange = event => {
    this.setState({ modalAddress: event.target.value });
  };

  handleModalCityChange = event => {
    this.setState({ modalCity: event.target.value });
  };

  handleModalZipCodeChange = event => {
    this.setState({ modalZipCode: event.target.value });
  };

  handleModalUSStateChange = event => {
    this.setState({ modalUSState: event.target.value });
  };


  handleOnClickArrow = () => {
    this.setState({ modalOpen: true });
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false
    });
    this.setState({ modalOpen: !this.state.modalOpen });
  };

  render() {
    let sectionHead = this.state.about.children;
    let sectionHeadBackground = sectionHead[2].imgix_url + '?w=2000';
    let sectionHeadTitle = sectionHead[0].value;
    let sectionHeadExc = sectionHead[1].value;
    let float = 'right';
    let bgColor = 'white';

    let cardColor =
      bgColor === 'blue' ? 'rgba(0, 83, 160, 0.96)' :
        bgColor === 'red' ? 'rgba(226, 56, 63, 0.96)' : 'rgba(255, 255, 255, 0.96)';

    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'keystone'}>
            <Head>
              <title>{`Keystone Aviation - Maintenance`}</title>
            </Head>
            {
              this.state.hero.children[1].value !== '' || this.state.hero.children[2].value !== '' &&
              <Hero hero={this.state.hero} headingWidth={1350} />
            }
            <section style={{
              position: 'relative',
              width: '100%',
              display: 'flex',
              flexWrap: 'wrap',
              height: '100%',
              justifyContent: float === 'right' ? 'flex-start' : 'flex-end'
            }}>
              <div
                style={{ background: cardColor }}
                className={float === 'right' ? 'banner-card right' : 'banner-card'}
              >
                <div className="banner-card-inner">
                  <h1 style={{
                    color: bgColor === 'blue' ? 'white' : bgColor === 'red' ? 'white' : '#4c4b4b'
                  }}>{sectionHeadTitle}</h1>
                  <div style={{
                    color: bgColor === 'blue' ? '#f3f3f3' : bgColor === 'red' ? '#f3f3f3' : '#626262',
                  }}>
                    {sectionHeadExc}
                  </div>
                  <a onClick={this.handleOnClickArrow.bind(this)} style={{ width: '100%', padding: 0 }}>
                    <ArrowButton color='#e2383f' phrase='Request A Quote' />
                  </a>
                </div>
              </div>
              <div className="sectionHeadImg" style={{
                background: `url(${sectionHeadBackground})`,
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                height: '70vh'
              }} />
            </section>
            <section style={{ margin: '100px 0' }} className="selling_processes">
              <div className="container">
                <h1 style={{ color: 'white' }}>Piston and Turbine Aircraft maintenance specialization in:</h1>
                <div className={'inner'} dangerouslySetInnerHTML={{ __html: this.state.page.specialization.value }} />
              </div>
            </section>
            <section>
              <div className="container">
                <h1>Authorized Service Center For:</h1>
                <Grid style={{ marginTop: 50, marginBottom: 50 }} container spacing={40}>
                  {
                    !!this.state.page.authorized_service && this.state.page.authorized_service.children.map((logo, index) => {
                      return (
                        <Grid item sm={3} key={index}>
                          <img src={logo.url} />
                        </Grid>
                      )
                    })
                  }
                </Grid>
              </div>
            </section>
            <ContactCTA
              cta={this.state.cta}
              emailValue={this.state.email}
              handleSubmit={this.handleSubmit.bind(this)}
              onEmailChange={this.handleEmailChange.bind(this)}
              nameValue={this.state.name}
              onNameChange={this.handleNameChange.bind(this)}
              phoneValue={this.state.phone}
              onPhoneChange={this.handlePhoneChange.bind(this)}
              messageValue={this.state.message}
              onMessageChange={this.handleMessageChange.bind(this)}
              bgColor='#8b8b8b'
              btnColor='#e2383f'
              submitted={this.state.submitted}
              submitting={this.state.submitting}
              failedToSumbit={this.state.failedSubmit}
            />
            <RequestModal
              modalOpen={this.state.modalOpen}
              onModalClose={this.handleModalClose.bind(this)}
              emailValue={this.state.modalEmail}
              handleSubmit={this.handleModalSubmit.bind(this)}
              onEmailChange={this.handleModalEmailChange.bind(this)}
              NameValue={this.state.modalName}
              onNameChange={this.handleModalNameChange.bind(this)}
              phoneValue={this.state.modalPhone}
              onPhoneChange={this.handleModalPhoneChange.bind(this)}
              commentsValue={this.state.modalComments}
              onCommentsChange={this.handleModalCommentsChange.bind(this)}
              aircraftMakeValue={this.state.modalAircraftMake}
              onAircraftMakeChange={this.handleModalAircraftMakeChange.bind(this)}
              aircraftModelValue={this.state.modalAircraftModel}
              onAircraftModelChange={this.handleModalAircraftModelChange.bind(this)}
              tailNumberValue={this.state.modalTailNumber}
              onTailNumberChange={this.handleModalTailNumberChange.bind(this)}
              cityValue={this.state.modalCity}
              onCityChange={this.handleModalCityChange.bind(this)}
              addressValue={this.state.modalAddress}
              onAddressChange={this.handleModalAddressChange.bind(this)}
              zipCodeValue={this.state.modalZipCode}
              onZipCodeChange={this.handleModalZipCodeChange.bind(this)}
              USStateValue={this.state.modalUSState}
              onUSStateChange={this.handleModalUSStateChange.bind(this)}
              submitted={this.state.modalSubmitted}
              submitting={this.state.modalSubmitting}
              failedToSumbit={this.state.modalFailedSubmit}
            />
          </article>
        )
    );
  }
}