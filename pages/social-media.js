import React from 'react'
import Request from '../utils/request';
import { withRouter } from 'next/router';
import Masonry from 'react-masonry-component';
import Grid from '@material-ui/core/Grid';
import Head from 'next/head';

class Feeds extends React.Component {
	static async getInitialProps({ req, query }) {
		let newsroomMeta;

		try {
			const ResponseNewsroomMeta = await Request.getObject('news-and-views', query.status, query.revision);
			newsroomMeta = ResponseNewsroomMeta.object;
		} catch (e) {
			console.log(e);
		}

		return { newsroomMeta }
	}

	constructor(props) {
		super(props);

		this.state = {
			newsroom: props.newsroomMeta,
			base: 'https://api.instagram.com/v1',
			tacairToken: '2150885558.1677ed0.cb050b9615db426fa256f57b59b2d8e2',
			tacToken: '3193443774.1677ed0.a4b98b8dede84a23973fe049f5432b01',
			tac2Token: '471379659.1677ed0.42a25f34d7c0413b8842eb612d8c5eef',
			feeds: [],
			unsorted_feeds: [],
			tweets: [],
			posts: [],
			viewState: 'all',
			extraName: '',
			//loading: true
		}

		this.fetchURLS = this.fetchURLS.bind(this);
		this.addFilter = this.addFilter.bind(this);
	}

	async fetchURLS() {
		const { base, tacairToken, tacToken, tac2Token } = this.state;
		try {
			return await Promise.all([
				fetch(`${base}/users/self/media/recent/?access_token=${tacairToken}`).then(response => response.json()),
				fetch(`${base}/users/self/media/recent/?access_token=${tacToken}`).then(response => response.json()),
				fetch(`${base}/users/self/media/recent/?access_token=${tac2Token}`).then(response => response.json()),
			]);
		} catch (error) {
			console.log(error);
		}
	}
	async fetchTLUrls() {
		try {
			return await Promise.all([
				fetch('/api/twitter').then(response => response.json()),
				//fetch('/api/fetch_linked_in_data_tac_energy').then(response => response.json()),
				// fetch('/api/fetch_linked_in_data_keystone').then(response => response.json()),
				// fetch('/api/fetch_linked_in_data_tac_air').then(response => response.json()),
				// fetch('/api/fetch_linked_in_data_thearnoldcos').then(response => response.json()),
			]);
		}
		catch (error) {
			console.log(error);
		}
	}

	componentDidMount() {
		this.fetchURLS().then(results => {
			//const mergeResults = [...results[0].data, ...results[1].data, ...results[2].data];
			const mergeResults = [...results[0].data, ...results[1].data];
			this.setState(state => {
				return {
					unsorted_feeds: [...state.unsorted_feeds, ...mergeResults]
				};

			});
		});

		this.fetchTLUrls().then(results => {
			//console.log(results);
			// this.setState({
			// 	loading: false
			// })
			const { unsorted_feeds } = this.state;

			var i = 0;
			results[0].tweets.forEach(function (val) {
				var tdate = new Date(val.created_at).getTime();
				tdate = tdate / 1000;
				results[0].tweets[i].created_time = tdate;
				results[0].tweets[i].content_type = "TWEET";
				if (results[0].tweets[i].user.name == "TAC Air") {
					results[0].tweets[i].myurl = "tacair";
				}
				else if (results[0].tweets[i].user.name == "The Arnold Companies") {
					results[0].tweets[i].myurl = "thearnoldcos";
				}
				else {
					results[0].tweets[i].myurl = results[0].tweets[i].user.name;
				}
				i++;
			});
			i = 0;
			// results[1].values.forEach(function (val) {
			// 	results[1].values[i].created_time = (val.timestamp / 1000);
			// 	results[1].values[i].content_type = "LINK_FEED";
			// 	results[1].values[i].myurl = "tac-energy";
			// 	i++;
			// });
			// i = 0;
			// results[2].values.forEach(function(val)
			// {
			// 	results[2].values[i].created_time =  (val.timestamp / 1000);
			// 	results[2].values[i].content_type = "LINK_FEED";
			// 	results[2].values[i].myurl = "keystone-aviation";
			// 	i++;
			// });
			// i = 0;
			// results[3].values.forEach(function(val)
			// {
			// 	results[3].values[i].created_time =  (val.timestamp / 1000);
			// 	results[3].values[i].content_type = "LINK_FEED";
			// 	results[3].values[i].myurl = "tac-air";
			// 	i++;
			// });
			// i = 0;
			// results[4].values.forEach(function(val)
			// {
			// 	results[4].values[i].created_time =  (val.timestamp / 1000);
			// 	results[4].values[i].content_type = "LINK_FEED";
			// 	results[4].values[i].myurl = "thearnoldcompanies";
			// 	i++;
			// });
			// const mergeResults = [...unsorted_feeds, ...results[0].tweets, ...results[1].values, ...results[2].values, ...results[3].values, ...results[4].values];
			const mergeResults = [...unsorted_feeds, ...results[0].tweets];
			//const mergeResults = [...unsorted_feeds];
			const sortedResults = mergeResults.sort((a, b) => {
				return b.created_time - a.created_time;
			});
			this.setState(state => {
				return {
					feeds: [...state.feeds, ...sortedResults]
				};
			});


		});

	}
	addFilter(str) {
		if (str == "all")
			this.setState({ viewState: "all", loading: false });
		else if (str == "thearnoldcos")
			this.setState({ viewState: "thearnoldcos", extraName: "The Arnold Companies", loading: false });
		else if (str == "tac_air")
			this.setState({ viewState: "tac_air", extraName: "TAC Air" });
		else if (str == "keystoneaviation")
			this.setState({ viewState: "keystoneaviation", extraName: "Keystone Aviation" });
		else if (str == "tacenergy")
			this.setState({ viewState: "tacenergy", extraName: "TACenergy" });
	}
	onCardClick(value) {
		const { router } = this.props;
		if (typeof router === 'undefined') return;
		router.push(value);
	}
	render() {
		let heroImage = this.state.newsroom.metadata.hero_image.imgix_url + '?w=2000';
		const { feeds, tweets } = this.state;

		const urlify = (text) => {
			let urlRegex = /(https?:\/\/[^\s]+)/g;
			return text.replace(urlRegex, (url) => {
				return `<a href=${url} target="_blank">${url}</a>`;
			});
		}

		const childElements = feeds.map((element, index) => {


			const date = new Date(parseInt(element.created_time) * 1000);
			const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
			var checked = false;
			if (element.content_type == "TWEET") {
				if (this.state.viewState == "all" || this.state.viewState == element.user.name || this.state.extraName == element.user.name) {

					return (
						<article key={index} style={{ padding: 10 }} className="post-element-class" onClick={() => onCardClick(`https://twitter.com/${element.myurl}`)}>
							<a href={`https://twitter.com/${element.myurl}`} target="_blank">
								<div className="card" style={{ width: '100%' }}>
									<div style={{ paddingLeft: '10%', paddingRight: '10%', paddingTop: 32, marginTop: '-5px' }}>
										<a href={`https://twitter.com/${element.myurl}`} target="_blank">
											<div className="post-date" style={{ marginBottom: '15px' }}>{new Date(element.created_at).toDateString()}</div>
										</a>
										<a href={`https://twitter.com/${element.myurl}`} target="_blank">
											<div className="post-teaser">
												<div dangerouslySetInnerHTML={{ __html: urlify(element.text) }} />
											</div>
										</a>
										<div className="post-categories">
											<a href={`https://twitter.com/${element.myurl}`} target="_blank" style={{ fontSize: 13, textTransform: 'capitalize', color: '#8d8d8d' }}>{element.user.name}</a>
										</div>
									</div>
								</div>
							</a>
						</article>
					);
				}
			}
			else if (element.content_type == "LINK_FEED") {
				if (this.state.viewState == "all" || this.state.viewState == element.updateContent.company.name || this.state.extraName == element.updateContent.company.name) {
					return (
						<article key={index} style={{ padding: 10 }} className="post-element-class" onClick={() => onCardClick(`https://in.linkedin.com/company/${element.myurl}`)} >
							<a href={`https://in.linkedin.com/company/${element.myurl}`} target="_blank">
								<div className="card" style={{ width: '100%' }}>
									<a href={`https://in.linkedin.com/company/${element.myurl}`} target="_blank">
										<div className="post-date linkedin-date" style={{ marginBottom: '5px' }}>
											{`${months[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}`}
										</div>
									</a>
									<a href={`https://in.linkedin.com/company/${element.myurl}`} target="_blank">
										<img width="100%" src={element.updateContent.companyStatusUpdate.share.content.submittedImageUrl} />
									</a>
									<div style={{ paddingLeft: '10%', paddingRight: '10%', paddingTop: 32, marginTop: '-5px' }}>
										<a href={`https://in.linkedin.com/company/${element.myurl}`} target="_blank">
											<div className="post-teaser">{element.updateContent.companyStatusUpdate.share.comment}</div>
										</a>
										<div className="post-categories">
											<a href={`https://in.linkedin.com/company/${element.myurl}`} target="_blank" style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{element.updateContent.company.name == 'The Arnold Companies' ? 'TAC - The Arnold Companies' : element.updateContent.company.name}</a>
										</div>
									</div>
								</div>
							</a>
						</article>
					);
				}
			}
			else {
				if (this.state.viewState == "all" || this.state.viewState == element.user.username || this.state.extraName == element.user.username) {
					return (
						<article key={index} style={{ padding: 10 }} className={`post-element-class ${element.user.username}`} onClick={() => onCardClick(`https://www.instagram.com/${element.user.username}`)} >
							<a href={`https://www.instagram.com/${element.user.username}`} target="_blank" >
								<div className="card" style={{ width: '100%' }}>
									<a href={`https://www.instagram.com/${element.user.username}`} target="_blank">
										<img width="100%" src={element.images.standard_resolution.url} />
									</a>
									<div style={{ paddingLeft: '10%', paddingRight: '10%', paddingTop: 32, marginTop: '-5px' }}>
										<a href={`https://www.instagram.com/${element.user.username}`} target="_blank">
											<div className="post-date" style={{ marginBottom: '15px' }}>
												{`${months[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}`}
											</div></a>
										<a href={`https://www.instagram.com/${element.user.username}`} target="_blank">
											<div className="post-teaser">{element.caption.text}</div>
										</a>
										<div className="post-categories">
											<a href={`https://www.instagram.com/${element.user.username}`} target="_blank" style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{element.user.full_name}</a>
										</div>
									</div>
								</div>
							</a>
						</article>
					);
				}
			}
		});

		return (
			<div>
				<div className="loading_page" style={this.state.loading ? { display: "block" } : { display: "none" }}>
					<div className="loading-img"><img src="../static/images/loading.gif" alt="" /></div>
				</div>
				<Head>
					<title>TAC - Social Media</title>
				</Head>
				<div style={{ height: '60vh', width: '100%', position: 'relative' }}>
					<div className="hero-img" style={{ background: `url(${heroImage})`, backgroundSize: 'cover', backgroundPosition: '50% 35%' }} />
					<div className="header-container hero-wrapper">
						<h1 className="hero-title" style={{ color: 'white', maxWidth: 660 }}>Social Media</h1>
					</div>
				</div>
				<div style={{ padding: '60px 0px' }} className="container">
					<Grid container spacing={8}>
						<Grid item xs={12} sm={12} md={3} id="mobile_views">
							<section style={{ padding: 20 }}>
								<div style={{ marginBottom: 50 }}>
									<div>
										<a className="button-cat" style={this.state.viewState == "all" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('all')} href="#all">All Brands</a>
										<a className="button-cat" style={this.state.viewState == "thearnoldcos" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('thearnoldcos')} href="#thearnoldcos">TAC - The Arnold Companies</a>
										<a className="button-cat" style={this.state.viewState == "tac_air" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('tac_air')} href="#tac_air">TAC Air</a>
										<a className="button-cat" style={this.state.viewState == "keystoneaviation" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('keystoneaviation')} href="#keystoneaviation">Keystone Aviation</a>
										<a className="button-cat" style={this.state.viewState == "tacenergy" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('tacenergy')} href="#tacenergy">TACenergy</a>
									</div>
								</div>
							</section>
						</Grid>

						<Grid item xs={12} sm={12} md={9} >
							<Masonry className={'my-gallery-class'}>
								{childElements}
							</Masonry>
						</Grid>
						<Grid item xs={12} sm={12} md={3} id="desktop_views">
							<section style={{ padding: 20 }}>
								<div style={{ marginBottom: 50 }}>
									<div>
										<a className="button-cat" style={this.state.viewState == "all" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('all')} href="#all">View All</a>
										<a className="button-cat" style={this.state.viewState == "thearnoldcos" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('thearnoldcos')} href="#thearnoldcos">TAC - The Arnold Companies</a>
										<a className="button-cat" style={this.state.viewState == "tac_air" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('tac_air')} href="#tac_air">TAC Air</a>
										<a className="button-cat" style={this.state.viewState == "keystoneaviation" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('keystoneaviation')} href="#keystoneaviation">Keystone Aviation</a>
										<a className="button-cat" style={this.state.viewState == "tacenergy" ? { background: "#0b559c" } : {}} onClick={() => this.addFilter('tacenergy')} href="#tacenergy">TACenergy</a>
									</div>
								</div>
							</section>
						</Grid>

					</Grid>
				</div>
			</div>
		);
	}
}

export default withRouter(Feeds);
