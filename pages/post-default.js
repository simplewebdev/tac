import React from 'react';
import Page from '../components/views/page';
import PageNotFound from '../components/views/404';
import Masonry from 'react-masonry-component';
import Request from '../utils/request';
import ImageGallery from 'react-image-gallery';
import Grid from '@material-ui/core/Grid';
import Sidebar from '../components/shared/sidebar';
import Head from 'next/head';
import CTA from '../components/views/cta';
import Archives from '../utils/archives';
import { withRouter } from 'next/router';
import moment from 'moment';
import timezone from 'moment-timezone';

export default class PostDefaultPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;
    let sidebarPosts = [];
    let posts;

    try {
      const ResponseAllPosts = await Request.getPosts(null);
      const pageResponse = await Request.getObject(query.pagename, query.status, query.revision);
      page = pageResponse.object;
      const ResponseSidebarPosts = await Request.getPosts(4);
      posts = ResponseAllPosts.objects;
      sidebarPosts = ResponseSidebarPosts.objects;
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { page, sidebarPosts, posts }
  }

  constructor(props) {
    super(props);

    this.state = {
      page: props.page,
      sidebarPosts: props.sidebarPosts,
      posts: props.posts,
      archives: [],
      cta: {
        value: 'Market Talk Updates',
        key: 'cta',
        title: 'CTA',
        type: 'parent',
        children:
          [{
            value: 'Market Talk Updates',
            key: 'title',
            title: 'Title',
            type: 'text',
            children: null
          },
          {
            value: 'a45afc40-86bc-11e8-b2a5-bf309249a48d-tac-energy-logo-wht.svg',
            key: 'image',
            title: 'Image',
            type: 'file',
            children: null,
            url: 'https://s3-us-west-2.amazonaws.com/cosmicjs/a45afc40-86bc-11e8-b2a5-bf309249a48d-tac-energy-logo-wht.svg',
            imgix_url: 'https://cosmic-s3.imgix.net/a45afc40-86bc-11e8-b2a5-bf309249a48d-tac-energy-logo-wht.svg'
          },
          {
            value: 'Sign up to receive market updates in your inbox each day.',
            key: 'excerpt',
            title: 'Excerpt',
            type: 'textarea',
            children: null
          }]
      },
      firstName: "",
      lastName: "",
      company: "",
      email: "",
      loading: false,
      submitting: false,
      submitted: false,
      pageOption: "detail",
      failedSubmit: false,
    }
    this.functionCheck = this.functionCheck.bind(this);
  }

  componentDidMount() {
    const { posts } = this.state;
    this.setState(state => {
      return {
        archives: Archives.filterPostsDateYear(posts)
      }
    });
  }

  async functionCheck(pagename) {
    this.setState(state => {
      return { loading: true }
    });
    if (pagename == "media") {
      this.setState(state => {
        return { pageOption: pagename, loading: false }
      });
    }
    else if (pagename != "newsroom") {
      let AllPostData;
      try {
        AllPostData = await Request.filterPostByCat(pagename);
      }
      catch (e) {
        page =
          {
            title: 'Page not found',
            component: '404',
          }
      }
      this.setState(state => {
        return { posts: AllPostData.objects, pageOption: pagename, loading: false }
      });
    }
    else {
      let ResponseAllPosts = await Request.getPosts(null);
      let posts = ResponseAllPosts.objects;
      this.setState(state => {
        return { posts: posts, pageOption: pagename, loading: false }
      });
    }
  }

  resetForm = () => {
    this.setState({
      firstName: "",
      lastName: "",
      company: "",
      email: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/cc-market-talk-subscribe', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "lists": [
          {
            "id": "881"
          }
        ],
        "company_name": this.state.company,
        "confirmed": false,
        "email_addresses": [
          {
            "email_address": this.state.email
          }
        ],
        "first_name": this.state.firstName,
        "last_name": this.state.lastName
      })
    }).then((res) => {
      if (res.status === 200) {
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  handleFirstNameChange = (event) => {
    this.setState({ firstName: event.target.value });
  };

  handleLastNameChange = (event) => {
    this.setState({ lastName: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handleCompanyChange = (event) => {
    this.setState({ company: event.target.value });
  };

  render() {
    const {
      metadata: {
        photo_gallery,
        category,
        featured_image,
        photo_gallery_options
      }
    } = this.state.page;

    const getDate = () => {
      if (this.state.page.metadata.date_posted) {
      return  moment(this.state.page.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
      }
      return moment(this.state.page.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
    }

    let catLength = category.length;
    const media = [];

    if (typeof photo_gallery !== 'undefined') {
      photo_gallery.photos.map((photo) => {
        var isImage = /([/|.|\w|\s|-])*\.(?:jpg|jpeg|JPG|JPEG|gif|GIF|png|PNG)/.test(photo.image);
        if (!isImage && photo.image.url != null) {
          media.push({
            original: `${photo.image.url}?w=1600`,
            thumbnail: `${photo.image.url}?w=92&h=92`,
          })
        }
      })
    }

    const Featured = () => {
      if (typeof featured_image !== 'undefined') {
        return <img width="100%" src={featured_image.imgix_url + '?w=2000'} />
      } else {
        return null;
      }
    }
    const PostImageGallery = () => {
      if (typeof photo_gallery === 'undefined' && media.length <= 0) return null;
      return (
        <ImageGallery
          items={media}
          showBullets={true}
          showPlayButton={false}
          showNav={true}
          showThumbnails={false}
          lazyLoad={true} />
      );
    };
    let childElements;
    if (this.state.pageOption == "media") {
      childElements = (
        <div>
          <Head>
            <title>{`TAC - Media Graphics`}</title>
          </Head>
          <h2 style={{ fontWeight: 600, marginBottom: 25 }}>Click on a graphic below to download in PDF format </h2>
          <a target={"_blank"} href={'https://cosmic-s3.imgix.net/30210740-966e-11e8-ab08-31f1ed4cd97d-tac-full-logo.svg'}>
            <img style={{ margin: 10, marginBottom: 50 }} width={'45%'} src={'https://cosmic-s3.imgix.net/30210740-966e-11e8-ab08-31f1ed4cd97d-tac-full-logo.svg'} />
          </a>
          <a target={"_blank"} href={'https://s3-us-west-2.amazonaws.com/cosmicjs/04a36f60-9fef-11e8-a649-bbc58f269540-Tac_Energy_Logo.pdf'}>
            <img style={{ margin: 10, marginBottom: 50 }} width={'45%'} src={'https://cosmic-s3.imgix.net/9376e000-758d-11e8-9c42-75c5f9571e6d-TACenergy.png'} />
          </a>
          <a target={"_blank"} href={'https://s3-us-west-2.amazonaws.com/cosmicjs/079cd940-9fef-11e8-baa7-297ee0900870-TAC_Air_Logo.pdf'}>
            <img style={{ margin: 10, marginBottom: 50 }} width={'45%'} src={'https://cosmic-s3.imgix.net/937384a0-758d-11e8-ac9f-85d733f58489-TAC%20Air.png'} />
          </a>
          <a target={"_blank"} href={'https://cosmic-s3.imgix.net/93a67b80-758d-11e8-9c42-75c5f9571e6d-Keystone%20Aviation.png'}>
            <img style={{ margin: 10, marginBottom: 50 }} width={'45%'} src={'https://cosmic-s3.imgix.net/93a67b80-758d-11e8-9c42-75c5f9571e6d-Keystone%20Aviation.png'} />
          </a>
        </div>
      );
    }
    else if (this.state.pageOption != "detail") {
      childElements = this.state.posts.map((element, index) => {
        let postDate;
        if (element.metadata.date_posted) {
            postDate = moment(element.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
        } else {
            postDate = moment(element.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
        }
        let catLength = element.metadata.category.length;
        return (
          <article key={index} style={{ padding: 10, cursor: 'pointer' }} className="post-element-class">
            <Head>
              <title>{`TAC - ${this.state.pageOption}`}</title>
            </Head>
            <a href={`/news-and-views/${element.slug}`} style={{ display: 'block' }}>
              <img width="100%" style={{ verticalAlign: 'middle' }} src={`${element.metadata.featured_image.imgix_url}?w=500`} />
            </a>
            <div style={{ paddingLeft: '10%', paddingRight: '10%', paddingTop: 32, marginTop: '-5px' }} className="card">
              <a href={`/news-and-views/${element.slug}`} >
                <div className="post-date">{postDate}</div>
              </a>
              <h2 className="post-title">
                {element.metadata.category.map((cat, index) => {
                  if (catLength === index + 1) {
                    if (cat.title == 'TACenergy') {
                      let str = cat.title.replace(/C/g, 'c-');
                      let TacEnergy = str.toLowerCase();
                      return (<a href={`/news-and-views/${element.slug}#${TacEnergy}`} >{element.title}</a>)
                    } else {
                      return (<a href={`/news-and-views/${element.slug}`} >{element.title}</a>)
                    }
                  }

                })}
              </h2>
              <div className="post-teaser">
                {element.metadata.category.map((cat, index) => {
                  if (catLength === index + 1) {
                    if (cat.title == 'TACenergy') {
                      let str = cat.title.replace(/C/g, 'c-');
                      let TacEnergy = str.toLowerCase();
                      return (<a href={`/news-and-views/${element.slug}#${TacEnergy}`} >{element.metadata.teaser}</a>)
                    } else {
                      return (<a href={`/news-and-views/${element.slug}`} >{element.metadata.teaser}</a>)
                    }
                  }
                })}
              </div>
              <div className="post-categories">
                {element.metadata.category.map((cat, index) => {
                  if (catLength === index + 1) {
                    return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}</a>)
                  } else {
                    return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}, </a>)
                  }
                })}
              </div>
            </div>
          </article>
        );
      });
    }
    else {
      childElements = (
        <div className={'post-content'}>
          {media.length <= 0 ? <Featured /> : null}
          {(photo_gallery_options === 'Top' && media.length !== 0) ? <ImageGallery items={media} showBullets={true} showPlayButton={false} showNav={true} showThumbnails={false} lazyLoad={true} /> : null}
          <h1>{this.state.page.title}</h1>
          <div className={'post-meta'}>
            <div className="post-date">{getDate()}</div>
            <div className={'post-meta-cats'}>
              {this.state.page.metadata.category.map((cat, index) => {
                if (catLength === index + 1) {
                  return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 12, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}</a>)
                } else {
                  return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 12, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}, </a>)
                }
              })}
            </div>
          </div>
          <div dangerouslySetInnerHTML={{ __html: this.state.page.content }} />
          {photo_gallery_options === 'Bottom' && media.length !== 0 ? <div style={{ paddingTop: '60px' }}><PostImageGallery /></div> : null}
        </div>
      );
    }

    return (
      this.props.page.component && this.props.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <section>
            <div className="container">
              <Head>
                <title>{`${this.state.page.title}`}</title>
              </Head>
              <div className="loading_page" style={this.state.loading ? { display: "block" } : { display: "none" }}>
                <div className="loading-img"><img src="../static/images/loading.gif" alt="" /></div>
              </div>
              <Grid container spacing={40}>
                <Grid item xs={12} sm={12} md={3} id="mobile_views">
                  <Sidebar
                    posts={this.state.sidebarPosts}
                    archives={this.state.archives}
                    pageName={this.state.pageOption}
                    functionCheck={this.functionCheck}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={9} >
                  <Masonry className={'my-gallery-class'}>
                    {childElements}
                  </Masonry>
                </Grid>
                <Grid item xs={12} sm={12} md={3} id="desktop_views">
                  <Sidebar
                    posts={this.state.sidebarPosts}
                    archives={this.state.archives}
                    pageName={this.state.pageOption}
                    functionCheck={this.functionCheck}
                  />
                </Grid>
              </Grid>
            </div>
            {this.state.page.metadata.category.map((cat, index) => {
              if (cat.slug === 'tac-energy-news') {
                return (
                  <section className={'energy'} key={index}>
                    <CTA
                      cta={this.state.cta}
                      emailValue={this.state.email}
                      handleSubscribe={this.handleSubmit.bind(this)}
                      onEmailChange={this.handleEmailChange.bind(this)}
                      firstNameValue={this.state.firstName}
                      onFirstNameChange={this.handleFirstNameChange.bind(this)}
                      lastNameValue={this.state.lastName}
                      onLastNameChange={this.handleLastNameChange.bind(this)}
                      companyValue={this.state.company}
                      onCompanyChange={this.handleCompanyChange.bind(this)}
                      logoHeight='50px'
                      bgColor='#8b8b8b'
                      btnColor='#0b559c'
                      submitted={this.state.submitted}
                      submitting={this.state.submitting}
                      failedToSumbit={this.state.failedSubmit}
                    />
                  </section>
                )
              }
            })}
          </section>
        )
    );
  }
}