import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import Grid from '@material-ui/core/Grid';
import ImageGallery from 'react-image-gallery';
import { Element } from 'react-scroll';
import Head from 'next/head';
import ArrowButton from '../components/buttons/ArrowButton';
import RequestQuote from '../components/views/keystone-sales-request';
import Post from '../utils/post';

export default class AircraftDefault extends React.Component {

  static async getInitialProps({ req, query }) {
    let charter;
    let page;
    let salesRep;
    let isSales;

    try {
      const pageResponse = await Request.getObject(query.pagename, query.status, query.revision);
      page = pageResponse.object;

      if (page.type_slug !== 'for-sale-aircrafts') {
        const fleet = await Request.getCharterFleet(4, 'random');
        charter = fleet.objects;
        salesRep = null;
        isSales = false;
      } else {
        const fleet = await Request.getAircraftsForSale(4, 'random');
        charter = fleet.objects;
        isSales = true;
        salesRep = page.metadata.salesman[0].metadata.email;
      }

    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { page, charter, salesRep, isSales }
  }

  constructor(props) {
    super(props);

    this.state = {
      page: props.page,
      charter: props.charter,
      showFullscreenButton: true,
      showGalleryFullscreenButton: true,
      showPlayButton: true,
      showGalleryPlayButton: true,
      showVideo: {},
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalCaptcha: "",
      isRep: props.isSales,
      salesRep: props.salesRep
    }
  }

  _resetVideo() {
    this.setState({ showVideo: {} });

    if (this.state.showPlayButton) {
      this.setState({ showGalleryPlayButton: true });
    }

    if (this.state.showFullscreenButton) {
      this.setState({ showGalleryFullscreenButton: true });
    }
  }

  _toggleShowVideo(url) {
    this.state.showVideo[url] = !Boolean(this.state.showVideo[url]);
    this.setState({
      showVideo: this.state.showVideo
    });

    if (this.state.showVideo[url]) {
      if (this.state.showPlayButton) {
        this.setState({ showGalleryPlayButton: false });
      }

      if (this.state.showFullscreenButton) {
        this.setState({ showGalleryFullscreenButton: false });
      }
    }
  }

  _renderVideo(item) {
    return (
      <div className='image-gallery-image'>
        {
          this.state.showVideo[item.embedUrl] ?
            <div className='video-wrapper'>
              <a
                className='close-video'
                onClick={this._toggleShowVideo.bind(this, item.embedUrl)}
              >
              </a>
              <iframe
                width='560'
                height='315'
                src={item.embedUrl}
                frameBorder='0'
                allowFullScreen
              >
              </iframe>
            </div>
            :
            <a onClick={this._toggleShowVideo.bind(this, item.embedUrl)}>
              <div className='play-button' />
              <img src={item.original} />
              {
                item.description &&
                <span
                  className='image-gallery-description'
                  style={{ right: '0', left: 'initial' }}
                >
                  {item.description}
                </span>
              }
            </a>
        }
      </div>
    );
  }

  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalPhone: "",
      modalComments: "",
    })
  };

  handleModalSubmit = () => {
    this.setState({ modalSubmitting: true });

    fetch('/api/sales-rep-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.modalEmail,
        name: this.state.modalName,
        phone: this.state.modalPhone,
        company: this.state.modalCompany,
        message: this.state.modalComments,
        rep: this.state.salesRep
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'Keystone Sales Rep Form - Aircraft';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Company: ${this.state.modalCompany}</b><br>`
          + `<b>Rep: ${this.state.salesRep}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({ modalSubmitted: true });
      } else {
        this.setState({ modalSubmitted: false, modalSubmitting: false, modalFailedSumbit: true });
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({ modalName: event.target.value });
  };

  handleModalPhoneChange = (event) => {
    this.setState({ modalPhone: event.target.value });
  };

  handleModalEmailChange = (event) => {
    this.setState({ modalEmail: event.target.value });
  };

  handleModalCompanyChange = (event) => {
    this.setState({ modalCompany: event.target.value });
  };

  handleModalCommentsChange = (event) => {
    this.setState({ modalComments: event.target.value });
  };

  handleModalCaptchaChange = (value) => {
    this.setState({ modalCaptcha: value });
  };

  handleOnClickArrow = () => {
    this.setState({ modalOpen: true });
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false,
      modalOpen: !this.state.modalOpen
    });
  };


  render() {
    const media = [];

    this.state.page.metadata.photo_gallery.featured_video ? media.push({
      original: this.state.page.metadata.photo_gallery.featured_image.url,
      thumbnail: this.state.page.metadata.photo_gallery.featured_image.url,
      embedUrl: this.state.page.metadata.photo_gallery.featured_video,
      renderItem: this._renderVideo.bind(this)
    }) :
      this.state.page.metadata.photo_gallery.featured_image && media.push({
        original: this.state.page.metadata.photo_gallery.featured_image.url,
        thumbnail: this.state.page.metadata.photo_gallery.featured_image.url,
      });

    if (this.state.page.metadata.photo_gallery.photos && Array.isArray(this.state.page.metadata.photo_gallery.photos) || this.state.page.metadata.photo_gallery.photos.length) {
      this.state.page.metadata.photo_gallery.photos.map((photos) => {
        var isImage = /([/|.|\w|\s|-])*\.(?:jpg|jpeg|JPG|JPEG|gif|GIF|png|PNG)/.test(photos.photo.imgix_url);

        if (isImage) {
          media.push({
            original: `${photos.photo.imgix_url}`,
            thumbnail: `${photos.photo.imgix_url}`,
          })
        }
      })
    }
    return (
      this.props.page.component && this.props.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className="container fleet">
            <Head>
              <title>{`Keystone Aviation - ${this.state.page.title}`}</title>
            </Head>
            <Grid container spacing={40}>
              <Grid item sm={12} md={8}>
                <ImageGallery
                  items={media}
                  showThumbnails={false}
                  showBullets={true}
                  showPlayButton={false}
                  showNav={true}
                  lazyLoad={true} />
              </Grid>
              <Grid item sm={12} md={4}>
                <section>
                  <div className={'fleet-top'}>
                    <h1>{this.state.page.title}</h1>
                    {this.state.page.metadata.price && <p>{this.state.page.metadata.price}</p>}
                  </div>
                  {this.state.page.metadata.specs &&
                    <div>
                      <img src={this.state.page.metadata.specs.flight_deck_image.url} width="100%" />
                      <div style={{ display: 'flex', justifyContent: 'space-evenly', textAlign: 'center' }}>
                        <div>
                          <h6>Passengers</h6>
                          <span>{this.state.page.metadata.specs.passengers}</span>
                        </div>
                        <div>
                          <h6>Range (miles)</h6>
                          <span>{this.state.page.metadata.specs.range_miles}</span>
                        </div>
                        <div>
                          <h6>Cruising Speed (mph)</h6>
                          <span>{this.state.page.metadata.specs.cruising_speed_mph}</span>
                        </div>
                      </div>
                    </div>
                  }
                  {this.state.page.metadata.description &&
                    <div>
                      <h5>Description</h5>
                      <Element name="description" className="element" id="containerElement" style={{
                        position: 'relative',
                        height: '30vh',
                        overflow: 'scroll',
                      }}>
                        <div dangerouslySetInnerHTML={{ __html: this.state.page.metadata.description }} />
                      </Element>
                    </div>
                  }
                  {this.state.page.type_slug === 'for-sale-aircrafts' &&
                    <a onClick={this.handleOnClickArrow.bind(this)}>
                      <ArrowButton color='#e2383f' phrase='Contact Sales' />
                    </a>
                  }
                </section>
              </Grid>
            </Grid>
            {this.state.page.type_slug !== 'for-sale-aircrafts' ?
              <div style={{ padding: '75px 0px' }}>
                <h1>Other Charter Aircraft</h1>
                <Grid container spacing={8}>
                  {!!this.state.charter && this.state.charter.map((aircraft, index) =>
                    <Grid key={index} item xs={12} sm={6} md={3}>
                      <article style={{ maxWidth: 400, padding: 10 }}>
                        <a href={`/keystone-aviation/${aircraft.slug}`}>
                          <div className="card">
                            {aircraft.metadata.photo_gallery.featured_image.url !== 'https://s3-us-west-2.amazonaws.com/cosmicjs/' &&
                              <img width="100%" src={aircraft.metadata.photo_gallery.featured_image.url}
                                sizes="(max-width: 1024px) 100vw, 1024px" />}
                            <h2 className="aircraft-title">{aircraft.title}</h2>
                          </div>
                        </a>
                      </article>
                    </Grid>
                  )}
                </Grid>
              </div> :
              <div style={{ padding: '75px 0px' }}>
                <h1>Other Aircraft For Sale</h1>
                <Grid container spacing={8}>
                  {!!this.state.charter && this.state.charter.map((aircraft, index) =>
                    <Grid key={index} item xs={12} sm={6} md={3}>
                      <article style={{ maxWidth: 400, padding: 10 }}>
                        <a href={`/keystone-aviation/${aircraft.slug}`}>
                          <div className="card">
                            {aircraft.metadata.photo_gallery.featured_image.url !== 'https://s3-us-west-2.amazonaws.com/cosmicjs/' &&
                              <img width="100%" src={aircraft.metadata.photo_gallery.featured_image.url}
                                sizes="(max-width: 1024px) 100vw, 1024px" />}
                            <h2 className="aircraft-title">{aircraft.title}</h2>
                          </div>
                        </a>
                      </article>
                    </Grid>
                  )}
                </Grid>
              </div>
            }
            {this.state.isRep &&
              <RequestQuote
                modalOpen={this.state.modalOpen}
                onModalClose={this.handleModalClose.bind(this)}
                emailValue={this.state.modalEmail}
                handleSubmit={this.handleModalSubmit.bind(this)}
                onEmailChange={this.handleModalEmailChange.bind(this)}
                NameValue={this.state.modalName}
                onNameChange={this.handleModalNameChange.bind(this)}
                phoneValue={this.state.modalPhone}
                onPhoneChange={this.handleModalPhoneChange.bind(this)}
                companyValue={this.state.modalCompany}
                onCompanyChange={this.handleModalCompanyChange.bind(this)}
                commentsValue={this.state.modalComments}
                onCommentsChange={this.handleModalCommentsChange.bind(this)}
                submitted={this.state.modalSubmitted}
                submitting={this.state.modalSubmitting}
                failedToSumbit={this.state.modalFailedSubmit}
                onCaptchaChange={this.handleModalCaptchaChange.bind(this)}
                salesman={this.state.page.metadata.salesman[0].metadata ? this.state.page.metadata.salesman[0].metadata : null}
              />
            }
          </article>
        )
    );
  }
}