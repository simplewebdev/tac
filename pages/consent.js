import React from 'react';
import PageNotFound from '../components/views/404';
import Request from '../utils/request';
import { mapPage } from "../utils/helper";
import Hero from '../components/views/hero';
import ContactCTA from '../components/views/contact-cta';
import SectionHead from '../components/views/section-head-consent';
import Head from 'next/head';
import Grid from '@material-ui/core/Grid';
import ArrowButton from '../components/buttons/ArrowButton';
import Post from '../utils/post';


export default class AboutPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject('consent', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }

    return { page }
  }

  constructor(props) {
    super(props);
    this.state = {
      page: props.page,
      hero: props.page.hero_banner,
      consent: props.page.about,
     
    };

  }


  render() {

    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'home'}>
            <Head>
              <title className="mytesting">TAC - Consent</title>
            </Head>
            {
              this.state.hero.children[1].value !== '' || this.state.hero.children[2].value !== '' &&
              <Hero hero={this.state.hero} />
            }
            <SectionHead 
            bgColor='white' 
            sectionHead={this.state.consent.children}
            />
          </article>
        )
    );
  }
}
