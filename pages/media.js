import React from 'react'
import Page from '../components/views/page'
import PageNotFound from '../components/views/404'
import { withRouter } from 'next/router';
import Request from '../utils/request';
import Masonry from 'react-masonry-component';
import Grid from '@material-ui/core/Grid';
import Sidebar from '../components/shared/sidebar';
import Head from 'next/head';
import Archives from '../utils/archives';

class NewsroomMediaPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;
    let newsroomMeta;
    let sidebarPosts = [];
    let posts;

    try {
      const ResponseSidebarPosts = await Request.getPosts(4);
      const ResponseAllPosts = await Request.getPosts(null);
      const ResponseNewsroomMeta = await Request.getObject('news-and-views');
      sidebarPosts = ResponseSidebarPosts.objects;
      posts = ResponseAllPosts.objects;
      newsroomMeta = ResponseNewsroomMeta.object;
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }
    return { sidebarPosts, newsroomMeta, posts, query }
  }

  constructor(props){
    super(props);

    this.state = {
      sidebarPosts: props.sidebarPosts,
      newsroom: props.newsroomMeta,
      posts: props.posts,
      archives: []
    }
  }

  componentDidMount() {
    const { posts } = this.state;
    this.setState(state => {
      return {
        archives: Archives.filterPostsDateYear(posts)
      }
    });
  }

  render() {
    let heroTitle = 'Newsroom: Media Graphics';
    let heroImage = this.state.newsroom.metadata.hero_image.url;
    let heroExcerpt = '';
    let heroLink = '';
    let heroVideo = '';
    let isVideo = false;

    return (
      <div>
        <Head>
          <title>{`TAC - Media Graphics`}</title>
        </Head>
        <div style={{ height: '60vh', width: '100%', position: 'relative' }}>
          <div
            className="hero-img"
            style={{
              //display: isVideo && 'none',
              background: isVideo ? 'rgba(0,0,0,0.15)' : `url(${heroImage})`,
              //zIndex: isVideo && 1,
              backgroundSize:'cover',
              backgroundPosition:'center'
            }}
          />
          {
            isVideo &&
            <ReactPlayer
              url={heroVideo}
              playing
              playsinline
              autoPlay
              muted
              loop
              width="100%"
              height="100%"
              className='player'
            />
          }
          <div className="header-container hero-wrapper">
            <div>
              <h1 className="hero-title" style={{color: 'white', maxWidth: 660}}>TAC Newsroom <br/> Media Graphics</h1>
              {
                heroExcerpt === '' ? null :
                  <p className="hero-excerpt" style={{maxWidth: 390, margin: '10px 0px'}}>{heroExcerpt}</p>
              }
              {
                heroLink === '' ? null :
                  <ArrowButton link={heroLink} color='white' phrase='learn more'/>
              }
            </div>
          </div>
        </div>
        <div style={{padding: '60px 0px'}} className="container">
          <Grid container spacing={8}>
            <Grid item xs={12} sm={12} md={9} >
              <h2 style={{fontWeight: 600, marginBottom: 25}}>Click on a graphic below to download in PDF format </h2>
              <a target={"_blank"} href={'https://cosmic-s3.imgix.net/30210740-966e-11e8-ab08-31f1ed4cd97d-tac-full-logo.svg'}>
                <img style={{margin: 10, marginBottom: 50}} width={'45%'} src={'https://cosmic-s3.imgix.net/30210740-966e-11e8-ab08-31f1ed4cd97d-tac-full-logo.svg'}/>
              </a>
              <a target={"_blank"} href={'https://s3-us-west-2.amazonaws.com/cosmicjs/04a36f60-9fef-11e8-a649-bbc58f269540-Tac_Energy_Logo.pdf'}>
                <img style={{margin: 10, marginBottom: 50}} width={'45%'} src={'https://cosmic-s3.imgix.net/9376e000-758d-11e8-9c42-75c5f9571e6d-TACenergy.png'}/>
              </a>
              <a target={"_blank"} href={'https://s3-us-west-2.amazonaws.com/cosmicjs/079cd940-9fef-11e8-baa7-297ee0900870-TAC_Air_Logo.pdf'}>
                <img style={{margin: 10, marginBottom: 50}} width={'45%'} src={'https://cosmic-s3.imgix.net/937384a0-758d-11e8-ac9f-85d733f58489-TAC%20Air.png'}/>
              </a>
              <a target={"_blank"} href={'https://cosmic-s3.imgix.net/93a67b80-758d-11e8-9c42-75c5f9571e6d-Keystone%20Aviation.png'}>
                <img style={{margin: 10, marginBottom: 50}} width={'45%'} src={'https://cosmic-s3.imgix.net/93a67b80-758d-11e8-9c42-75c5f9571e6d-Keystone%20Aviation.png'}/>
              </a>
            </Grid>
            <Grid item xs={12} sm={12} md={3}>
              <Sidebar 
                posts={this.state.sidebarPosts} 
                archives={this.state.archives}
                pageName={this.props.query.pagename}
                 />
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}
export default withRouter(NewsroomMediaPage);
