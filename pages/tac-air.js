import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import Post from '../utils/post';
import { mapPage } from "../utils/helper";
import Hero from '../components/views/main-page-hero';
import SectionHead from '../components/views/section-head';
import SectionBlurb from '../components/views/section-blurb';
import ContactCTA from '../components/views/contact-air-modal';
import Grid from '@material-ui/core/Grid';
import ArrowButton from '../components/buttons/ArrowButton';
import Paper from '@material-ui/core/Paper';
import Head from 'next/head';
import SVG from 'react-inlinesvg';
import moment from "moment/moment";
import RequestModal from '../components/views/fbo-service-request';
import AirCTA from '../components/views/air-cta';

export default class TACAirPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject('tac-air', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }

    return { page }
  }

  constructor(props) {
    super(props);
    this.state = {
      page: props.page,
      hero: props.page.hero_banner,
      cta: props.page.cta,
      about: props.page.about,
      blurb: props.page.blurb,
      services: props.page.services,
      serviceCards: props.page.services.children[3].children,
      name: "",
      email: "",
      phone: "",
      company: "",
      title: "",
      tailNumber: "",
      selectFBO: "",
      message: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalDepDateTime: "",
      modalArrDateTime: "",
      modalTailNumber: "",
      modalFBO: "",
      commentsModalOpen: false,
    }
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
      company: "",
      title: "",
      tailNumber: "",
      selectFBO: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/tac-air-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
        company: this.state.company,
        title: this.state.title,
        tail_number: this.state.tailNumber,
        fbo: this.state.selectFBO,
      })
    }).then((res) => {

      if (res.status === 200) {
        let fboVal = this.state.selectFBO;
        let fboRec;
        switch (fboVal) {
          case 'ama':
            fboRec = "Dist_WebComments_AMA@tacair.com";
            break;
          case 'apa':
            fboRec = "Dist_WebComments_APA@tacair.com";
            break;
          case 'bdl':
            fboRec = "Dist_WebComments_BDL@tacair.com";
            break;
          case 'dal':
            //fboRec = "jgibney@tacair.com";
            fboRec = "Dist_WebComments_Kdal@tacenergy.com";
            break;
          case 'fsm':
            fboRec = "Dist_WebComments_FSM@tacair.com";
            break;
          case 'lex':
            fboRec = "Dist_WebComments_LEX@tacair.com";
            break;
          case 'lit':
            fboRec = "Dist_WebComments_LIT@tacair.com";
            break;
          case 'oma':
            fboRec = "Dist_WebComments_OMA@tacair.com";
            break;
          case 'pvu':
            fboRec = "Dist_WebComments_PVU@tacair.com";
            break;
          case 'rdu':
            fboRec = "Dist_WebComments_RDU@tacair.com";
            break;
          case 'shv':
            fboRec = "Dist_WebComments_SHV@tacair.com";
            break;
          case 'slc':
            fboRec = "Dist_WebComments_SLC@tacair.com";
            break;
          case 'sus':
            fboRec = "Dist_WebComments_SUS@tacair.com";
            break;
          case 'txk':
            fboRec = "Dist_WebComments_TXK@tacair.com";
            break;
          case 'tys':
            fboRec = "Dist_WebComments_TYS@tacair.com";
            break;
          default:
            fboRec = "marketing@tacenergy.com";

        }
        let title = 'Send Comments Form - TAC Air';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>`
          + `<b>Company: ${this.state.company}</b><br>`
          + `<b>Title: ${this.state.title}</b><br>`
          + `<b>Tail Number: ${this.state.tailNumber}</b><br>`
          + `<b>FBO: ${this.state.selectFBO}</b><br>`
          + `<b>Sent to: ${fboRec}</b><br>` +
          `</div>`;
        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({ submitted: true });

      } else {
        this.setState({ submitted: false, submitting: false, failedSubmit: true });
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePhoneChange = (event) => {
    this.setState({ phone: event.target.value });
  };

  handleMessageChange = (event) => {
    this.setState({ message: event.target.value });
  };

  handleCompanyChange = (event) => {
    this.setState({ company: event.target.value });
  };

  handleTitleChange = (event) => {
    this.setState({ title: event.target.value });
  };

  handleTailNumberChange = (event) => {
    this.setState({ tailNumber: event.target.value });
  };

  handleFBOChange = (event) => {
    this.setState({ selectFBO: event.target.value });
  };

  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalDepDateTime: "",
      modalArrDateTime: "",
      modalTailNumber: "",
      modalFBO: "",
    })
  };

  handleModalSubmit = (event) => {
    this.setState({ modalSubmitting: true });
    event.preventDefault();

    fetch('/api/fbo-service-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.modalName,
        email: this.state.modalEmail,
        phone: this.state.modalPhone,
        message: this.state.modalComments,
        departure_date_time: moment(this.state.modalDepDateTime).format('MM/DD/YYYY h:mm a'),
        arrival_date_time: moment(this.state.modalArrDateTime).format('MM/DD/YYYY h:mm a'),
        tail_number: this.state.modalTailNumber,
        fbo: this.state.modalFBO,
      })
    }).then((res) => {

      if (res.status === 200) {
        let fboVal = this.state.modalFBO;
        let fboRec;
        switch (fboVal) {
          case 'ama':
            fboRec = 'amafbo@tacair.com';
            break;
          case 'apa':
            fboRec = 'apafbo@tacair.com';
            break;
          case 'bdl':
            fboRec = 'bdlfbo@tacair.com';
            break;
          case 'dal':
            //fboRec = 'jgibney@tacair.com';
            fboRec = 'DIST_FBO_DAL@tacenergy.com';
            break;
          case 'fsm':
            fboRec = 'fsmfbo@tacair.com';
            break;
          case 'lex':
            fboRec = 'lexfbo@tacair.com';
            break;
          case 'lit':
            fboRec = 'litfbo@tacair.com';
            break;
          case 'oma':
            fboRec = 'omafbo@tacair.com';
            break;
          case 'pvu':
            fboRec = 'pvufbo@tacair.com';
            break;
          case 'rdu':
            fboRec = 'rdufbo@tacair.com';
            break;
          case 'shv':
            fboRec = 'shvfbo@tacair.com';
            break;
          case 'slc':
            fboRec = 'slcfbo@tacair.com';
            break;
          case 'sus':
            fboRec = 'susfbo@tacair.com';
            break;
          case 'txk':
            fboRec = 'txkfbo@tacair.com';
            break;
          case 'tys':
            fboRec = 'tysfbo@tacair.com';
            break;
          default:
            fboRec = "marketing@tacenergy.com";
        }
        let title = 'FBO Service Request ' + this.state.modalFBO;
        let html =
          `<div>`
          + `<b>New FBO service request from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Arrival Date/Time: ${moment(this.state.modalArrDateTime).format('MM/DD/YYYY h:mm a')}</b><br>`
          + `<b>Departure Date/Time: ${moment(this.state.modalDepDateTime).format('MM/DD/YYYY h:mm a')}</b><br>`
          + `<b>Tail Number: ${this.state.modalTailNumber}</b><br>`
          + `<b>FBO: ${this.state.modalFBO}</b><br>`
          + `<b>Sent To: ${fboRec}</b><br>` +
          `</div>`;
        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({ modalSubmitted: true });
      } else {
        this.setState({ modalSubmitted: false, modalSubmitting: false, modalFailedSubmit: true });
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({ modalName: event.target.value });
  };

  handleModalPhoneChange = (event) => {
    this.setState({ modalPhone: event.target.value });
  };

  handleModalEmailChange = (event) => {
    this.setState({ modalEmail: event.target.value });
  };

  handleModalCommentsChange = (event) => {
    this.setState({ modalComments: event.target.value });
  };

  handleModalDepDateTimeChange = event => {
    this.setState({ modalDepDateTime: event.target.value });
  };

  handleModalArrDateTimeChange = event => {
    this.setState({ modalArrDateTime: event.target.value });
  };

  handleModalTailNumberChange = event => {
    this.setState({ modalTailNumber: event.target.value });
  };

  handleModalFBOChange = event => {
    this.setState({
      modalFBO: event.target.value,
      modalFBOEmail: event.target[event.target.selectedIndex].getAttribute('data-tacair-email')
    });
  };

  handleOnClickArrow = () => {
    this.setState({ modalOpen: true });
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false
    });
    this.setState({ modalOpen: !this.state.modalOpen });
  };

  handleOnClickArrowComments = () => {
    this.setState({ commentsModalOpen: true });
  };

  handleCommentsModalClose = () => {
    this.resetModalForm();
    this.setState({
      submitted: false,
      submitting: false,
      failedSubmit: false
    });
    this.setState({ commentsModalOpen: !this.state.commentsModalOpen });
  };

  render() {
    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'air'}>
            <Head>
              <title>TAC Air</title>
            </Head>
            {
              this.state.hero.children[1].value !== '' || this.state.hero.children[2].value !== '' &&
              <Hero hero={this.state.hero} headingWidth={700} excerptWidth={475} />
            }
            <SectionHead cta={<a href='/tac-air/locations'><ArrowButton phrase='view locations' /></a>} bgColor='red' sectionHead={this.state.about.children} />
            <section id="locations" className="air-locations" style={{ marginBottom: 100, marginTop: 100 }}>
              <div className="container">
                <h1 style={{ color: 'white' }}>TAC Air FBO Locations</h1>
                <Grid style={{ justifyContent: 'center', alignItems: 'center' }} container spacing={8}>
                  <Grid item md={6}>
                    <Grid container spacing={8}>
                      <Grid className="location-list" item xs={12} sm={6}>
                        <a href="/tac-air/ama">AMA – Amarillo, TX</a>
                        <a href="/tac-air/apa">APA – Denver, CO</a>
                        <a href="/tac-air/bdl">BDL – Hartford, CT</a>
                        <a href="/tac-air/dal">DAL – Dallas, TX</a>
                        <a href="/tac-air/fsm">FSM – Fort Smith, AR</a>
                        <a href="/tac-air/lex">LEX – Lexington, KY</a>
                        <a href="/tac-air/lit">LIT – Little Rock, AR</a>
                        <a href="/tac-air/oma">OMA – Omaha, NE</a>
                      </Grid>
                      <Grid className="location-list" item xs={12} sm={6}>
                        <a href="/tac-air/pvu">PVU – Provo, UT</a>
                        <a href="/tac-air/rdu">RDU – Raleigh-Durham, NC</a>
                        <a href="/tac-air/slc">SLC – Salt Lake City, UT</a>
                        <a href="/tac-air/shv">SHV – Shreveport, LA</a>
                        <a href="/tac-air/sus">SUS – St. Louis, MO</a>
                        <a href="/tac-air/txk">TXK – Texarkana, AR</a>
                        <a href="/tac-air/tys">TYS – Knoxville, TN</a>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid style={{ padding: 20 }} item md={6}>
                    <SVG src="../static/images/tac-air-map.svg" />
                  </Grid>
                </Grid>
                <div style={{ justifyContent: 'flex-end', display: 'flex' }}>
                  <a href='/tac-air/locations'>
                    <ArrowButton phrase='view locations' />
                  </a>
                </div>
              </div>
            </section>
            <SectionBlurb sectionBlurb={this.state.blurb.children} />
            <section id="services" className="tac-air-services">
              <SectionHead bgColor='blue' float='right' sectionHead={this.state.services.children} />
              <div className='container' style={{ padding: '75px 0px' }}>
                <Grid container spacing={40}>
                  {
                    !!this.state.serviceCards && this.state.serviceCards.map((card, index) =>
                      <Grid key={index} item xs={12} sm={6} style={{ padding: 30 }}>
                        <Paper elevation={5} className='card'>
                          <a href={`${card.children[3].value}`} style={{ padding: 40, height: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'center' }} key={`card_${index}`}>
                            <img src={`${`https://s3-us-west-2.amazonaws.com/cosmicjs/${card.children[0].value}`}`} style={{ height: '55px', alignSelf: 'baseline' }} />
                            <h4 style={{ textTransform: 'uppercase', color: '#4c4b4b', margin: 0, padding: '15px 0px' }}>{card.value}</h4>
                            <div style={{ padding: '5px 0px', fontSize: 12, color: '#626262' }}>
                              {card.children[2].value}
                            </div>
                            <ArrowButton color='#e2383f' />
                          </a>
                        </Paper>
                      </Grid>
                    )
                  }
                </Grid>
                <div id="air-get-in-touch" />
              </div>
            </section>
            <AirCTA
              handleOnClickArrow={this.handleOnClickArrow.bind(this)}
              handleOnClickArrowComments={this.handleOnClickArrowComments.bind(this)}
            />
            <section>
              <RequestModal
                modalOpen={this.state.modalOpen}
                onModalClose={this.handleModalClose.bind(this)}
                emailValue={this.state.modalEmail}
                handleSubmit={this.handleModalSubmit.bind(this)}
                onEmailChange={this.handleModalEmailChange.bind(this)}
                NameValue={this.state.modalName}
                onNameChange={this.handleModalNameChange.bind(this)}
                phoneValue={this.state.modalPhone}
                onPhoneChange={this.handleModalPhoneChange.bind(this)}
                commentsValue={this.state.modalComments}
                onCommentsChange={this.handleModalCommentsChange.bind(this)}
                depDateValue={this.state.modalDepDateTime}
                onDepDateChange={this.handleModalDepDateTimeChange.bind(this)}
                arrDateValue={this.state.modalArrDateTime}
                onArrDateChange={this.handleModalArrDateTimeChange.bind(this)}
                tailNumberValue={this.state.modalTailNumber}
                onTailNumberChange={this.handleModalTailNumberChange.bind(this)}
                fboValue={this.state.modalFBO}
                onFboChange={this.handleModalFBOChange.bind(this)}
                submitted={this.state.modalSubmitted}
                submitting={this.state.modalSubmitting}
                failedToSumbit={this.state.modalFailedSubmit}
              />
              <ContactCTA
                modalOpen={this.state.commentsModalOpen}
                onModalClose={this.handleCommentsModalClose.bind(this)}
                emailValue={this.state.email}
                handleSubmit={this.handleSubmit.bind(this)}
                onEmailChange={this.handleEmailChange.bind(this)}
                nameValue={this.state.name}
                onNameChange={this.handleNameChange.bind(this)}
                phoneValue={this.state.phone}
                onPhoneChange={this.handlePhoneChange.bind(this)}
                messageValue={this.state.message}
                onMessageChange={this.handleMessageChange.bind(this)}
                companyValue={this.state.company}
                onCompanyChange={this.handleCompanyChange.bind(this)}
                titleValue={this.state.title}
                onTitleChange={this.handleTitleChange.bind(this)}
                tailNumberValue={this.state.tailNumber}
                onTailNumberChange={this.handleTailNumberChange.bind(this)}
                fboValue={this.state.selectFBO}
                onFBOChange={this.handleFBOChange.bind(this)}
                submitted={this.state.submitted}
                submitting={this.state.submitting}
                failedToSumbit={this.state.failedSubmit}
              />
            </section>
          </article>
        )
    );
  }
}