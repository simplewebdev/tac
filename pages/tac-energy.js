import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import CardSet from "../components/views/cards";
import { mapPage } from "../utils/helper";
import Hero from '../components/views/main-page-hero';
import SectionHead from '../components/views/section-head';
import CTA from '../components/views/cta';
import Grid from '@material-ui/core/Grid';
import ArrowButton from '../components/buttons/ArrowButton';
import RequestQuote from '../components/views/request-quote-modal';
import Head from 'next/head';
import Router from 'next/router';
import Post from '../utils/post';

export default class TACEnergyPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject('tac-energy', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }

    return { page }
  }

  constructor(props) {
    super(props);
    this.requestRef = React.createRef();
    this.state = {
      page: props.page,
      hero: props.page.hero_banner,
      cta: props.page.cta,
      about: props.page.about,
      blurb: props.page.blurb,
      bulk_trading: props.page.bulk_trading,
      products_services: props.page.products_services,
      supply: props.page.supply,
      firstName: "",
      lastName: "",
      company: "",
      states: "",
      email: "",
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalState: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalCaptcha: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    };
    this.handleOnClickArrow = this.handleOnClickArrow.bind(this);
  }

  componentDidMount() {
    if (/request-a-quote/.test(window.location.href)) {
      this.setState({ modalOpen: true });
    }
  }

  resetForm = () => {
    this.setState({
      firstName: "",
      lastName: "",
      company: "",
      email: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/cc-market-talk-subscribe', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "lists": [
          {
            "id": "881"
          }
        ],
        "company_name": this.state.company,
        "confirmed": false,
        "email_addresses": [
          {
            "email_address": this.state.email
          }
        ],
        "first_name": this.state.firstName,
        "last_name": this.state.lastName
      })
    }).then((res) => {
      if (res.status === 200) {
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  handleFirstNameChange = (event) => {
    this.setState({ firstName: event.target.value });
  };

  handleLastNameChange = (event) => {
    this.setState({ lastName: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handleCompanyChange = (event) => {
    this.setState({ company: event.target.value });
  };



  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalState: "",
      modalPhone: "",
      modalComments: "",
    })
  };

  handleModalSubmit = (event) => {
    this.setState({ modalSubmitting: true });
    event.preventDefault();

    fetch('/api/energy-quote-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.modalEmail,
        name: this.state.modalName,
        phone: this.state.modalPhone,
        company: this.state.modalCompany,
        states: this.state.modalState,
        message: this.state.modalComments
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'Energy Quote Request - Main Page';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Company: ${this.state.modalCompany}</b><br>`
          + `<b>State: ${this.state.modalState}</b><br>`
          + `<b>Sent to: marketing@tacenergy.com, sales@tacenergy.com</b><br>` +
          `</div>`;
        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({ modalSubmitted: true });
      } else {
        this.setState({ modalSubmitted: false, modalSubmitting: false, modalFailedSumbit: true });
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({ modalName: event.target.value });
  };

  handleModalPhoneChange = (event) => {
    this.setState({ modalPhone: event.target.value });
  };

  handleModalEmailChange = (event) => {
    this.setState({ modalEmail: event.target.value });
  };

  handleModalCompanyChange = (event) => {
    this.setState({ modalCompany: event.target.value });
  };

  handleModalStateChange = (event) => {
    this.setState({ modalState: event.target.value });
  };

  handleModalCommentsChange = (event) => {
    this.setState({ modalComments: event.target.value });
  };

  handleModalCaptchaChange = (value) => {
    this.setState({ modalCaptcha: value });
  };

  handleOnClickArrow() {
    this.setState({ modalOpen: true });
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false,
      modalOpen: !this.state.modalOpen
    });
    Router.push('/tac-energy');
  };

  render() {
    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'energy'}>
            <Head>
              <title>TAC Energy</title>
            </Head>
            {
              this.state.hero.children[1].value !== '' || this.state.hero.children[2].value !== '' &&
              <Hero hero={this.state.hero} />
            }
            <SectionHead sectionHead={this.state.about.children} />
            <div style={{ paddingTop: 50 }} className="container">
              <div id="request-a-quote" className={'section-blurb'}>
                <Grid container spacing={40}>
                  <Grid item sm={12} md={4} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <img src={this.state.blurb.children[0].url} alt="" height={"50px"} />
                  </Grid>
                  <Grid item sm={12} md={4} style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
                    <em style={{ fontWeight: 600, letterSpacing: '0.8px', lineHeight: '28px' }}>{this.state.blurb.children[2].value}</em>
                    <a onClick={this.handleOnClickArrow}>
                      <ArrowButton color='#e2383f' phrase='Request a Quote' />
                    </a>
                  </Grid>
                  <Grid item sm={12} md={4} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <p style={{ fontSize: '14px', lineHeight: '21px', letterSpacing: '1px' }}>
                      {this.state.blurb.children[1].value}
                    </p>
                  </Grid>
                </Grid>
              </div>
            </div>
            <div id="products-services">
              <SectionHead sectionHead={this.state.products_services.children} bgColor='blue' />
              <CardSet iconHeight="55px" cardSet={this.state.products_services.children[3].children} />
            </div>
            <div id="supply">
              <SectionHead sectionHead={this.state.supply.children} float='right' bgColor='white' />
              <CardSet iconHeight="55px" cardSet={this.state.supply.children[3].children} />
            </div>
            <div style={{ marginBottom: 100 }} id="bulk-trading">
              <SectionHead sectionHead={this.state.bulk_trading.children} bgColor='red' />
            </div>
            <section id="market-talk-updates">
              <CTA
                cta={this.state.cta}
                emailValue={this.state.email}
                handleSubscribe={this.handleSubmit.bind(this)}
                onEmailChange={this.handleEmailChange.bind(this)}
                firstNameValue={this.state.firstName}
                onFirstNameChange={this.handleFirstNameChange.bind(this)}
                lastNameValue={this.state.lastName}
                onLastNameChange={this.handleLastNameChange.bind(this)}
                companyValue={this.state.company}
                onCompanyChange={this.handleCompanyChange.bind(this)}
                logoHeight='50px'
                bgColor='#8b8b8b'
                btnColor='#0b559c'
                submitted={this.state.submitted}
                submitting={this.state.submitting}
                failedToSumbit={this.state.failedSubmit}
              />
            </section>
            <RequestQuote
              modalOpen={this.state.modalOpen}
              onModalClose={this.handleModalClose.bind(this)}
              emailValue={this.state.modalEmail}
              handleSubmit={this.handleModalSubmit.bind(this)}
              onEmailChange={this.handleModalEmailChange.bind(this)}
              NameValue={this.state.modalName}
              onNameChange={this.handleModalNameChange.bind(this)}
              phoneValue={this.state.modalPhone}
              onPhoneChange={this.handleModalPhoneChange.bind(this)}
              companyValue={this.state.modalCompany}
              onCompanyChange={this.handleModalCompanyChange.bind(this)}

              stateValue={this.state.modalState}
              onStateChange={this.handleModalStateChange.bind(this)}

              commentsValue={this.state.modalComments}
              onCommentsChange={this.handleModalCommentsChange.bind(this)}
              submitted={this.state.modalSubmitted}
              submitting={this.state.modalSubmitting}
              failedToSumbit={this.state.modalFailedSubmit}
              onCaptchaChange={this.handleModalCaptchaChange.bind(this)}
            />
          </article>
        )
    );
  }
}