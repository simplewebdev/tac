import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import { mapPage } from "../utils/helper";
import Hero from '../components/views/hero';
import ContactCTA from '../components/views/contact-cta';
import Grid from '@material-ui/core/Grid';
import ArrowButton from '../components/buttons/ArrowButton';
import Head from 'next/head';
import Post from '../utils/post';

export default class KeystoneSalesPage extends React.Component {

  static async getInitialProps({req, query}) {
    let page;
    let charter;

    try {
      const fleet = await Request.getAircraftsForSale(4, 'random');
      charter = fleet.objects;
      const pageResponse = await Request.getObject('sales-and-brokerage', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }
    return { page, charter }
  }

  constructor(props){
    super(props);
    this.state = {
      page: props.page,
      hero: props.page.hero_banner,
      cta: props.page.cta,
      about: props.page.about,
      name: "",
      email: "",
      phone: "",
      message: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
      charter: props.charter
    }
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({submitting: true});
    event.preventDefault();

    fetch('/api/sales-brokerage-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
      })
    }).then((res)=>{
      if (res.status === 200){
        let title = 'Keystone Aviation Contact - Sales & Brokerage';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({submitted: true});
      }else {
        this.setState({submitted: false, submitting: false, failedSumbit: true});
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({name: event.target.value});
  };

  handleEmailChange = (event) => {
    this.setState({email: event.target.value});
  };

  handlePhoneChange = (event) => {
    this.setState({phone: event.target.value});
  };

  handleMessageChange = (event) => {
    this.setState({message: event.target.value});
  };

  render() {

    let bgColor = 'white';
    let float = 'right';
    let sectionHead = this.state.about.children;

    let sectionHeadBackground = sectionHead[2].imgix_url + '?w=2000';
    let sectionHeadTitle = sectionHead[0].value;
    let sectionHeadExc = sectionHead[1].value;

    let cardColor =
      bgColor === 'blue' ? 'rgba(0, 83, 160, 0.96)' :
        bgColor === 'red' ? 'rgba(226, 56, 63, 0.96)' : 'rgba(255, 255, 255, 0.96)';

    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
        <article className={'keystone'}>
          <Head>
            <title>{`Keystone Aviation - Sales and Brokerage`}</title>
          </Head>
          {
            this.state.hero.children[1].value !== '' || this.state.hero.children[2].value !== '' &&
            <Hero hero={this.state.hero} headingWidth={800}/>
          }
          <section style={{
            position: 'relative',
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
            height: '100%',
            justifyContent: float === 'right' ? 'flex-start' : 'flex-end'
          }}>
            <div
              style={{background: cardColor }}
              className={float === 'right' ? 'banner-card right' : 'banner-card'}
            >
              <div className="banner-card-inner">
                <h1 style={{
                  color:  bgColor === 'blue' ? 'white' : bgColor === 'red' ? 'white' : '#4c4b4b'
                }}>{sectionHeadTitle}</h1>
                <div style={{
                  color: bgColor === 'blue' ? '#f3f3f3' : bgColor === 'red' ? '#f3f3f3' : '#626262',
                }}>
                  {sectionHeadExc}
                  <a href='/keystone-aviation/sales-and-brokerage#contact' style={{ marginTop: 15}}>
                    <ArrowButton color='#e2383f' phrase='Contact Sales'/>
                  </a>
                </div>
              </div>
            </div>
            <div className="sectionHeadImg" style={{
              background: `url(${sectionHeadBackground})`,
              backgroundSize:'cover',
              backgroundPosition:'center'
            }}/>
          </section>
          <div className="container" style={{padding: '75px 0px'}}>
            <h1 style={{fontWeight: 500, textAlign: 'center'}}>Aircraft For Sale</h1>
            <Grid container spacing={8}>
              {!!this.state.charter && this.state.charter.map((aircraft, index) =>
                <Grid key={index} item xs={12} sm={6} md={3}>
                  <article style={{maxWidth: 400, padding: 10}}>
                    <a href={`/keystone-aviation/${aircraft.slug}`}>
                      <div className="card">
                        <img height="200px" width="100%" src={`${aircraft.metadata.photo_gallery.featured_image.imgix_url}?w=500`}/>
                        <h2 className="aircraft-title">{aircraft.title}</h2>
                      </div>
                    </a>
                  </article>
                </Grid>
              )}
            </Grid>
            <a href='/keystone-aviation/aircrafts-for-sale' style={{ display: 'flex', justifyContent: 'flex-end', marginRight: 15, marginTop: 15}}>
              <ArrowButton color='#e2383f' phrase='View All Aircraft'/>
            </a>
          </div>
          <section style={{marginTop: 100}} className="selling_processes">
            <div className="container">
              <h1 style={{color: 'white'}}>Our acquisition and selling processes include:</h1>
              <div className={'inner'} dangerouslySetInnerHTML={{ __html: this.state.page.selling_processes.value }}/>
            </div>
          </section>
          <section id="contact" style={{clear: 'both'}}>
          <ContactCTA
            cta={this.state.cta}
            emailValue={this.state.email}
            handleSubmit={this.handleSubmit.bind(this)}
            onEmailChange={this.handleEmailChange.bind(this)}
            nameValue={this.state.name}
            onNameChange={this.handleNameChange.bind(this)}
            phoneValue={this.state.phone}
            onPhoneChange={this.handlePhoneChange.bind(this)}
            messageValue={this.state.message}
            onMessageChange={this.handleMessageChange.bind(this)}
            bgColor='#8b8b8b'
            btnColor='#e2383f'
            submitted={this.state.submitted}
            submitting={this.state.submitting}
            failedToSumbit={this.state.failedSubmit}
          />
          </section>
        </article>
      )
    );
  }
}