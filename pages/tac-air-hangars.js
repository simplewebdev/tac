import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import ArrowButton from '../components/buttons/ArrowButton';
import AirCTA from '../components/views/air-cta';
import RequestModal from '../components/views/fbo-service-request';
import ContactCTA from '../components/views/contact-air-modal';
import Head from 'next/head';
import moment from "moment/moment";
import Post from '../utils/post';

export default class AirHangarsPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject('hangars', query.status, query.revision);
      page = pageResponse.object;
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { page }
  }

  constructor(props){
    super(props);

    this.state = {
      page: props.page,
      cta: props.page.metafields[1] && props.page.metafields[1],
      name: "",
      email: "",
      phone: "",
      message: "",
      company: "",
      title: "",
      tailNumber: "",
      selectFBO: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalDepDateTime: "",
      modalArrDateTime: "",
      modalTailNumber: "",
      modalFBO: "",
      commentsModalOpen: false,
    }
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
      company: "",
      title: "",
      tailNumber: "",
      selectFBO: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({submitting: true});
    event.preventDefault();

    fetch('/api/tac-air-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
        company: this.state.company,
        title: this.state.title,
        tail_number: this.state.tailNumber,
        fbo: this.state.selectFBO,
      })
    }).then((res)=>{
      if (res.status === 200){
        let title = 'Send Comments Form - TAC Air - Hangars';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>`
          + `<b>Company: ${this.state.company}</b><br>`
          + `<b>Title: ${this.state.title}</b><br>`
          + `<b>Tail Number: ${this.state.tailNumber}</b><br>`
          + `<b>FBO: ${this.state.fbo}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({submitted: true});
      }else {
        this.setState({submitted: false, submitting: false, failedSumbit: true});
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({name: event.target.value});
  };

  handleEmailChange = (event) => {
    this.setState({email: event.target.value});
  };

  handlePhoneChange = (event) => {
    this.setState({phone: event.target.value});
  };

  handleMessageChange = (event) => {
    this.setState({message: event.target.value});
  };

  handleCompanyChange = (event) => {
    this.setState({company: event.target.value});
  };

  handleTitleChange = (event) => {
    this.setState({title: event.target.value});
  };

  handleTailNumberChange = (event) => {
    this.setState({tailNumber: event.target.value});
  };

  handleFBOChange = (event) => {
    this.setState({selectFBO: event.target.value});
  };

  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalDepDateTime: "",
      modalArrDateTime: "",
      modalTailNumber: "",
      modalFBO: "",
    })
  };

  handleModalSubmit = (event) => {
    this.setState({modalSubmitting: true});
    event.preventDefault();

    fetch('/api/fbo-service-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.modalName,
        email: this.state.modalEmail,
        phone: this.state.modalPhone,
        message: this.state.modalComments,
        departure_date_time: moment(this.state.modalDepDateTime).format('MM/DD/YYYY h:mm a'),
        arrival_date_time: moment(this.state.modalArrDateTime).format('MM/DD/YYYY h:mm a'),
        tail_number: this.state.modalTailNumber,
        fbo: this.state.modalFBO,
      })
    }).then((res)=>{
      if (res.status === 200){
        let title = 'FBO Service Request - ' + this.state.modalFBO;
        let html =
          `<div>`
          + `<b>New FBO service request from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Arrival Date/Time: ${moment(this.state.modalDepDateTime).format('MM/DD/YYYY h:mm a')}</b><br>`
          + `<b>Departure Date/Time: ${moment(this.state.modalArrDateTime).format('MM/DD/YYYY h:mm a')}</b><br>`
          + `<b>Tail Number: ${this.state.modalTailNumber}</b><br>`
          + `<b>FBO: ${this.state.modalFBO}</b><br>`+
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({modalSubmitted: true});
      }else {
        this.setState({modalSubmitted: false, modalSubmitting: false, modalFailedSubmit: true});
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({modalName: event.target.value});
  };

  handleModalPhoneChange = (event) => {
    this.setState({modalPhone: event.target.value});
  };

  handleModalEmailChange = (event) => {
    this.setState({modalEmail: event.target.value});
  };

  handleModalCommentsChange = (event) => {
    this.setState({modalComments: event.target.value});
  };

  handleModalDepDateTimeChange = event => {
    this.setState({ modalDepDateTime: event.target.value });
  };

  handleModalArrDateTimeChange = event => {
    this.setState({ modalArrDateTime: event.target.value });
  };

  handleModalTailNumberChange = event => {
    this.setState({ modalTailNumber: event.target.value});
  };

  handleModalFBOChange = event => {
    this.setState({ modalFBO: event.target.value});
  };

  handleOnClickArrow = () => {
    this.setState({modalOpen: true});
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false
    });
    this.setState({modalOpen: !this.state.modalOpen});
  };

  handleOnClickArrowComments = () => {
    this.setState({commentsModalOpen: true});
  };

  handleCommentsModalClose = () => {
    this.resetModalForm();
    this.setState({
      submitted: false,
      submitting: false,
      failedSubmit: false
    });
    this.setState({commentsModalOpen: !this.state.commentsModalOpen});
  };

  render() {

    return (
      this.props.page.component && this.props.page.component==='404' ? (
        <PageNotFound />
      ) : (
        <article className={'air'}>
          <Head>
            <title>{`TAC Air - Hangars`}</title>
          </Head>
          <section style={{
            position: 'relative',
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
            height: '100%',
            alignItems: 'center',
            justifyContent: 'flex-end'
          }}>
            <div className="subpageImg" style={{
              background: `url(${this.state.page.metafields[0].url ? this.state.page.metafields[0].imgix_url + '?w=2000' : 'https://cosmic-s3.imgix.net/3790ec30-7be2-11e8-8a7c-ed8ed617634d-1522.jpg'})`,
              backgroundSize:'cover',
              backgroundPosition:'center',
            }}/>
            <div
              style={{
                background: 'rgba(255, 255, 255, 0.80)',
              }}
              className='subpage-card'
            >
              <div className="subpage-inner">
                <div>
                  <h1>{ this.state.page.title }</h1>
                  <div dangerouslySetInnerHTML={{ __html: this.state.page.content }}/>
                  <a href={'/tac-air/locations'}>
                    <ArrowButton color='#e2383f' phrase='View Locations'/>
                  </a>
                </div>
              </div>
            </div>
          </section>
          <AirCTA
            handleOnClickArrow={this.handleOnClickArrow.bind(this)}
            handleOnClickArrowComments={this.handleOnClickArrowComments.bind(this)}
          />
          <section>
            <RequestModal
              modalOpen={this.state.modalOpen}
              onModalClose={this.handleModalClose.bind(this)}
              emailValue={this.state.modalEmail}
              handleSubmit={this.handleModalSubmit.bind(this)}
              onEmailChange={this.handleModalEmailChange.bind(this)}
              NameValue={this.state.modalName}
              onNameChange={this.handleModalNameChange.bind(this)}
              phoneValue={this.state.modalPhone}
              onPhoneChange={this.handleModalPhoneChange.bind(this)}
              commentsValue={this.state.modalComments}
              onCommentsChange={this.handleModalCommentsChange.bind(this)}
              depDateValue={this.state.modalDepDateTime}
              onDepDateChange={this.handleModalDepDateTimeChange.bind(this)}
              arrDateValue={this.state.modalArrDateTime}
              onArrDateChange={this.handleModalArrDateTimeChange.bind(this)}
              tailNumberValue={this.state.modalTailNumber}
              onTailNumberChange={this.handleModalTailNumberChange.bind(this)}
              fboValue={this.state.modalFBO}
              onFboChange={this.handleModalFBOChange.bind(this)}
              submitted={this.state.modalSubmitted}
              submitting={this.state.modalSubmitting}
              failedToSumbit={this.state.modalFailedSubmit}
            />
            <ContactCTA
              modalOpen={this.state.commentsModalOpen}
              onModalClose={this.handleCommentsModalClose.bind(this)}
              emailValue={this.state.email}
              handleSubmit={this.handleSubmit.bind(this)}
              onEmailChange={this.handleEmailChange.bind(this)}
              nameValue={this.state.name}
              onNameChange={this.handleNameChange.bind(this)}
              phoneValue={this.state.phone}
              onPhoneChange={this.handlePhoneChange.bind(this)}
              messageValue={this.state.message}
              onMessageChange={this.handleMessageChange.bind(this)}
              companyValue={this.state.company}
              onCompanyChange={this.handleCompanyChange.bind(this)}
              titleValue={this.state.title}
              onTitleChange={this.handleTitleChange.bind(this)}
              tailNumberValue={this.state.tailNumber}
              onTailNumberChange={this.handleTailNumberChange.bind(this)}
              fboValue={this.state.selectFBO}
              onFBOChange={this.handleFBOChange.bind(this)}
              submitted={this.state.submitted}
              submitting={this.state.submitting}
              failedToSumbit={this.state.failedSubmit}
            />
          </section>
        </article>
      )
    );
  }
}