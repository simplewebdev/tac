import React from 'react'
import Page from '../components/views/page'
import PageNotFound from '../components/views/404'
import { withRouter } from 'next/router';
import Request from '../utils/request';
import Masonry from 'react-masonry-component';
import Grid from '@material-ui/core/Grid';
import Sidebar from '../components/shared/sidebar';
import Head from 'next/head';
import CTA from '../components/views/cta';
import Archives from '../utils/archives';
import moment from 'moment';
import timezone from 'moment-timezone';

class CatDefaultPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let filteredPosts;
    let posts;
    let page;
    let newsroomMeta;
    let sidebarPosts = [];
    let pageCat = query.pagename;
    let categoryData;
    try {
      const ResponseAllPosts = await Request.filterPostByCat(query.pagename);
      const ResponseCategoryData = await Request.getObject(query.pagename);
      const ResponseSidebarPosts = await Request.getPosts(4);
      const Posts = await Request.getPosts(null);
      const ResponseNewsroomMeta = await Request.getObject('news-and-views');
      filteredPosts = ResponseAllPosts.objects;
      posts = Posts.objects;
      sidebarPosts = ResponseSidebarPosts.objects;
      newsroomMeta = ResponseNewsroomMeta.object;
      categoryData = ResponseCategoryData;
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }


    return { posts, filteredPosts, sidebarPosts, pageCat, newsroomMeta, categoryData, query }
  }

  constructor(props) {
    super(props);
    this.state = {
      filteredPosts: props.filteredPosts,
      posts: props.posts,
      sidebarPosts: props.sidebarPosts,
      categoryData: props.categoryData,
      newsroom: props.newsroomMeta,
      archives: [],
      cta: {
        value: 'Market Talk Updates',
        key: 'cta',
        title: 'CTA',
        type: 'parent',
        children:
          [{
            value: 'Market Talk Updates',
            key: 'title',
            title: 'Title',
            type: 'text',
            children: null
          },
          {
            value: 'a45afc40-86bc-11e8-b2a5-bf309249a48d-tac-energy-logo-wht.svg',
            key: 'image',
            title: 'Image',
            type: 'file',
            children: null,
            url: 'https://s3-us-west-2.amazonaws.com/cosmicjs/a45afc40-86bc-11e8-b2a5-bf309249a48d-tac-energy-logo-wht.svg',
            imgix_url: 'https://cosmic-s3.imgix.net/a45afc40-86bc-11e8-b2a5-bf309249a48d-tac-energy-logo-wht.svg'
          },
          {
            value: 'Sign up to receive market updates in your inbox each day.',
            key: 'excerpt',
            title: 'Excerpt',
            type: 'textarea',
            children: null
          }]
      },
      firstName: "",
      lastName: "",
      company: "",
      email: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    }
  }

  resetForm = () => {
    this.setState({
      firstName: "",
      lastName: "",
      company: "",
      email: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/cc-market-talk-subscribe', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "lists": [
          {
            "id": "881"
          }
        ],
        "company_name": this.state.company,
        "confirmed": false,
        "email_addresses": [
          {
            "email_address": this.state.email
          }
        ],
        "first_name": this.state.firstName,
        "last_name": this.state.lastName
      })
    }).then((res) => {
      if (res.status === 200) {
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  componentDidMount() {
    const { posts } = this.state;
    this.setState(state => {
      return {
        archives: Archives.filterPostsDateYear(posts)
      }
    });
  }

  handleFirstNameChange = (event) => {
    this.setState({ firstName: event.target.value });
  };

  handleLastNameChange = (event) => {
    this.setState({ lastName: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handleCompanyChange = (event) => {
    this.setState({ company: event.target.value });
  };

  onCardClick = (value) => {
    const { router } = this.props;
    if (typeof router === 'undefined') return;
    router.push(value);
  };

  render() {
    let { object } = this.state.categoryData;

    // client don't want to show the title on market-talk-news
    // category this is just a temporary solution to solve this.
    let heroTitle = object.slug === 'market-talk-news' ? '' : this.state.newsroom.metadata.hero_title;

    let heroImage = object.metadata !== null ? object.metadata.image.url : this.state.newsroom.metadata.hero_image.url;
    let heroExcerpt = '';
    let heroLink = '';
    let heroVideo = '';
    let isVideo = false;

    const childElements = this.state.filteredPosts.map((element, index) => {
      let postDate;
      if (element.metadata.created_at) {
        postDate = moment(element.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
      } else {
        postDate = moment(element.created_at).tz('America/Chicago').format('dddd D MMM YYYY');
      }
      let catLength = element.metadata.category.length;
      return (
        <article key={index} style={{ padding: 10, cursor: 'pointer' }} className="post-element-class" >
          <a style={{ display: 'block' }} href={`/news-and-views/${element.slug}`}>
            <img width="100%" style={{ verticalAlign: 'middle' }} src={`${element.metadata.featured_image.imgix_url}?w=500`} />
          </a>
          <div style={{ paddingLeft: '10%', paddingRight: '10%', paddingTop: 32, marginTop: '-5px' }} className="card">
            <div className="post-date">{postDate}</div>
            <a href={`/news-and-views/${element.slug}`}><h2 className="post-title">{element.title}</h2></a>
            <div className="post-teaser">{element.metadata.teaser}</div>
            <div className="post-categories">
              {element.metadata.category.map((cat, index) => {
                if (catLength === index + 1) {
                  return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}</a>)
                } else {
                  return (<a key={index} href={`/news-and-views/category/${cat.slug}`} style={{ fontSize: 13, textTransform: 'uppercase', color: '#8d8d8d' }}>{cat.title}, </a>)
                }
              })}
            </div>
          </div>
        </article>
      );
    });

    return (
      <section>
        <Head>
          <title>{`TAC - ${this.props.pageCat}`}</title>
        </Head>
        <div style={{ height: '60vh', width: '100%', position: 'relative' }}>
          <div
            className="hero-img"
            style={{
              //display: isVideo && 'none',
              background: isVideo ? 'rgba(0,0,0,0.15)' : `url(${heroImage})`,
              //zIndex: isVideo && 1,
              backgroundSize: 'cover',
              backgroundPosition: 'center'
            }}
          />
          {
            isVideo &&
            <ReactPlayer
              url={heroVideo}
              playing
              playsinline
              autoPlay
              muted
              loop
              width="100%"
              height="100%"
              className='player'
            />
          }
          <div className="header-container hero-wrapper">
            <div>
              <h1 className="hero-title" style={{ color: 'white', maxWidth: 660 }}>{heroTitle}</h1>
              {
                heroExcerpt === '' ? null : <p className="hero-excerpt" style={{ maxWidth: 390, margin: '10px 0px' }}>{heroExcerpt}</p>
              }
              {
                heroLink === '' ? null :
                  <ArrowButton link={heroLink} color='white' phrase='learn more' />
              }
            </div>
          </div>
        </div>
        <div style={{ padding: '60px 0px' }} className="container">
          <Grid container spacing={8}>

            <Grid item xs={12} sm={12} md={3} id="mobile_views">
              <Sidebar
                posts={this.state.sidebarPosts}
                archives={this.state.archives}
                pageName={this.props.query.pagename}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={9} >
              <Masonry
                className={'my-gallery-class'} // default ''

              >
                {childElements}
              </Masonry>
            </Grid>
            <Grid item xs={12} sm={12} md={3} id="desktop_views">
              <Sidebar
                posts={this.state.sidebarPosts}
                archives={this.state.archives}
                pageName={this.props.query.pagename}
              />
            </Grid>
          </Grid>
        </div>
        {
          (this.props.pageCat === 'tac-energy-news' || this.props.pageCat === 'market-talk-news') &&
          <section className={'energy'}>
            <CTA
              cta={this.state.cta}
              emailValue={this.state.email}
              handleSubscribe={this.handleSubmit.bind(this)}
              onEmailChange={this.handleEmailChange.bind(this)}
              firstNameValue={this.state.firstName}
              onFirstNameChange={this.handleFirstNameChange.bind(this)}
              lastNameValue={this.state.lastName}
              onLastNameChange={this.handleLastNameChange.bind(this)}
              companyValue={this.state.company}
              onCompanyChange={this.handleCompanyChange.bind(this)}
              logoHeight='50px'
              bgColor='#8b8b8b'
              btnColor='#0b559c'
              submitted={this.state.submitted}
              submitting={this.state.submitting}
              failedToSumbit={this.state.failedSubmit}
            />
          </section>
        }
      </section>
    );
  }
}

export default withRouter(CatDefaultPage);
