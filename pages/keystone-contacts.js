import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import Grid from '@material-ui/core/Grid';
import { mapPage } from "../utils/helper";
import Head from 'next/head';
import ContactCTA from '../components/views/contact-cta';
import Post from '../utils/post';

export default class EnergyContactsPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;
    let contacts;
    let pageTitle;

    try {
      contacts = await Request.getKeystoneContacts();
      const pageResponse = await Request.getObject('contacts', query.status, query.revision);
      page = mapPage(pageResponse.object);
      pageTitle = pageResponse.object.title;
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { page, contacts, pageTitle }
  }

  constructor(props) {
    super(props);
    this.requestRef = React.createRef();
    this.state = {
      page: props.page,
      pageTitle: props.pageTitle,
      contacts: props.contacts,
      name: "",
      email: "",
      phone: "",
      message: "",
      submitting: false,
      submitted: false,
      failedSubmit: false
    };
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/keystone-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'Keystone Aviation Contact - Home';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>`
          + `<b>Sent to: dchamberlain@keystoneaviation.com </b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePhoneChange = (event) => {
    this.setState({ phone: event.target.value });
  };

  handleMessageChange = (event) => {
    this.setState({ message: event.target.value });
  };

  render() {
    let contacts = this.state.contacts;

    return (
      this.props.page.component && this.props.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'energy keystone'}>
            <Head>
              <title>Keystone Aviation - Contacts</title>
            </Head>
            <section className="container location">
              <h1>{this.state.pageTitle}</h1>
            </section>
            <section className='container' style={{ paddingBottom: '75px' }}>
              <Grid container spacing={8}>
                {
                  !!contacts && contacts.objects.map((value, index) =>
                    <Grid key={index} item xs={12} sm={6} md={4} style={{ padding: 20 }}>
                      <div className="location-card contacts">
                        <h3>{value.title}</h3>
                        <div className="location-card-list contacts">
                          <div className="location-card-list-item contacts">
                            <div className="location-card-icon">
                              <img src="https://cosmic-s3.imgix.net/6f306e80-e4a2-11e8-bbfb-47935b0f92ad-ic_user.svg" height="25px" />
                            </div>
                            <div><a className="location-card-link" href={`mailto:${value.metadata.email}`}>{value.metadata.name}</a></div>
                          </div>
                          <div className="location-card-list-item contacts">
                            <div className="location-card-icon">
                              <img src="https://cosmic-s3.imgix.net/e6652cc0-891c-11e8-96d5-43db6338ad71-phone-call.svg" height="25px" />
                            </div>
                            <div>
                              <a className="location-card-link" href={`tel:${value.metadata.phone}`}>{value.metadata.phone}</a>
                            </div>
                          </div>
                          <div className="location-card-list-item contacts">
                            <div className="location-card-icon">
                              <img src="https://cosmic-s3.imgix.net/6c471ca0-e362-11e8-8955-7f53de970a57-ic_mail.svg" height="20px" />
                            </div>
                            <div><a className="location-card-link" href={`mailto:${value.metadata.email}`}>Email</a></div>
                          </div>
                        </div>
                      </div>
                    </Grid>
                  )
                }
              </Grid>
            </section>
            <ContactCTA
              cta={this.state.cta}
              emailValue={this.state.email}
              handleSubmit={this.handleSubmit.bind(this)}
              onEmailChange={this.handleEmailChange.bind(this)}
              nameValue={this.state.name}
              onNameChange={this.handleNameChange.bind(this)}
              phoneValue={this.state.phone}
              onPhoneChange={this.handlePhoneChange.bind(this)}
              messageValue={this.state.message}
              onMessageChange={this.handleMessageChange.bind(this)}
              bgColor='#8b8b8b'
              btnColor='#e2383f'
              submitted={this.state.submitted}
              submitting={this.state.submitting}
              failedToSumbit={this.state.failedSubmit}
            />
          </article>
        )
    );
  }
}