import React from 'react';
import PageNotFound from '../components/views/404';
import Request from '../utils/request';
import { mapPage } from "../utils/helper";
import Hero from '../components/views/hero';
import ContactCTA from '../components/views/contact-cta';
import SectionHead from '../components/views/section-head';
import Head from 'next/head';
import Grid from '@material-ui/core/Grid';
import ArrowButton from '../components/buttons/ArrowButton';
import Post from '../utils/post';

export default class AboutPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject('about', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }

    return { page }
  }

  constructor(props) {
    super(props);
    this.state = {
      page: props.page,
      hero: props.page.hero_banner,
      about: props.page.about,
      blurb: props.page.blurb,
      cta: {},
      name: "",
      email: "",
      phone: "",
      message: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    };

  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/tac-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'TAC Contact Form - About';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>`
          + `<b>Sent to: marketing@tacenergy.com</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePhoneChange = (event) => {
    this.setState({ phone: event.target.value });
  };

  handleMessageChange = (event) => {
    this.setState({ message: event.target.value });
  };


  render() {

    let sectionBlurb = this.state.blurb.children;

    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'home'}>
            <Head>
              <title className="mytesting">TAC - About 2</title>
            </Head>
            {
              this.state.hero.children[1].value !== '' || this.state.hero.children[2].value !== '' &&
              <Hero hero={this.state.hero} />
            }
            <SectionHead bgColor='blue' sectionHead={this.state.about.children} />
            <section className="container">
              <div className="section-blurb">
                <Grid container spacing={40}>
                  <Grid item xs={12} sm={12} md={4} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <img style={{ margin: 35, height: '100%', width: '100%' }} src={sectionBlurb[0].url} alt="" />
                  </Grid>
                  <Grid item xs={12} sm={12} md={4} style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
                    <em style={{ fontWeight: 600, letterSpacing: '0.8px', lineHeight: '28px' }}>{sectionBlurb[2].value}</em>
                    {sectionBlurb[3].value ? <ArrowButton link={sectionBlurb[3].value} color='#e2383f' phrase='Request a Quote' /> : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={4} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <p style={{ fontSize: '14px', lineHeight: '21px', letterSpacing: '1px' }}>
                      {sectionBlurb[1].value}
                    </p>
                  </Grid>
                </Grid>
              </div>
            </section>
            <ContactCTA
              cta={this.state.cta}
              emailValue={this.state.email}
              handleSubmit={this.handleSubmit.bind(this)}
              onEmailChange={this.handleEmailChange.bind(this)}
              nameValue={this.state.name}
              onNameChange={this.handleNameChange.bind(this)}
              phoneValue={this.state.phone}
              onPhoneChange={this.handlePhoneChange.bind(this)}
              messageValue={this.state.message}
              onMessageChange={this.handleMessageChange.bind(this)}
              submitted={this.state.submitted}
              submitting={this.state.submitting}
              failedToSumbit={this.state.failedSubmit}
            />
          </article>
        )
    );
  }
}
