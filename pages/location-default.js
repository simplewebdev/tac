import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import Grid from '@material-ui/core/Grid';
import AirCTA from '../components/views/air-cta';
import RequestModal from '../components/views/fbo-service-request';
import ContactCTA from '../components/views/contact-air-modal';
import ImageGallery from 'react-image-gallery';
import Head from 'next/head';
import moment from 'moment';
import Post from "../utils/post";

export default class LocationDefault extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject(query.pagename, query.status, query.revision);
      page = pageResponse.object;


    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { page }
  }

  constructor(props) {
    super(props);

    this.state = {
      page: props.page,
      images: props.images,
      cta: {},
      name: "",
      email: "",
      phone: "",
      message: "",
      company: "",
      title: "",
      tailNumber: "",
      selectFBO: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
      showFullscreenButton: true,
      showGalleryFullscreenButton: true,
      showPlayButton: true,
      showGalleryPlayButton: true,
      showVideo: {},
      isFBO: props.page.type_slug === 'air-locations',
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalDepDateTime: "",
      modalArrDateTime: "",
      modalTailNumber: "",
      modalFBO: "",
      commentsModalOpen: false,
    }
  }

  _resetVideo() {
    this.setState({ showVideo: {} });

    if (this.state.showPlayButton) {
      this.setState({ showGalleryPlayButton: true });
    }

    if (this.state.showFullscreenButton) {
      this.setState({ showGalleryFullscreenButton: true });
    }
  }

  _toggleShowVideo(url) {
    this.state.showVideo[url] = !Boolean(this.state.showVideo[url]);
    this.setState({
      showVideo: this.state.showVideo
    });

    if (this.state.showVideo[url]) {
      if (this.state.showPlayButton) {
        this.setState({ showGalleryPlayButton: false });
      }

      if (this.state.showFullscreenButton) {
        this.setState({ showGalleryFullscreenButton: false });
      }
    }
  }

  _renderVideo(item) {
    return (
      <div className='image-gallery-image'>
        {
          this.state.showVideo[item.embedUrl] ?
            <div className='video-wrapper'>
              <a
                className='close-video'
                onClick={this._toggleShowVideo.bind(this, item.embedUrl)}
              >
              </a>
              <iframe
                id="gallery-frame"
                width='561'
                height='315'
                src={item.embedUrl}
                frameBorder='0'
                allowFullScreen
              >
              </iframe>
            </div>
            :
            <a onClick={this._toggleShowVideo.bind(this, item.embedUrl)}>
              <div className='play-button' />
              <img src={item.original} />
              {
                item.description &&
                <span
                  className='image-gallery-description'
                  style={{ right: '0', left: 'initial' }}
                >
                  {item.description}
                </span>
              }
            </a>
        }
      </div>
    );
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
      company: "",
      title: "",
      tailNumber: "",
      selectFBO: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/tac-air-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
        company: this.state.company,
        title: this.state.title,
        tail_number: this.state.tailNumber,
        fbo: this.state.selectFBO,
      })
    }).then((res) => {
      if (res.status === 200) {
        let fboVal = this.state.selectFBO;
        let fboRec;
        if (fboVal == 'dal') {
          fboRec = "Dist_WebComments_Kdal@tacenergy.com"
        } else {
          if (this.state.page.metadata.location.email) {
            fboRec = this.state.page.metadata.location.email;
          } else {
            fboRec = "marketing@tacenergy.com";
          }
        }
        let title = 'TAC Air Contact - FBO ' + this.state.modalFBO;
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>`
          + `<b>Company: ${this.state.company}</b><br>`
          + `<b>Title: ${this.state.title}</b><br>`
          + `<b>Tail Number: ${this.state.tailNumber}</b><br>`
          + `<b>FBO: ${this.state.selectFBO}</b><br>`
          + `<b>SENT TO: ${fboRec}</b><br>` +
          `</div>`;
        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePhoneChange = (event) => {
    this.setState({ phone: event.target.value });
  };

  handleMessageChange = (event) => {
    this.setState({ message: event.target.value });
  };

  handleCompanyChange = (event) => {
    this.setState({ company: event.target.value });
  };

  handleTitleChange = (event) => {
    this.setState({ title: event.target.value });
  };

  handleTailNumberChange = (event) => {
    this.setState({ tailNumber: event.target.value });
  };

  handleFBOChange = (event) => {
    this.setState({ selectFBO: event.target.value });
  };

  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalDepDateTime: "",
      modalArrDateTime: "",
      modalTailNumber: "",
      modalFBO: "",
    })
  };

  handleModalSubmit = (event) => {
    this.setState({ modalSubmitting: true });
    event.preventDefault();

    fetch('/api/fbo-service-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.modalName,
        email: this.state.modalEmail,
        phone: this.state.modalPhone,
        message: this.state.modalComments,
        departure_date_time: moment(this.state.modalDepDateTime).format('MM/DD/YYYY h:mm a'),
        arrival_date_time: moment(this.state.modalArrDateTime).format('MM/DD/YYYY h:mm a'),
        tail_number: this.state.modalTailNumber,
        fbo: this.state.modalFBO
      })
    }).then((res) => {
      if (res.status === 200) {
        let fboVal = this.state.modalFBO;
        let fboRec;
        if (fboVal == 'dal') {
          fboRec = "DIST_FBO_DAL@tacenergy.com"
        } else {
          if (this.state.page.metadata.location.email) {
            fboRec = this.state.page.metadata.location.email;
          } else {
            fboRec = "marketing@tacenergy.com";
          }
        }
        let title = 'FBO Service Request ' + this.state.modalFBO;
        let html =
          `<div>`
          + `<b>New FBO service request from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Arrival Date/Time: ${moment(this.state.modalDepDateTime).format('MM/DD/YYYY h:mm a')}</b><br>`
          + `<b>Departure Date/Time: ${moment(this.state.modalArrDateTime).format('MM/DD/YYYY h:mm a')}</b><br>`
          + `<b>Tail Number: ${this.state.modalTailNumber}</b><br>`
          + `<b>FBO: ${this.state.modalFBO}</b><br>`
          + `<b>SENT TO: ${fboRec}</b><br>` +
          `</div>`;
        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({ modalSubmitted: true });
      } else {
        this.setState({ modalSubmitted: false, modalSubmitting: false, modalFailedSubmit: true });
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({ modalName: event.target.value });
  };

  handleModalPhoneChange = (event) => {
    this.setState({ modalPhone: event.target.value });
  };

  handleModalEmailChange = (event) => {
    this.setState({ modalEmail: event.target.value });
  };

  handleModalCommentsChange = (event) => {
    this.setState({ modalComments: event.target.value });
  };

  handleModalDepDateTimeChange = event => {
    this.setState({ modalDepDateTime: event.target.value });
  };

  handleModalArrDateTimeChange = event => {
    this.setState({ modalArrDateTime: event.target.value });
  };

  handleModalTailNumberChange = event => {
    this.setState({ modalTailNumber: event.target.value });
  };

  handleModalFBOChange = event => {
    this.setState({ modalFBO: event.target.value });
  };

  handleOnClickArrow = () => {
    this.setState({ modalOpen: true });
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false
    });
    this.setState({ modalOpen: !this.state.modalOpen });
  };

  handleOnClickArrowComments = () => {
    this.setState({ commentsModalOpen: true });
  };

  handleCommentsModalClose = () => {
    this.resetModalForm();
    this.setState({
      submitted: false,
      submitting: false,
      failedSubmit: false
    });
    this.setState({ commentsModalOpen: !this.state.commentsModalOpen });
  };

  render() {
    let bgColor = 'white';
    let cardColor =
      bgColor === 'blue' ? 'rgba(0, 83, 160, 0.96)' :
        bgColor === 'red' ? 'rgba(226, 56, 63, 0.96)' : 'rgba(255, 255, 255, 0.96)';

    const media = [];

    this.state.page.metadata.photo_gallery.video && media.push({
      original: `${this.state.page.metadata.photo_gallery.photos[0].image.url}?w=2000`,
      thumbnail: `${this.state.page.metadata.photo_gallery.photos[0].image.url}?w=2000`,
      embedUrl: this.state.page.metadata.photo_gallery.video,
      renderItem: this._renderVideo.bind(this)
    });


    if (this.state.page.metadata.photo_gallery.photos && Array.isArray(this.state.page.metadata.photo_gallery.photos) || this.state.page.metadata.photo_gallery.photos.length) {
      this.state.page.metadata.photo_gallery.photos.map((photo, index) => {
        if (this.state.page.metadata.photo_gallery.video && index !== 0) {
          media.push({
            original: `${photo.image.url}?w=2000`,
            thumbnail: `${photo.image.url}?w=2000`,
          })
        } else if (!this.state.page.metadata.photo_gallery.video) {
          var isImage = /([/|.|\w|\s|-])*\.(?:jpg|jpeg|JPG|JPEG|gif|GIF|png|PNG)/.test(photo.image);
          if (!isImage) {
            media.push({
              original: `${photo.image.url}?w=2000`,
              thumbnail: `${photo.image.url}?w=2000`,
            })
          }
        }

      })
    }
    const cleanText = val => val.replace(/<\/?[^>]+(>|$)/g, '');

    return (
      this.props.page.component && this.props.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'air'}>
            <Head>
              <title>{`${this.state.page.title}`}</title>
            </Head>
            <div className="fbo-hero" style={{ width: '100%', position: 'relative' }}>
              <div
                className="hero-img"
                style={{
                  background: this.state.page.metadata.hero_image ? this.state.page.metadata.hero_image.url !== 'https://s3-us-west-2.amazonaws.com/cosmicjs/' ?
                    `url(${this.state.page.metadata.hero_image.imgix_url}?w=2000)` :
                    `url(https://cosmic-s3.imgix.net/98b52680-9bfd-11e8-8928-b51c5e4c8149-Camron_Johnson_LineTech_TYS.jpg)` :
                    `url(https://cosmic-s3.imgix.net/98b52680-9bfd-11e8-8928-b51c5e4c8149-Camron_Johnson_LineTech_TYS.jpg)`,
                  zIndex: 1
                }}
              />
            </div>
            <section style={{ marginTop: 50 }} className="container">
              <div style={{
                height: '100%',
                WebkitBoxShadow: '-18px 19px 32px 5px rgba(0,0,0,0.05)',
                MozBoxShadow: '-18px 19px 32px 5px rgba(0,0,0,0.05)',
                boxShadow: '-18px 19px 32px 5px rgba(0,0,0,0.05)',
              }}>
                <div style={{ padding: 60, height: '100%', display: 'flex', flexDirection: 'column', alignItems: 'baseline', justifyContent: 'center' }}>
                  <h2 style={{ textTransform: 'uppercase', color: '#4c4b4b', margin: 0, padding: '15px 5px' }}>{this.state.page.title}</h2>
                  <div style={{ padding: '5px 5px', fontSize: 14, color: '#626262' }}>
                    <div dangerouslySetInnerHTML={{ __html: this.state.page.content }} />
                  </div>
                </div>
              </div>
              <Grid container spacing={40}>
                <Grid item xs={12} sm={6} style={{ padding: '80px 20px' }}>
                  <div style={{
                    height: '100%',
                    WebkitBoxShadow: '-18px 19px 32px 5px rgba(0,0,0,0.05)',
                    MozBoxShadow: '-18px 19px 32px 5px rgba(0,0,0,0.05)',
                    boxShadow: '-18px 19px 32px 5px rgba(0,0,0,0.05)',
                  }}>
                    <div style={{
                      padding: '10px 0px 10px 25px',
                      height: '100%',
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'baseline',
                      justifyContent: 'center',
                      background: 'rgb(226, 56, 63)'
                    }}>
                      <div className='amenities'>
                        <h2 style={{
                          display: 'inline-block',
                          textTransform: 'uppercase',
                          color: 'white',
                          margin: 0,
                          paddingTop: '15px',
                          letterSpacing: '1.2px'
                        }}>Amenities</h2>
                        <div className="amenities-img" style={{ display: 'inline-block', float: 'right' }}>
                          <img
                            style={{ width: 130, height: 130, objectFit: 'cover', objectPosition: '50% 25%' }}
                            src="../static/images/ama.png" />
                          {this.state.page.title == 'TAC AIR-DAL' ? <img className="is-bahs"
                            style={{ width: 130, height: 130, objectFit: 'cover', objectPosition: 'center' }}
                            src="../static/images/ama_two_old.png" /> : <img
                              style={{ width: 130, height: 130, objectFit: 'cover', objectPosition: '50% 25%' }}
                              src="../static/images/ama_two.png" />}

                        </div>
                      </div>
                      <div style={{ fontSize: 12, color: 'white' }}>
                        {this.state.page.metadata.amenities &&
                          <div dangerouslySetInnerHTML={{ __html: this.state.page.metadata.amenities }} />}
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={12} sm={6} style={{ padding: '80px 20px' }}>
                  <div style={{
                    height: '100%',
                    WebkitBoxShadow: '-18px 19px 32px 5px rgba(0,0,0,0.05)',
                    MozBoxShadow: '-18px 19px 32px 5px rgba(0,0,0,0.05)',
                    boxShadow: '-18px 19px 32px 5px rgba(0,0,0,0.05)',
                  }}>
                    <Grid container style={{
                      padding: '10px 25px 10px 40px',
                      height: '100%',
                      display: 'flex',
                      justifyContent: 'center',
                      background: '#0b559c'
                    }}>
                      <Grid item xs={12} sm={12} md={6}>
                        <h2 style={{
                          textTransform: 'uppercase',
                          color: 'white',
                          margin: 0,
                          paddingTop: '15px',
                          letterSpacing: '1.2px'
                        }}>Contact</h2>
                        <div style={{ fontSize: 12, color: 'white', marginTop: 40 }}>
                          <a className="location-contact-address" href={`https://maps.google.com/?q=${cleanText(this.state.page.metadata.location.address)}`} target="_blank">
                            <p>{this.state.page.metadata.location.airport}</p>
                            <div dangerouslySetInnerHTML={{ __html: this.state.page.metadata.location.address }} />
                          </a>
                          <p>
                            <a className="location-contact-phone" href={`tel:+1${this.state.page.metadata.location.phone_number}`}>
                              Phone: {this.state.page.metadata.location.phone_number}
                            </a>
                          </p>
                        </div>
                        <div style={{ fontSize: 12, color: 'white', marginTop: 20 }}>
                          {
                            this.state.page.metadata.location.asri &&
                            <p>{this.state.page.metadata.location.asri} - ASRI</p>
                          }
                          {
                            this.state.page.metadata.location.unicom &&
                            <p>{this.state.page.metadata.location.unicom} - UNICOM</p>
                          }
                          {
                            this.state.page.metadata.location.arinc &&
                            <p>{this.state.page.metadata.location.arinc} - ARINC</p>
                          }
                        </div>
                      </Grid>
                      <Grid style={{ marginTop: 15 }} item xs={12} sm={12} md={6}>
                        <a className="float-right button button-air-white"
                          style={{ fontWeight: '500', color: 'black', background: 'white' }}
                          href={`${`tel:+1${this.state.page.metadata.location.phone_number}`}`}>{this.state.page.metadata.location.phone_number}</a>
                        <a className="float-right button" style={{ fontWeight: '500', color: 'white', background: 'grey' }}
                          onClick={this.handleOnClickArrow.bind(this)}>{this.state.page.slug !== 'dal' ? 'SERVICE REQUEST' : 'CONTACT'}</a>
                        <a className="float-right button" style={{ fontWeight: '500' }} onClick={this.handleOnClickArrowComments.bind(this)}>SEND
                        COMMENTS</a>
                        {/*<a className="float-right button" style={{fontWeight: '100', color: 'black', background: 'white'}} target="_blank" href="https://s3-us-west-2.amazonaws.com/cosmicjs/1b542e10-a199-11e8-b671-4969be7a2b02-ValuedTACAirCustomer.pdf">TSA COMPLIANCE</a>*/}
                      </Grid>
                    </Grid>
                  </div>
                </Grid>
              </Grid>

            </section>
            <section className="container">
              {
                this.state.page.metadata.photo_gallery.photos[0] &&
                <ImageGallery
                  items={media}
                  showThumbnails={false}
                  showBullets={true}
                  showPlayButton={false}
                  showNav={true}
                  lazyLoad={true} />
              }
            </section>
            <section id="contact-fbo" style={{ clear: 'both' }}>
              <AirCTA
                phone={this.state.page.metadata.location.phone_number}
                handleOnClickArrow={this.handleOnClickArrow.bind(this)}
                handleOnClickArrowComments={this.handleOnClickArrowComments.bind(this)}
              />
              <section>
                <RequestModal
                  modalOpen={this.state.modalOpen}
                  onModalClose={this.handleModalClose.bind(this)}
                  emailValue={this.state.modalEmail}
                  handleSubmit={this.handleModalSubmit.bind(this)}
                  onEmailChange={this.handleModalEmailChange.bind(this)}
                  NameValue={this.state.modalName}
                  onNameChange={this.handleModalNameChange.bind(this)}
                  phoneValue={this.state.modalPhone}
                  onPhoneChange={this.handleModalPhoneChange.bind(this)}
                  commentsValue={this.state.modalComments}
                  onCommentsChange={this.handleModalCommentsChange.bind(this)}
                  depDateValue={this.state.modalDepDateTime}
                  onDepDateChange={this.handleModalDepDateTimeChange.bind(this)}
                  arrDateValue={this.state.modalArrDateTime}
                  onArrDateChange={this.handleModalArrDateTimeChange.bind(this)}
                  tailNumberValue={this.state.modalTailNumber}
                  onTailNumberChange={this.handleModalTailNumberChange.bind(this)}
                  fboValue={this.state.modalFBO}
                  onFboChange={this.handleModalFBOChange.bind(this)}
                  submitted={this.state.modalSubmitted}
                  submitting={this.state.modalSubmitting}
                  failedToSumbit={this.state.modalFailedSubmit}
                />
                <ContactCTA
                  modalOpen={this.state.commentsModalOpen}
                  onModalClose={this.handleCommentsModalClose.bind(this)}
                  emailValue={this.state.email}
                  handleSubmit={this.handleSubmit.bind(this)}
                  onEmailChange={this.handleEmailChange.bind(this)}
                  nameValue={this.state.name}
                  onNameChange={this.handleNameChange.bind(this)}
                  phoneValue={this.state.phone}
                  onPhoneChange={this.handlePhoneChange.bind(this)}
                  messageValue={this.state.message}
                  onMessageChange={this.handleMessageChange.bind(this)}
                  companyValue={this.state.company}
                  onCompanyChange={this.handleCompanyChange.bind(this)}
                  titleValue={this.state.title}
                  onTitleChange={this.handleTitleChange.bind(this)}
                  tailNumberValue={this.state.tailNumber}
                  onTailNumberChange={this.handleTailNumberChange.bind(this)}
                  fboValue={this.state.selectFBO}
                  onFBOChange={this.handleFBOChange.bind(this)}
                  submitted={this.state.submitted}
                  submitting={this.state.submitting}
                  failedToSumbit={this.state.failedSubmit}
                />
              </section>
            </section>
          </article>
        )
    );
  }
}