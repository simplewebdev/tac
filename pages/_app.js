import React from 'react';
import Request from "../utils/request";
import { mapGlobals } from "../utils/helper";
import FooterTop from "../components/shared/footer/footer-top";
import Footer from "../components/shared/footer";
import Header from '../components/shared/header';
import App, { Container } from 'next/app';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import JssProvider from 'react-jss/lib/JssProvider';
import getPageContext from '../utils/getPageContext';
import 'whatwg-fetch';
import Router from 'next/router';

class Layout extends React.Component {

  render() {
    const { children, logo, favicon, logoSize, company } = this.props;

    return (
      <div className='main'>
        <Header logo={logo} logoHeight={logoSize} favicon={favicon} company={company} />
        {children}
        <FooterTop />
        <Footer name="dddddddd" />
      </div>
    )
  }
}

export default class TACApp extends App {
  constructor(props) {
    super(props);
    this.pageContext = getPageContext();
    this.state = {
      globals: {},
      headerLogo: '',
      logoSize: '',
      favicon: '',
      company: ''
    };
  }

  pageContext = null;

  async componentDidMount() {

    if (/cosmicapp/.test(window.location.href)) {
      Router.push("https://thearnoldcos.com/");
    }

    let globals;
    try {
      const Response = await Request.getGlobals();
      globals = mapGlobals(Response.objects);

      if (/tac-energy/.test(window.location.href)) {
        this.setState({
          headerLogo: "https://cosmic-s3.imgix.net/a81a7870-86c0-11e8-93b4-d5cc057540a7-energy-logo.svg?w=250",
          logoSize: "45px",
          company: 'tac-energy'
        })
      } else if (/tac-air/.test(window.location.href)) {
        this.setState({
          headerLogo: "https://cosmic-s3.imgix.net/acfa5590-86c0-11e8-bad9-277f9bdb485c-air-logo.svg?w=250",
          logoSize: "40px",
          company: 'tac-air'
        })
      } else if (/tac-investments/.test(window.location.href)) {
        this.setState({
          headerLogo: "https://cosmic-s3.imgix.net/c44690e0-86bd-11e8-b688-19eb7178b919-investments-logo.svg?w=250",
          logoSize: "35px",
          company: 'tac-investments'
        })
      } else if (/IPCnowTACenergy/.test(window.location.href)) {
        this.setState({
          headerLogo: "https://cosmic-s3.imgix.net/a81a7870-86c0-11e8-93b4-d5cc057540a7-energy-logo.svg?w=250",
          logoSize: "45px",
          company: 'tac-energy'
        })
      } else if (/keystone-aviation/.test(window.location.href)) {
        this.setState({
          headerLogo: "https://cosmic-s3.imgix.net/93a67b80-758d-11e8-9c42-75c5f9571e6d-Keystone%20Aviation.png?w=250",
          logoSize: "65px",
          company: 'keystone-aviation'
        })
      }else {
        this.setState({
          headerLogo: 'https://cosmic-s3.imgix.net/30210740-966e-11e8-ab08-31f1ed4cd97d-tac-full-logo.svg?w=250',
          logoSize: "50px",
          company: ''
        })
      }

    } catch (e) {
      console.log("error loading globals from bucket");
    }

    this.setState({
      globals: globals,
      favicon: globals.header.metadata.favicon.url
    });

    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }


  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        {/* Wrap every page in Jss and Theme providers */}
        <JssProvider
          registry={this.pageContext.sheetsRegistry}
          generateClassName={this.pageContext.generateClassName}
        >
          {/* MuiThemeProvider makes the theme available down the React
              tree thanks to React context. */}
          <MuiThemeProvider
            theme={this.pageContext.theme}
            sheetsManager={this.pageContext.sheetsManager}
          >
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            <Layout logo={this.state.headerLogo} logoSize={this.state.logoSize} favicon={this.state.favicon} company={this.state.company}>
              <Component pageContext={this.pageContext} {...pageProps} />
            </Layout>
          </MuiThemeProvider>
        </JssProvider>
      </Container>
    )
  }
}