import React from 'react'
import Page from '../components/views/page'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import Grid from '@material-ui/core/Grid';
import Head from 'next/head';

export default class AircraftForSalePage extends React.Component {

  static async getInitialProps() {
    let aircrafts;
    let page;

    try {
      const request = await Request.getAircraftsForSale(null, null);
      aircrafts = request.objects;
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { aircrafts }
  }

  constructor(props){
    super(props);

    this.state = {
      aircrafts: props.aircrafts,
    }
  }

  render() {
    return (
      <div style={{padding: '60px 0px'}} className="container">
        <Head>
          <title>{`Keystone Aviation - Aircraft For Sale`}</title>
        </Head>
        <h2 style={{fontWeight: 500}}>AIRCRAFT FOR SALE</h2>
        <Grid container spacing={8}>
          {!!this.state.aircrafts && this.state.aircrafts.map((aircraft, index) =>
            <Grid key={index} item xs={12} sm={6} md={3}>
              <article style={{maxWidth: 400, padding: 10}}>
                <a href={`/keystone-aviation/${aircraft.slug}`}>
                  <div className="card">
                    <img width="100%" src={`${aircraft.metadata.photo_gallery.featured_image.imgix_url}?w=500`} style={{minHeight: 225}}/>
                    <h2 className="aircraft-title">{aircraft.title}</h2>
                  </div>
                </a>
              </article>
            </Grid>
          )}
        </Grid>
      </div>
    );
  }
}