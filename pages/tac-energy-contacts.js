import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import Grid from '@material-ui/core/Grid';
import { mapPage } from "../utils/helper";
import Head from 'next/head';
import CTA from '../components/views/cta';
import RequestQuote from '../components/views/request-quote-modal';
import Router from 'next/router';

export default class EnergyContactsPage extends React.Component {

  static async getInitialProps({req, query}) {
    let page;
    let contacts;
    let pageTitle;

    try {
      contacts = await Request.getTACEnergyContacts();
      const pageResponse = await Request.getObject('contacts', query.status, query.revision);
      page = mapPage(pageResponse.object);
      pageTitle = pageResponse.object.title;
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { page, contacts, pageTitle }
  }

  constructor(props){
    super(props);
    this.requestRef = React.createRef();
    this.state = {
      page: props.page,
      pageTitle: props.pageTitle,
      contacts: props.contacts,
      cta: props.page.cta,
      firstName: "",
      lastName: "",
      company: "",
      email: "",
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalCaptcha: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    };
    this.handleOnClickArrow = this.handleOnClickArrow.bind(this);
  }

  componentDidMount() {
    if (/request-a-quote/.test(window.location.href)) {
      this.setState({modalOpen: true});
    }
  }

  resetForm = () => {
    this.setState({
      firstName: "",
      lastName: "",
      company: "",
      email: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({submitting: true});
    event.preventDefault();

    fetch('/api/cc-market-talk-subscribe', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "lists": [
          {
            "id": "881"
          }
        ],
        "company_name": this.state.company,
        "confirmed": false,
        "email_addresses": [
          {
            "email_address": this.state.email
          }
        ],
        "first_name": this.state.firstName,
        "last_name": this.state.lastName
      })
    }).then((res)=>{
      if (res.status === 200){
        this.resetForm();
        this.setState({submitted: true});
      }else {
        this.setState({submitted: false, submitting: false, failedSumbit: true});
      }
    })
  };

  handleFirstNameChange = (event) => {
    this.setState({firstName: event.target.value});
  };

  handleLastNameChange = (event) => {
    this.setState({lastName: event.target.value});
  };

  handleEmailChange = (event) => {
    this.setState({email: event.target.value});
  };

  handleCompanyChange = (event) => {
    this.setState({company: event.target.value});
  };

  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalPhone: "",
      modalComments: "",
    })
  };

  handleModalSubmit = (event) => {
    this.setState({modalSubmitting: true});
    event.preventDefault();

    fetch('/api/energy-quote-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.modalEmail,
        name: this.state.modalName,
        phone: this.state.modalPhone,
        company: this.state.modalCompany,
        message: this.state.modalComments
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'Energy Quote Request - Main Page';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Company: ${this.state.modalCompany}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({modalSubmitted: true});
      } else {
        this.setState({modalSubmitted: false, modalSubmitting: false, modalFailedSumbit: true});
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({modalName: event.target.value});
  };

  handleModalPhoneChange = (event) => {
    this.setState({modalPhone: event.target.value});
  };

  handleModalEmailChange = (event) => {
    this.setState({modalEmail: event.target.value});
  };

  handleModalCompanyChange = (event) => {
    this.setState({modalCompany: event.target.value});
  };

  handleModalCommentsChange = (event) => {
    this.setState({modalComments: event.target.value});
  };

  handleModalCaptchaChange = (value) => {
    this.setState({modalCaptcha: value});
  };

  handleOnClickArrow() {
    this.setState({modalOpen: true});
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false,
      modalOpen: !this.state.modalOpen
    });
    Router.push('/tac-energy');
  };

  render() {
    let contacts = this.state.contacts;

    return (
      this.props.page.component && this.props.page.component === '404' ? (
        <PageNotFound />
      ) : (
        <article className={'energy'}>
          <Head>
            <title>TAC Energy - Contacts</title>
          </Head>
          <section className="container location">
            <h1>{this.state.pageTitle}</h1>
          </section>
          <section className='container' style={{paddingBottom: '75px'}}>
            <Grid container spacing={8}>
            {
              !!contacts && contacts.objects.map((value, index) =>
                <Grid key={index} item xs={12} sm={6} md={4} style={{padding: 20}}>
                  <div className="location-card contacts">
                    <h3>{value.title}</h3>
                    <div className="location-card-list contacts">
                      <div className="location-card-list-item contacts">
                        <div className="location-card-icon">
                          <img src="https://cosmic-s3.imgix.net/6f306e80-e4a2-11e8-bbfb-47935b0f92ad-ic_user.svg" height="25px"/>
                        </div>
                        <div><a className="location-card-link" href={`mailto:${value.metadata.email}`}>{value.metadata.name}</a></div>
                      </div>
                      <div className="location-card-list-item contacts">
                        <div className="location-card-icon">
                          <img src="https://cosmic-s3.imgix.net/e6652cc0-891c-11e8-96d5-43db6338ad71-phone-call.svg" height="25px"/>
                        </div>
                        <div>
                          <a style={{color: '#495E6E'}} href={`tel:${value.metadata.phone}`}>{value.metadata.phone}</a>
                        </div>
                      </div>
                      <div className="location-card-list-item contacts">
                        <div className="location-card-icon">
                          <img src="https://cosmic-s3.imgix.net/6c471ca0-e362-11e8-8955-7f53de970a57-ic_mail.svg" height="20px"/>
                        </div>
                        <div><a className="location-card-link" href={`mailto:${value.metadata.email}`}>Email</a></div>
                      </div>
                    </div>
                  </div>
                </Grid>
              )
            }
            </Grid>
          </section>
          <section id="market-talk-updates">
            <CTA
              cta={this.state.cta}
              emailValue={this.state.email}
              handleSubscribe={this.handleSubmit.bind(this)}
              onEmailChange={this.handleEmailChange.bind(this)}
              firstNameValue={this.state.firstName}
              onFirstNameChange={this.handleFirstNameChange.bind(this)}
              lastNameValue={this.state.lastName}
              onLastNameChange={this.handleLastNameChange.bind(this)}
              companyValue={this.state.company}
              onCompanyChange={this.handleCompanyChange.bind(this)}
              logoHeight='50px'
              bgColor='#8b8b8b'
              btnColor='#0b559c'
              submitted={this.state.submitted}
              submitting={this.state.submitting}
              failedToSumbit={this.state.failedSubmit}
            />
          </section>
          <RequestQuote
            modalOpen={this.state.modalOpen}
            onModalClose={this.handleModalClose.bind(this)}
            emailValue={this.state.modalEmail}
            handleSubmit={this.handleModalSubmit.bind(this)}
            onEmailChange={this.handleModalEmailChange.bind(this)}
            NameValue={this.state.modalName}
            onNameChange={this.handleModalNameChange.bind(this)}
            phoneValue={this.state.modalPhone}
            onPhoneChange={this.handleModalPhoneChange.bind(this)}
            companyValue={this.state.modalCompany}
            onCompanyChange={this.handleModalCompanyChange.bind(this)}
            commentsValue={this.state.modalComments}
            onCommentsChange={this.handleModalCommentsChange.bind(this)}
            submitted={this.state.modalSubmitted}
            submitting={this.state.modalSubmitting}
            failedToSumbit={this.state.modalFailedSubmit}
            onCaptchaChange={this.handleModalCaptchaChange.bind(this)}
          />
        </article>
      )
    );
  }
}