import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import { mapPage } from "../utils/helper";
import Hero from '../components/views/main-page-hero';
import SectionHead from '../components/views/section-head';
import SectionBlurb from '../components/views/section-blurb';
import Head from 'next/head';
import Post from '../utils/post';

export default class TACInvestPage extends React.Component {

  static async getInitialProps({req, query}) {
    let page;

    try {
      const pageResponse = await Request.getObject('tac-investments', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }
    return { page }
  }

  constructor(props){
    super(props);
    this.state = {
      page: props.page,
      hero: props.page.hero_banner,
      cta: props.page.cta,
      about: props.page.about,
      blurb: props.page.blurb,
      name: "",
      email: "",
      phone: "",
      message: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    }
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({submitting: true});
    event.preventDefault();

    fetch('/api/tac-investments-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
      })
    }).then((res)=>{
      if (res.status === 200){
        let title = 'TAC Contact Form - Investments';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({submitted: true});
      }else {
        this.setState({submitted: false, submitting: false, failedSumbit: true});
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({name: event.target.value});
  };

  handleEmailChange = (event) => {
    this.setState({email: event.target.value});
  };

  handlePhoneChange = (event) => {
    this.setState({phone: event.target.value});
  };

  handleMessageChange = (event) => {
    this.setState({message: event.target.value});
  };

  render() {
    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
        <article className={'investments'}>
          <Head>
            <title>{`TAC Investments`}</title>
          </Head>
          <Hero hero={this.state.hero}/>
          <SectionHead bgColor='blue' sectionHead={this.state.about.children} />
          <SectionBlurb logoHeight="30px" sectionBlurb={this.state.blurb.children}/>
          <div style={{backgroundColor: '#0b559c', display: 'flex', alignItems: 'center', flexDirection: 'column', minHeight: '5vh'}}/>
        </article>
      )
    );
  }
}