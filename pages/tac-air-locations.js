import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import { mapAirLocations } from "../utils/helper";
import Grid from '@material-ui/core/Grid';
import SVG from 'react-inlinesvg';
import Head from 'next/head';
import AirCTA from '../components/views/air-cta';
import RequestModal from '../components/views/fbo-service-request';
import ContactCTA from '../components/views/contact-air-modal';
import moment from "moment/moment";
import Post from '../utils/post';


export default class AirLocationsPage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;
    let locations;
    try {
      const Response = await Request.getAirLocations();
      locations = mapAirLocations(Response.objects);
      const pageResponse = await Request.getObject('locations', query.status, query.revision);
      page = pageResponse.object;
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { page, locations }
  }

  constructor(props) {
    super(props);

    this.state = {
      page: props.page,
      locations: props.locations,
      cta: props.page.metafields[0] && props.page.metafields[0],
      name: "",
      email: "",
      phone: "",
      message: "",
      company: "",
      title: "",
      tailNumber: "",
      selectFBO: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
      modalDepDateTime: "",
      modalArrDateTime: "",
      modalTailNumber: "",
      modalFBO: "",
      commentsModalOpen: false,
    }
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
      company: "",
      title: "",
      tailNumber: "",
      selectFBO: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/tac-air-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
        company: this.state.company,
        title: this.state.title,
        tail_number: this.state.tailNumber,
        fbo: this.state.selectFBO,
      })
    }).then((res) => {
      if (res.status === 200) {
        let fboVal = this.state.selectFBO;
        let fboRec;
        switch (fboVal) {
          case 'ama':
            fboRec = "Dist_WebComments_AMA@tacair.com";
            break;
          case 'apa':
            fboRec = "Dist_WebComments_APA@tacair.com";
            break;
          case 'bdl':
            fboRec = "Dist_WebComments_BDL@tacair.com";
            break;
          case 'dal':
            //fboRec = "jgibney@tacair.com";
            fboRec = "Dist_WebComments_Kdal@tacenergy.com";
            break;
          case 'fsm':
            fboRec = "Dist_WebComments_FSM@tacair.com";
            break;
          case 'lex':
            fboRec = "Dist_WebComments_LEX@tacair.com";
            break;
          case 'lit':
            fboRec = "Dist_WebComments_LIT@tacair.com";
            break;
          case 'oma':
            fboRec = "Dist_WebComments_OMA@tacair.com";
            break;
          case 'pvu':
            fboRec = "Dist_WebComments_PVU@tacair.com";
            break;
          case 'rdu':
            fboRec = "Dist_WebComments_RDU@tacair.com";
            break;
          case 'shv':
            fboRec = "Dist_WebComments_SHV@tacair.com";
            break;
          case 'slc':
            fboRec = "Dist_WebComments_SLC@tacair.com";
            break;
          case 'sus':
            fboRec = "Dist_WebComments_SUS@tacair.com";
            break;
          case 'txk':
            fboRec = "Dist_WebComments_TXK@tacair.com";
            break;
          case 'tys':
            fboRec = "Dist_WebComments_TYS@tacair.com";
            break;
          default:
            fboRec = "marketing@tacenergy.com";

        }

        let title = 'Send Comments Form - TAC Air - Locations';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>`
          + `<b>Company: ${this.state.company}</b><br>`
          + `<b>Title: ${this.state.title}</b><br>`
          + `<b>Tail Number: ${this.state.tailNumber}</b><br>`
          + `<b>FBO: ${this.state.selectFBO}</b><br>`
          + `<b>Sent to: ${fboRec}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePhoneChange = (event) => {
    this.setState({ phone: event.target.value });
  };

  handleMessageChange = (event) => {
    this.setState({ message: event.target.value });
  };

  handleCompanyChange = (event) => {
    this.setState({ company: event.target.value });
  };

  handleTitleChange = (event) => {
    this.setState({ title: event.target.value });
  };

  handleTailNumberChange = (event) => {
    this.setState({ tailNumber: event.target.value });
  };

  handleFBOChange = (event) => {
    this.setState({ selectFBO: event.target.value });
  };

  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalPhone: "",
      modalComments: "",
      modalDepDateTime: "",
      modalArrDateTime: "",
      modalTailNumber: "",
      modalFBO: "",
    })
  };

  handleModalSubmit = () => {
    this.setState({ modalSubmitting: true });

    fetch('/api/fbo-service-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.modalName,
        email: this.state.modalEmail,
        phone: this.state.modalPhone,
        message: this.state.modalComments,
        departure_date_time: moment(this.state.modalDepDateTime).format('MM/DD/YYYY h:mm a'),
        arrival_date_time: moment(this.state.modalArrDateTime).format('MM/DD/YYYY h:mm a'),
        tail_number: this.state.modalTailNumber,
        fbo: this.state.modalFBO,
      })
    }).then((res) => {

      if (res.status === 200) {
        let fboVal = this.state.modalFBO;
        let fboRec;
        switch (fboVal) {
          case 'ama':
            fboRec = 'amafbo@tacair.com';
            break;
          case 'apa':
            fboRec = 'apafbo@tacair.com';
            break;
          case 'bdl':
            fboRec = 'bdlfbo@tacair.com';
            break;
          case 'dal':
            //fboRec = 'jgibney@tacair.com';
            fboRec = 'DIST_FBO_DAL@tacenergy.com';
            break;
          case 'fsm':
            fboRec = 'fsmfbo@tacair.com';
            break;
          case 'lex':
            fboRec = 'lexfbo@tacair.com';
            break;
          case 'lit':
            fboRec = 'litfbo@tacair.com';
            break;
          case 'oma':
            fboRec = 'omafbo@tacair.com';
            break;
          case 'pvu':
            fboRec = 'pvufbo@tacair.com';
            break;
          case 'rdu':
            fboRec = 'rdufbo@tacair.com';
            break;
          case 'shv':
            fboRec = 'shvfbo@tacair.com';
            break;
          case 'slc':
            fboRec = 'slcfbo@tacair.com';
            break;
          case 'sus':
            fboRec = 'susfbo@tacair.com';
            break;
          case 'txk':
            fboRec = 'txkfbo@tacair.com';
            break;
          case 'tys':
            fboRec = 'tysfbo@tacair.com';
            break;
          default:
            fboRec = "marketing@tacenergy.com";

        }
        let title = 'FBO Service Request - ' + this.state.modalFBO;
        let html =
          `<div>`
          + `<b>New FBO service request from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Arrival Date/Time: ${moment(this.state.modalDepDateTime).format('MM/DD/YYYY h:mm a')}</b><br>`
          + `<b>Departure Date/Time: ${moment(this.state.modalArrDateTime).format('MM/DD/YYYY h:mm a')}</b><br>`
          + `<b>Tail Number: ${this.state.modalTailNumber}</b><br>`
          + `<b>FBO: ${this.state.modalFBO}</b><br>`
          + `<b>Sent to: ${fboRec}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({ modalSubmitted: true });
      } else {
        this.setState({ modalSubmitted: false, modalSubmitting: false, modalFailedSubmit: true });
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({ modalName: event.target.value });
  };

  handleModalPhoneChange = (event) => {
    this.setState({ modalPhone: event.target.value });
  };

  handleModalEmailChange = (event) => {
    this.setState({ modalEmail: event.target.value });
  };

  handleModalCommentsChange = (event) => {
    this.setState({ modalComments: event.target.value });
  };

  handleModalDepDateTimeChange = event => {
    this.setState({ modalDepDateTime: event.target.value });
  };

  handleModalArrDateTimeChange = event => {
    this.setState({ modalArrDateTime: event.target.value });
  };

  handleModalTailNumberChange = event => {
    this.setState({ modalTailNumber: event.target.value });
  };

  handleModalFBOChange = event => {
    this.setState({ modalFBO: event.target.value });
  };

  handleOnClickArrow = () => {
    this.setState({ modalOpen: true });
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false
    });
    this.setState({ modalOpen: !this.state.modalOpen });
  };

  handleOnClickArrowComments = () => {
    this.setState({ commentsModalOpen: true });
  };

  handleCommentsModalClose = () => {
    this.resetModalForm();
    this.setState({
      submitted: false,
      submitting: false,
      failedSubmit: false
    });
    this.setState({ commentsModalOpen: !this.state.commentsModalOpen });
  };

  render() {
    let locations = this.state.locations;
    let color = '#0b559c';

    return (
      this.props.page.component && this.props.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'air'}>
            <Head>
              <title>TAC Air - Locations</title>
            </Head>
            <section className="container location">
              <h1>{this.state.page.title}</h1>
              <SVG src="../static/images/tac-air-map.svg" />
            </section>
            <section className='container' style={{ padding: '75px 0px' }}>
              <Grid container spacing={8}>
                {
                  !!locations && Object.keys(locations).map((key, index) =>
                    <Grid key={index} item xs={12} sm={6} md={4} style={{ padding: 20 }}>
                      <a href={`/tac-air/${locations[key].slug}`}>
                        <div className="location-card">
                          <h3>{locations[key].title}</h3>
                          {`${locations[key].slug}` !== 'dalg' ?
                            <div className="location-card-list">
                              <div className="location-card-list-item">
                                <div className="location-card-icon">
                                  <img src="https://cosmic-s3.imgix.net/a3a87a50-891b-11e8-9785-f78c5808beb9-aeroplane.svg" height="25px" />
                                </div>
                                <div>{locations[key].metadata.location.airport}</div>
                              </div>
                              <div className="location-card-list-item">
                                <div className="location-card-icon">
                                  <img src="https://cosmic-s3.imgix.net/c47f2f10-8918-11e8-96d5-43db6338ad71-placeholder.svg" height="25px" />
                                </div>
                                <div dangerouslySetInnerHTML={{ __html: locations[key].metadata.location.address }} />
                              </div>
                              <div className="location-card-list-item">
                                <div className="location-card-icon">
                                  <img src="https://cosmic-s3.imgix.net/e6652cc0-891c-11e8-96d5-43db6338ad71-phone-call.svg" height="25px" />
                                </div>
                                <div>
                                  <a className="location-card-link"
                                    href={`tel:${locations[key].metadata.location.phone_number}`}>{locations[key].metadata.location.phone_number}</a>
                                </div>
                              </div>
                              <div style={{ display: 'flex', alignItems: 'center', textDecoration: 'none', justifyContent: 'flex-end', marginRight: 10 }}>
                                <p style={{ padding: '10px 5px', textTransform: 'uppercase', fontSize: 12, color: color ? color : 'white', fontWeight: 600 }}>View Location</p>
                                <svg style={{ paddingTop: 5, paddingLeft: 5 }} width="0.597in" height="0.306in">
                                  <path
                                    fill={color ? color : 'white'}
                                    d="M1.151,11.511 L40.649,11.511 L31.604,20.568 C31.349,20.824 31.349,21.242 31.604,21.498 C31.860,21.753 32.277,21.753 32.532,21.498 L42.701,11.320 C42.704,11.314 42.710,11.307 42.717,11.300 C42.730,11.286 42.743,11.272 42.753,11.255 C42.762,11.246 42.772,11.232 42.778,11.223 C42.788,11.206 42.798,11.192 42.808,11.176 C42.814,11.163 42.821,11.148 42.826,11.135 C42.833,11.125 42.837,11.116 42.844,11.106 C42.844,11.099 42.846,11.093 42.846,11.088 C42.853,11.073 42.856,11.059 42.862,11.045 C42.865,11.028 42.872,11.012 42.876,10.996 C42.878,10.981 42.882,10.967 42.882,10.951 C42.885,10.934 42.889,10.918 42.889,10.902 C42.892,10.886 42.892,10.870 42.892,10.853 C42.892,10.837 42.892,10.823 42.889,10.806 C42.889,10.790 42.885,10.772 42.882,10.756 C42.882,10.743 42.878,10.727 42.876,10.712 C42.872,10.695 42.865,10.678 42.862,10.663 C42.856,10.647 42.853,10.633 42.849,10.620 C42.846,10.614 42.844,10.607 42.844,10.602 C42.837,10.591 42.833,10.581 42.826,10.571 C42.821,10.559 42.814,10.546 42.808,10.533 C42.798,10.517 42.788,10.501 42.778,10.487 C42.772,10.475 42.762,10.463 42.753,10.452 C42.743,10.437 42.730,10.423 42.717,10.410 C42.710,10.403 42.707,10.396 42.701,10.388 L32.532,0.209 C32.406,0.083 32.238,0.018 32.070,0.018 C31.902,0.018 31.733,0.083 31.604,0.209 C31.349,0.467 31.349,0.883 31.604,1.139 L31.604,1.141 L40.649,10.196 L1.151,10.196 C0.789,10.196 0.495,10.491 0.495,10.853 C0.495,11.216 0.789,11.511 1.151,11.511 Z" />
                                </svg>
                              </div>
                            </div> :
                            <div style={{ height: 223 }}>
                              <h2 style={{
                                textTransform: 'uppercase',
                                textAlign: 'center',
                                fontWeight: 400
                              }}>Go Live August  <br /> 1st 2019</h2>
                            </div>
                          }

                        </div>
                      </a>
                    </Grid>
                  )
                }
              </Grid>
            </section>
            <AirCTA
              handleOnClickArrow={this.handleOnClickArrow.bind(this)}
              handleOnClickArrowComments={this.handleOnClickArrowComments.bind(this)}
            />
            <section>
              <RequestModal
                modalOpen={this.state.modalOpen}
                onModalClose={this.handleModalClose.bind(this)}
                emailValue={this.state.modalEmail}
                handleSubmit={this.handleModalSubmit.bind(this)}
                onEmailChange={this.handleModalEmailChange.bind(this)}
                NameValue={this.state.modalName}
                onNameChange={this.handleModalNameChange.bind(this)}
                phoneValue={this.state.modalPhone}
                onPhoneChange={this.handleModalPhoneChange.bind(this)}
                commentsValue={this.state.modalComments}
                onCommentsChange={this.handleModalCommentsChange.bind(this)}
                depDateValue={this.state.modalDepDateTime}
                onDepDateChange={this.handleModalDepDateTimeChange.bind(this)}
                arrDateValue={this.state.modalArrDateTime}
                onArrDateChange={this.handleModalArrDateTimeChange.bind(this)}
                tailNumberValue={this.state.modalTailNumber}
                onTailNumberChange={this.handleModalTailNumberChange.bind(this)}
                fboValue={this.state.modalFBO}
                onFboChange={this.handleModalFBOChange.bind(this)}
                submitted={this.state.modalSubmitted}
                submitting={this.state.modalSubmitting}
                failedToSumbit={this.state.modalFailedSubmit}
              />
              <ContactCTA
                modalOpen={this.state.commentsModalOpen}
                onModalClose={this.handleCommentsModalClose.bind(this)}
                emailValue={this.state.email}
                handleSubmit={this.handleSubmit.bind(this)}
                onEmailChange={this.handleEmailChange.bind(this)}
                nameValue={this.state.name}
                onNameChange={this.handleNameChange.bind(this)}
                phoneValue={this.state.phone}
                onPhoneChange={this.handlePhoneChange.bind(this)}
                messageValue={this.state.message}
                onMessageChange={this.handleMessageChange.bind(this)}
                companyValue={this.state.company}
                onCompanyChange={this.handleCompanyChange.bind(this)}
                titleValue={this.state.title}
                onTitleChange={this.handleTitleChange.bind(this)}
                tailNumberValue={this.state.tailNumber}
                onTailNumberChange={this.handleTailNumberChange.bind(this)}
                fboValue={this.state.selectFBO}
                onFBOChange={this.handleFBOChange.bind(this)}
                submitted={this.state.submitted}
                submitting={this.state.submitting}
                failedToSumbit={this.state.failedSubmit}
              />
            </section>
          </article>
        )
    );
  }
}