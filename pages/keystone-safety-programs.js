import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import { mapPage } from "../utils/helper";
import ContactCTA from '../components/views/contact-cta';
import Head from 'next/head';
import Post from '../utils/post';

export default class KeystoneSafetyProgramsPage extends React.Component {

  static async getInitialProps({req, query}) {
    let page;

    try {
      const pageResponse = await Request.getObject('safety-programs', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }

    return { page }
  }

  constructor(props){
    super(props);
    this.state = {
      page: props.page,
      cta: props.page.cta,
      about: props.page.about,
      name: "",
      email: "",
      phone: "",
      message: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    }
  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({submitting: true});
    event.preventDefault();

    fetch('/api/tac-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
      })
    }).then((res)=>{
      if (res.status === 200){
        let title = 'Keystone Aviation Contact - Safety';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({submitted: true});
      }else {
        this.setState({submitted: false, submitting: false, failedSumbit: true});
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({name: event.target.value});
  };

  handleEmailChange = (event) => {
    this.setState({email: event.target.value});
  };

  handlePhoneChange = (event) => {
    this.setState({phone: event.target.value});
  };

  handleMessageChange = (event) => {
    this.setState({message: event.target.value});
  };

  render() {
    let sectionHead=this.state.about.children;
    let sectionHeadBackground = sectionHead[2].imgix_url + '?w=2000';
    let sectionHeadTitle = sectionHead[0].value;
    let sectionHeadExc = sectionHead[1].value;
    let float = 'right';
    let bgColor = 'white';

    let cardColor =
      bgColor === 'blue' ? 'rgba(0, 83, 160, 0.96)' :
        bgColor === 'red' ? 'rgba(226, 56, 63, 0.96)' : 'rgba(255, 255, 255, 0.96)';

    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
        <article className={'keystone'}>
          <Head>
            <title>{`Keystone Aviation - Safety Programs`}</title>
          </Head>
          <section style={{
            position: 'relative',
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
            height: '100%',
            justifyContent: float === 'right' ? 'flex-start' : 'flex-end'
          }}>
            <div
              style={{background: cardColor}}
              className={float === 'right' ? 'banner-card right' : 'banner-card'}
            >
              <div className="banner-card-inner">
                <h1 style={{
                  color:  bgColor === 'blue' ? 'white' : bgColor === 'red' ? 'white' : '#4c4b4b'
                }}>{sectionHeadTitle}</h1>
                <div style={{
                  color: bgColor === 'blue' ? '#f3f3f3' : bgColor === 'red' ? '#f3f3f3' : '#626262',
                }}>
                  {sectionHeadExc}
                </div>
              </div>
            </div>
            <div className="sectionHeadImg" style={{
              background: `url(${sectionHeadBackground})`,
              backgroundSize:'cover',
              backgroundPosition:'center',
              height: '70vh'
            }}/>
          </section>
          <div className={'container'}>
            <section style={{
              position: 'relative',
              width: '100%',
              display: 'flex',
              flexWrap: 'wrap',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'flex-start',
              paddingTop: 150
            }}>
              <div className="subpageImg" style={{
                background: `url(${this.state.page.wyvern.children[1].url ? this.state.page.wyvern.children[1].url : 'https://www.wyvernltd.com/hubfs/Wyvern_Logos/wingman_logo-460x.png?t=1533240261608'})`,
                backgroundSize:'contain',
                backgroundPosition:'center',
                backgroundRepeat: 'no-repeat',

              }}/>
              <div
                style={{
                  background: 'rgba(255, 255, 255, 0.77)',
                  minHeight: 'inherit',
                  top: 'inherit'
                }}
                className={'subpage-card subpage-right'}
              >
                <div className="subpage-inner">
                  <div>
                    <div dangerouslySetInnerHTML={{ __html: this.state.page.wyvern.children[0].value}}/>
                  </div>
                </div>
              </div>
            </section>
            <section style={{
              position: 'relative',
              width: '100%',
              display: 'flex',
              flexWrap: 'wrap',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
              <div className="subpageImg" style={{
                background: `url(${this.state.page.acsf.children[1].url ? this.state.page.argus.children[1].url : 'https://r3.aviationpros.com/files/base/image/CAVC/2012/10/16x9/640x360/acsf-registered-web_10813509.jpg'})`,
                backgroundSize:'contain',
                backgroundPosition:'center',
                backgroundRepeat: 'no-repeat',
              }}/>
              <div
                style={{
                  background: 'rgba(255, 255, 255, 0.80)',
                }}
                className='subpage-card'
              >
                <div className="subpage-inner">
                  <div>
                    <div dangerouslySetInnerHTML={{ __html: this.state.page.argus.children[0].value }}/>
                  </div>
                </div>
              </div>
            </section>
            <section style={{
              position: 'relative',
              width: '100%',
              display: 'flex',
              flexWrap: 'wrap',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'flex-start'
            }}>
              <div className="subpageImg" style={{
                background: `url(${this.state.page.nata.children[1].url ? this.state.page.nata.children[1].url : 'https://www.wyvernltd.com/hubfs/Wyvern_Logos/wingman_logo-460x.png?t=1533240261608'})`,
                backgroundSize:'contain',
                backgroundPosition:'center',
                backgroundRepeat: 'no-repeat',

              }}/>
              <div
                style={{
                  background: 'rgba(255, 255, 255, 0.77)',
                }}
                className={'subpage-card subpage-right'}
              >
                <div className="subpage-inner">
                  <div>
                    <div dangerouslySetInnerHTML={{ __html: this.state.page.nata.children[0].value}}/>
                  </div>
                </div>
              </div>
            </section>
            <section style={{
              position: 'relative',
              width: '100%',
              display: 'flex',
              flexWrap: 'wrap',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
              <div className="subpageImg" style={{
                background: `url(${this.state.page.acsf.children[1].url ? this.state.page.acsf.children[1].url : 'https://r3.aviationpros.com/files/base/image/CAVC/2012/10/16x9/640x360/acsf-registered-web_10813509.jpg'})`,
                backgroundSize:'contain',
                backgroundPosition:'center',
                backgroundRepeat: 'no-repeat',
              }}/>
              <div
                style={{
                  background: 'rgba(255, 255, 255, 0.80)',
                  minHeight: 'inherit',
                  top: 'inherit'
                }}
                className='subpage-card'
              >
                <div className="subpage-inner">
                  <div>
                    <div dangerouslySetInnerHTML={{ __html: this.state.page.acsf.children[0].value }}/>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <ContactCTA
            cta={this.state.cta}
            emailValue={this.state.email}
            handleSubmit={this.handleSubmit.bind(this)}
            onEmailChange={this.handleEmailChange.bind(this)}
            nameValue={this.state.name}
            onNameChange={this.handleNameChange.bind(this)}
            phoneValue={this.state.phone}
            onPhoneChange={this.handlePhoneChange.bind(this)}
            messageValue={this.state.message}
            onMessageChange={this.handleMessageChange.bind(this)}
            bgColor='#8b8b8b'
            btnColor='#e2383f'
            submitted={this.state.submitted}
            submitting={this.state.submitting}
            failedToSumbit={this.state.failedSubmit}
          />
        </article>
      )
    );
  }
}