import React from 'react'
import PageNotFound from '../components/views/404'
import Request from '../utils/request';
import CTA from '../components/views/cta';
import ArrowButton from '../components/buttons/ArrowButton';
import RequestQuote from '../components/views/request-quote-modal';
import Head from 'next/head';
import Post from '../utils/post';

export default class dieselExhaustFluidPage extends React.Component {

  static async getInitialProps({req, query}) {
    let page;

    try {
      const pageResponse = await Request.getObject('diesel-exhaust-fluid', query.status, query.revision);
      page = pageResponse.object;
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { page }
  }

  constructor(props){
    super(props);

    this.state = {
      page: props.page,
      cta: props.page.metafields[2] && props.page.metafields[2],
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalPhone: "",
      modalComments: "",
      modalOpen: false,
      modalSubmitting: false,
      modalSubmitted: false,
      modalFailedSubmit: false,
    }
  }

  resetModalForm = () => {
    this.setState({
      modalName: "",
      modalEmail: "",
      modalCompany: "",
      modalPhone: "",
      modalComments: "",
    })
  };

  handleModalSubmit = (event) => {
    this.setState({modalSubmitting: true});
    event.preventDefault();

    fetch('/api/energy-quote-request', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.modalEmail,
        name: this.state.modalName,
        phone: this.state.modalPhone,
        company: this.state.modalCompany,
        message: this.state.modalComments
      })
    }).then((res)=>{
      if (res.status === 200){
        let title = 'Energy Quote Request - DEF';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.modalName}</b><br>`
          + `<b>Message: ${this.state.modalComments}</b><br><br><br>`
          + `<b>Email: ${this.state.modalEmail}</b><br>`
          + `<b>Phone: ${this.state.modalPhone}</b><br>`
          + `<b>Company: ${this.state.modalCompany}</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetModalForm();
        this.setState({modalSubmitted: true});
      }else {
        this.setState({modalSubmitted: false, modalSubmitting: false, modalFailedSubmit: true});
      }
    })
  };

  handleModalNameChange = (event) => {
    this.setState({modalName: event.target.value});
  };

  handleModalPhoneChange = (event) => {
    this.setState({modalPhone: event.target.value});
  };

  handleModalEmailChange = (event) => {
    this.setState({modalEmail: event.target.value});
  };

  handleModalCompanyChange = (event) => {
    this.setState({modalCompany: event.target.value});
  };

  handleModalCommentsChange = (event) => {
    this.setState({modalComments: event.target.value});
  };

  handleOnClickArrow = () => {
    this.setState({modalOpen: true});
  };

  handleModalClose = () => {
    this.resetModalForm();
    this.setState({
      modalSubmitted: false,
      modalSubmitting: false,
      modalFailedSubmit: false
    });
    this.setState({modalOpen: !this.state.modalOpen});
  };

  render() {
    return (
      this.props.page.component && this.props.page.component === '404' ? (
        <PageNotFound />
      ) : (
        <article>
          <Head>
            <title>{`TAC Energy - Diesel Exhaust Fluid`}</title>
          </Head>
          <section style={{
            position: 'relative',
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
            height: '100%',
            alignItems: 'center',
            justifyContent: 'flex-end'
          }}>
            <div className="subpageImg" style={{
              background: `url(${this.state.page.metafields[0].imgix_url ? this.state.page.metafields[0].imgix_url + '?w=2000' : 'https://cosmic-s3.imgix.net/3790ec30-7be2-11e8-8a7c-ed8ed617634d-1522.jpg'})`,
              backgroundSize:'cover',
              backgroundPosition:'center',
            }}/>
            <div
              style={{
                background: 'rgba(255, 255, 255, 0.80)',
              }}
              className='subpage-card'
            >
              <div className="subpage-inner">
                <div>
                  <img src={`${this.state.page.metadata.icon.url}`} height="75px"/>
                  <h1>{ this.state.page.title }</h1>
                  <div dangerouslySetInnerHTML={{ __html: this.state.page.content }}/>
                  <a onClick={this.handleOnClickArrow.bind(this)}>
                    <ArrowButton color='#e2383f' phrase='Request a Quote'/>
                  </a>
                </div>
              </div>
            </div>
          </section>
          <section style={{
            position: 'relative',
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
            height: '100%',
            alignItems: 'center',
            justifyContent: 'flex-start'
          }}>
            <div className="subpageImg" style={{
              background: `url(${this.state.page.metadata.section.image.imgix_url ? this.state.page.metadata.section.image.imgix_url + '?w=2000' : 'https://cosmic-s3.imgix.net/3790ec30-7be2-11e8-8a7c-ed8ed617634d-1522.jpg'})`,
              backgroundSize:'cover',
              backgroundPosition:'center',
            }}/>
            <div
              style={{
                background: 'rgba(255, 255, 255, 0.77)',
              }}
              className={'subpage-card subpage-right'}
            >
              <div className="subpage-inner">
                <div>
                  <div dangerouslySetInnerHTML={{ __html: this.state.page.metadata.section.questions }}/>
                </div>
              </div>
            </div>
          </section>
          {this.state.cta && <CTA cta={this.state.cta} logoHeight='50px' bgColor='#8b8b8b' btnColor='#0b559c'/>}
          <RequestQuote
            modalOpen={this.state.modalOpen}
            onModalClose={this.handleModalClose.bind(this)}
            emailValue={this.state.modalEmail}
            handleSubmit={this.handleModalSubmit.bind(this)}
            onEmailChange={this.handleModalEmailChange.bind(this)}
            NameValue={this.state.modalName}
            onNameChange={this.handleModalNameChange.bind(this)}
            phoneValue={this.state.modalPhone}
            onPhoneChange={this.handleModalPhoneChange.bind(this)}
            companyValue={this.state.modalCompany}
            onCompanyChange={this.handleModalCompanyChange.bind(this)}
            commentsValue={this.state.modalComments}
            onCommentsChange={this.handleModalCommentsChange.bind(this)}
            submitted={this.state.modalSubmitted}
            submitting={this.state.modalSubmitting}
            failedToSubmit={this.state.modalFailedSubmit}
          />
        </article>
      )
    );
  }
}