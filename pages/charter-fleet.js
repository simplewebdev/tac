import React from 'react'
import Request from '../utils/request';
import Fleet from '../components/views/fleet';
import Head from 'next/head';

export default class CharterFleetPage extends React.Component {

  static async getInitialProps() {
    let charter;
    let page;

    try {
      const fleet = await Request.getCharterFleet(null, null);
      charter = fleet.objects;
    } catch(e) {
      page = {
        title: 'Page not found',
        component: '404',
      }
    }

    return { charter }
  }

  constructor(props){
    super(props);

    this.state = {
      charter: props.charter,
    }
  }

  render() {
    return (
      <div style={{padding: '60px 0px'}} className="container">
        <Head>
          <title>{`Keystone Aviation - Charter Fleet`}</title>
        </Head>
        <h2 style={{fontWeight: 500}}>THE KEYSTONE AVIATION AIR CHARTER FLEET</h2>
        <Fleet charter={this.state.charter}/>
      </div>
    );
  }
}