import React from 'react';
import PageNotFound from '../components/views/404';
import Request from '../utils/request';
import { mapPage } from "../utils/helper";
import Hero from '../components/views/main-page-hero';
import ContactCTA from '../components/views/contact-cta';
import CardSet from "../components/views/cards";
import Head from 'next/head';
import Post from '../utils/post';

export default class HomePage extends React.Component {

  static async getInitialProps({ req, query }) {
    let page;

    try {
      const pageResponse = await Request.getObject('home', query.status, query.revision);
      page = mapPage(pageResponse.object);
    } catch (e) {
      page = {
        title: 'Page not found',
        component: '404'
      }
    }
    return { page }
  }

  constructor(props) {
    super(props);
    this.state = {
      page: props.page,
      hero: props.page.hero_banner,
      cta: props.page.cta,
      name: "",
      email: "",
      phone: "",
      message: "",
      submitting: false,
      submitted: false,
      failedSubmit: false,
    };

  }

  resetForm = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      message: "",
    })
  };

  handleSubmit = (event) => {
    this.setState({ submitting: true });
    event.preventDefault();

    fetch('/api/tac-contact', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        message: this.state.message,
      })
    }).then((res) => {
      if (res.status === 200) {
        let title = 'TAC Contact Form - Home';
        let html =
          `<div>`
          + `<b>New contact form message from: ${this.state.name}</b><br>`
          + `<b>Message: ${this.state.message}</b><br><br><br>`
          + `<b>Email: ${this.state.email}</b><br>`
          + `<b>Phone: ${this.state.phone}</b><br>`
          + `<b>Sent to: marketing@tacenergy.com</b><br>` +
          `</div>`;

        Post.addFormSubmission(title, html);
        this.resetForm();
        this.setState({ submitted: true });
      } else {
        this.setState({ submitted: false, submitting: false, failedSumbit: true });
      }
    })
  };

  handleNameChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePhoneChange = (event) => {
    this.setState({ phone: event.target.value });
  };

  handleMessageChange = (event) => {
    this.setState({ message: event.target.value });
  };


  render() {
    return (
      this.state.page.component && this.state.page.component === '404' ? (
        <PageNotFound />
      ) : (
          <article className={'home'}>
            <Head>
              <title>TAC - The Arnold Companies</title>
            </Head>
            <Hero hero={this.state.hero} />
            <CardSet cardSet={this.state.page.cards} />
            <ContactCTA
              cta={this.state.cta}
              emailValue={this.state.email}
              handleSubmit={this.handleSubmit.bind(this)}
              onEmailChange={this.handleEmailChange.bind(this)}
              nameValue={this.state.name}
              onNameChange={this.handleNameChange.bind(this)}
              phoneValue={this.state.phone}
              onPhoneChange={this.handlePhoneChange.bind(this)}
              messageValue={this.state.message}
              onMessageChange={this.handleMessageChange.bind(this)}
              submitted={this.state.submitted}
              submitting={this.state.submitting}
              failedToSumbit={this.state.failedSubmit}
            />
          </article>
        )
    );
  }
}