const { createServer } = require('http');
const { parse } = require('url');
const next = require('next');
const express = require('express');
const routes = require('./routes');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const dotenv = require('dotenv');
const fetch = require('node-fetch');
const Twitter = require('twitter');

dotenv.config();

// let transporter = nodemailer.createTransport({
//   host: process.env.SMTP_HOST,
//   port: 587,
//   secure: false, // true for 465, false for other ports
//   auth: {
//     user: process.env.SMTP_USER,
//     pass: process.env.SMTP_PASS
//   }
// });
let transporter = nodemailer.createTransport({
  host: 'smtp.mailgun.org',
  port: 587,
  secure: false, // true for 465, false for other ports
  auth: {
    user: "postmaster@thearnoldcos.com",
    pass: "0498e43cf793fc2dd437893abb333ca7-c1fe131e-f68888d9"
  }
});


const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handler = routes.getRequestHandler(app);
const PORT = process.env.PORT || 3000;

app.prepare().then(() => {
  const server = express();

  server.get('/api/twitter', (req, res) => {
    let client = new Twitter({
      consumer_key: 'uYIdxigniYFIkeBshVohFZ67N',
      consumer_secret: '51slsqSScjYd3CwLqMpLuXHzYKJ5SzU7vzTPtRTaReogXTYS9Y',
      access_token_key: '304621988-LocEDPx6ZkudTHUrUkHSiPz5MbWycbeLtQ7szYUq',
      access_token_secret: '7P4Uq4L9G5ECtrl2ekHQn4WcrVCTeZeHwSzhrfhkD4naN'
    });

    let client3 = new Twitter({
      consumer_key: 'pqn6KtzCPWobDSNtk6Vs0dLi2',
      consumer_secret: 'PPiKI6ZiePi6H1pr7DKJj83sIgZuQt5qw4IC4AivmughIY44HE',
      access_token_key: '183084727-g0TjqM7IcBy0nfui9RGSmCpuqr4yjlJ4fY3P97TA',
      access_token_secret: '1GnWTvccxDP1uudCdWqB6xgg8qzRkf2eXHqRWTZPRExbi'
    });


    let dt1 = [];
    let dt2 = [];
    let tweetdata = [];

    client.get('statuses/user_timeline', {}, (error, tweets, response) => {
      if (!error) {
        dt1 = tweets;
        let client2 = new Twitter({
          consumer_key: 'SjvRE2zGqOR6eubVDn0PICgUw',
          consumer_secret: 'jmVpCmt8TPwsnkWCvD9JjSGbB0B1Mw4woHhQVSQwjqGG28Sq4n',
          access_token_key: '1022125017830711296-KT1ZJwrcFkSZSp7XSzffbnMQAJE3jI',
          access_token_secret: '3a2XPavFcHLQONdMNaWHCwKzZJ6qBZoEpgGoJ0Ki56W5B'
        });
        client2.get('statuses/user_timeline', {}, (error, tweets, response) => {
          if (!error) {
            dt2 = tweets;

            for (let i = 0; i < dt1.length; i++) {
              tweetdata.push(dt1[i]);
            }
            for (let i = 0; i < dt2.length; i++) {
              tweetdata.push(dt2[i]);
            }
            client3.get('statuses/user_timeline', {}, (error, tweets, response) => {
              if (!error) {
                for (let i = 0; i < tweets.length; i++) {
                  tweetdata.push(tweets[i]);
                }
                res.json({ tweets: tweetdata });
              }
            });



          } else {
            res.json({ msg: error });
          }
        });

        //res.json({ tweets: tweets });
      } else {
        res.json({ msg: error });
      }
    });

  });
  server.get('/_next/*', (req, res) => handler(req, res));
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({ extended: true }));
  transporter.verify((error, success) => {
    if (error) {
      console.log(error);
    } else {
      console.log('Mail server is ready to send messages');
    }
  });
  server.post('/api/tac-contact', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      // to: process.env.TAC_EMAIL_RECIPIENT,
      to: "marketing@tacenergy.com",
      subject: 'TAC - Contact Form Message',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message}`,
      html:
        `<div>`
        + `<b>New contact form message from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>` +
        `</div>`
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });
  server.post('/api/sales-brokerage-contact', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      // to: process.env.TAC_KEYSTONE_SB_EMAIL_RECIPIENT,
      to: "bmiller@keystoneaviation.com, dchamberlain@keystoneaviation.com",
      subject: 'TAC - Contact Form Message',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message}`,
      html:
        `<div>`
        + `<b>New contact form message from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/keystone-contact', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      // to: process.env.TAC_KEYSTONE_EMAIL_RECIPIENT,
      to: "dchamberlain@keystoneaviation.com",
      subject: 'TAC - Contact Form Message',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message}`,
      html:
        `<div>`
        + `<b>New contact form message from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/tac-air-contact', (req, res) => {

    let fbo = req.body.fbo;
    let fboRec;

    switch (fbo) {
      case 'ama':
        //fboRec = process.env.TACAIR_AMA_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_AMA@tacair.com";
        break;
      case 'apa':
        //fboRec = process.env.TACAIR_APA_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_APA@tacair.com";
        break;
      case 'bdl':
        //fboRec = process.env.TACAIR_BDL_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_BDL@tacair.com";
        break;
      case 'dal':
        //fboRec = process.env.TACAIR_DAL_EMAIL_RECIPIENT;
        //fboRec = "jgibney@tacair.com";
        fboRec = "Dist_WebComments_Kdal@tacenergy.com";
        break;
      case 'fsm':
        //fboRec = process.env.TACAIR_FSM_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_FSM@tacair.com";
        break;
      case 'lex':
        //fboRec = process.env.TACAIR_LEX_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_LEX@tacair.com";
        break;
      case 'lit':
        //fboRec = process.env.TACAIR_LIT_EMAIL_RECIPIENT;
        fboRec = " Dist_WebComments_LIT@tacair.com";
        break;
      case 'oma':
        //fboRec = process.env.TACAIR_OMA_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_OMA@tacair.com";
        break;
      case 'pvu':
        //fboRec = process.env.TACAIR_PVU_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_PVU@tacair.com";
        break;
      case 'rdu':
        //fboRec = process.env.TACAIR_RDU_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_RDU@tacair.com";
        break;
      case 'shv':
        //fboRec = process.env.TACAIR_SHV_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_SHV@tacair.com";
        break;
      case 'slc':
        //fboRec = process.env.TACAIR_SLC_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_SLC@tacair.com";
        break;
      case 'sus':
        //fboRec = process.env.TACAIR_SUS_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_SUS@tacair.com";
        break;
      case 'txk':
        //fboRec = process.env.TACAIR_TXK_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_TXK@tacair.com";
        break;
      case 'tys':
        //fboRec = process.env.TACAIR_TYS_EMAIL_RECIPIENT;
        fboRec = "Dist_WebComments_TYS@tacair.com";
        break;
      default:
        //fboRec = process.env.TACAIR_EMAIL_RECIPIENT;
        fboRec = "marketing@tacenergy.com";

    }

    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      to: fboRec,
      subject: 'TAC Air - Contact Form Message',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message},
         company: ${req.body.company},
         title: ${req.body.title}, 
         tail Number: ${req.body.tail_number}, 
         FBO: ${req.body.fbo}`,
      html:
        `<div>`
        + `<b>New contact form message from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>`
        + `<b>Company: ${req.body.company}</b><br>`
        + `<b>Title: ${req.body.title}</b><br>`
        + `<b>Tail Number: ${req.body.tail_number}</b><br>`
        + `<b>FBO: ${req.body.fbo}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/fbo-service-request', (req, res) => {

    let fbo = req.body.fbo;
    let fboRec;

    switch (fbo) {
      case 'ama':
        fboRec = 'amafbo@tacair.com';
        break;
      case 'apa':
        fboRec = 'apafbo@tacair.com';
        break;
      case 'bdl':
        fboRec = 'bdlfbo@tacair.com';
        break;
      case 'dal':
        //fboRec = 'jgibney@tacair.com';
        fboRec = 'DIST_FBO_DAL@tacenergy.com';
        break;
      case 'fsm':
        fboRec = 'fsmfbo@tacair.com';
        break;
      case 'lex':
        fboRec = 'lexfbo@tacair.com';
        break;
      case 'lit':
        fboRec = 'litfbo@tacair.com';
        break;
      case 'oma':
        fboRec = 'omafbo@tacair.com';
        break;
      case 'pvu':
        fboRec = 'pvufbo@tacair.com';
        break;
      case 'rdu':
        fboRec = 'rdufbo@tacair.com';
        break;
      case 'shv':
        fboRec = 'shvfbo@tacair.com';
        break;
      case 'slc':
        fboRec = 'slcfbo@tacair.com';
        break;
      case 'sus':
        fboRec = 'susfbo@tacair.com';
        break;
      case 'txk':
        fboRec = 'txkfbo@tacair.com';
        break;
      case 'tys':
        fboRec = 'tysfbo@tacair.com';
        break;
      default:
        //fboRec = process.env.TACAIR_EMAIL_RECIPIENT;
        fboRec = "marketing@tacenergy.com";
    }

    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      to: fboRec,
      subject: 'TAC Air FBO - Contact Form Message',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message},
         Departure Date/Time: ${req.body.departure_date_time},
         Arrival Date/Time: ${req.body.arrival_date_time},
         Tail Number: ${req.body.tail_number}, 
         FBO: ${req.body.fbo}
         SENT TO: ${fboRec}`,
      html:
        `<div>`
        + `<b>New FBO service request from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>`
        + `<b>Arrival Date/Time: ${req.body.arrival_date_time}</b><br>`
        + `<b>Departure Date/Time: ${req.body.departure_date_time}</b><br>`
        + `<b>Tail Number: ${req.body.tail_number}</b><br>`
        + `<b>FBO: ${req.body.fbo}</b><br>`
        + `<b>SENT TO: ${fboRec}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/tac-investments-contact', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      // to: process.env.TACINVESTMENTS_EMAIL_RECIPIENT,
      to: "marketing@tacenergy.com",
      subject: 'TAC Investments - Contact Form Message',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message}`,
      html:
        `<div>`
        + `<b>New contact form message from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/energy-quote-request', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      //to: process.env.TACENERGY_EMAIL_RECIPIENT,
      to: "marketing@tacenergy.com, sales@tacenergy.com",
      subject: 'Request A Quote',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         company: ${req.body.company},
         State: ${req.body.states},
         message: ${req.body.message}`,
      html:
        `<div>`
        + `<b>New Quote request from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Company: ${req.body.company}</b><br>`
        + `<b>State: ${req.body.states}</b><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/sales-rep-contact', (req, res) => {

    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      to: req.body.rep,
      subject: 'Aircraft Contact Form',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         company: ${req.body.company},
         message: ${req.body.message}`,
      html:
        `<div>`
        + `<b>New Quote request from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Company: ${req.body.company}</b><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/keystone-emptylegs-request', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      //to: process.env.KEYSTONE_EMPTYLEGS_EMAIL_RECIPIENT,
      to: "marketing@tacenergy.com, ops@keystoneaviation.com, dchamberlain@keystoneaviation.com",
      subject: 'Keystone Aviation - Emptylegs Quote Request',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message}`,
      html:
        `<div>`
        + `<b>New Quote request from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/keystone-emptylegs-request-receive-email', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      //to: process.env.KEYSTONE_EMPTYLEGS_EMAIL_RECIPIENT,
      to: "dchamberlain@keystoneaviation.com",
      subject: 'Keystone Aviation - Emptylegs Receive Email Update',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}`,
      html:
        `<div>`
        + `<b>New Receive Email Update from: ${req.body.name}</b><br><br>`
        + `<b>Name: ${req.body.name}</b><br>`
        + `<b>Email: ${req.body.email}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/keystone-management-request', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      //to: process.env.KEYSTONE_MANAGEMENT_EMAIL_RECIPIENT,
      to: "marketing@tacenergy.com, btanis@keystoneaviation.com, cwalker@keystoneaviation.com, jray@keystoneaviation.com, bhoddenbach@keystoneaviation.com, crobertson@keystoneaviation.com, ssobczak@keystoneaviation.com, ARichins@tacenergy.com",

      subject: 'Keystone Aviation - Management Quote Request',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message}`,
      html:
        `<div>`
        + `<b>New Quote request from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/keystone-maintenance-request', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      //to: process.env.KEYSTONE_MAINTENANCE_EMAIL_RECIPIENT,
      to: "marketing@tacenergy.com, btanis@keystoneaviation.com, cwalker@keystoneaviation.com, jray@keystoneaviation.com, bhoddenbach@keystoneaviation.com, crobertson@keystoneaviation.com, ssobczak@keystoneaviation.com, ARichins@tacenergy.com",
      subject: 'Keystone Aviation - Maintenance Quote Request',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message},
         Aircraft Make: ${req.body.aircraftMake},
         Aircraft Model: ${req.body.aircraftModel},
         Tail Number: ${req.body.tailNumber},
         Address: ${req.body.address},
         Zipcode: ${req.body.zipcode},
         City: ${req.body.city}`,
      html:
        `<div>`
        + `<b>New Quote request from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>`
        + `<b>Aircraft Make: ${req.body.aircraftMake}</b><br>`
        + `<b>Aircraft Model: ${req.body.aircraftModel}</b><br>`
        + `<b>Tail Number: ${req.body.tailNumber}</b><br>`
        + `<b>Address: ${req.body.address}</b><br>`
        + `<b>Zipcode: ${req.body.zipcode}</b><br>`
        + `<b>City: ${req.body.city}</b><br>` +
        `</div>`
    };


    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });

  server.post('/api/keystone-charter-request', (req, res) => {
    let mailOptions = {
      from: `${req.body.name} <${req.body.email}>`,
      //to: process.env.KEYSTONE_CHARTER_EMAIL_RECIPIENT,
      to: "marketing@tacenergy.com, ops@keystoneaviation.com, dchamberlain@keystoneaviation.com",
      subject: 'Keystone Aviation - Charter Service Quote Request',
      text:
        `Name: ${req.body.name},
         Email: ${req.body.email}, 
         phone: ${req.body.phone}, 
         message: ${req.body.message},
         Departure City: ${req.body.departure_city},
         Departure Date: ${req.body.departure_date},
         Arrival City: ${req.body.arrival_city},
         Arrival Date: ${req.body.arrival_date},
         Prefered Aircraft: ${req.body.pref_aircraft},
         Number of Passengers: ${req.body.num_of_pass},
         Address: ${req.body.address},
         Zipcode: ${req.body.zipcode},
         City: ${req.body.city}`,
      html:
        `<div>`
        + `<b>New Quote request from: ${req.body.name}</b><br>`
        + `<b>Message: ${req.body.message}</b><br><br><br>`
        + `<b>Email: ${req.body.email}</b><br>`
        + `<b>Phone: ${req.body.phone}</b><br>`
        + `<b>Departure City: ${req.body.departure_city}</b><br>`
        + `<b>Departure Date: ${req.body.departure_date}</b><br>`
        + `<b>Arrival City: ${req.body.arrival_city}</b><br>`
        + `<b>Arrival Date: ${req.body.arrival_date}</b><br>`
        + `<b>Prefered Aircraft: ${req.body.pref_aircraft}</b><br>`
        + `<b>Number of Passengers: ${req.body.num_of_pass}</b><br>`
        + `<b>Address: ${req.body.address}</b><br>`
        + `<b>Zipcode: ${req.body.zipcode}</b><br>`
        + `<b>City: ${req.body.city}</b><br>` +
        `</div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    });
  });


  server.get('/api/fetch_linked_in_data_tac_energy', (req, res) => {
    fetch('https://api.linkedin.com/v2/organizations/2626125/updates?format=json',
      {
        method: 'GET',
        mode: "cors",
        credentials: 'include',
        headers: {
          'Authorization': 'Bearer AQV8DnRkhwK1nBfMNvB1I57j9LtLxkM57RstKmKzQXVc9YQaMjOUMPqT5c59iKlWD-jTOFq7xRegipcL1yKugTSk_rbBJ1WAJDzwusurDXc0TXfwEFa8FEIXIj9FlCvVOvNEKff2rqKaYYVb6xHBhnbvrpJp2ef3-NqwWMOvwrahWd5sZp-8hKBaxN8TNukg5Y7FlLLVWQXfcNsJS7a8lHehWYqtT4OmQB5sj55GEH35icLvYjqdxdKxndRCI-xPZUOqK2mtLQQSM4tsA8K7Uisi1W7zSFCzrZY-xDiDGELy6OZ7dQP7iCA3FtRha-fZjzKgFUIGyzUPIOtLk5wXw2gnQlci6w',
          'Content-Type': 'application/json'
        }
      }
    ).then(response => response.json())
      .then(results => { res.json(results) });

  });
  server.get('/api/fetch_linked_in_data_tac_energy', (req, res) => {
    fetch('https://api.linkedin.com/v2/companies/2626125/updates?format=json',
      {
        method: 'GET',
        mode: "cors",
        credentials: 'include',
        headers: {
          'Authorization': 'Bearer AQV8DnRkhwK1nBfMNvB1I57j9LtLxkM57RstKmKzQXVc9YQaMjOUMPqT5c59iKlWD-jTOFq7xRegipcL1yKugTSk_rbBJ1WAJDzwusurDXc0TXfwEFa8FEIXIj9FlCvVOvNEKff2rqKaYYVb6xHBhnbvrpJp2ef3-NqwWMOvwrahWd5sZp-8hKBaxN8TNukg5Y7FlLLVWQXfcNsJS7a8lHehWYqtT4OmQB5sj55GEH35icLvYjqdxdKxndRCI-xPZUOqK2mtLQQSM4tsA8K7Uisi1W7zSFCzrZY-xDiDGELy6OZ7dQP7iCA3FtRha-fZjzKgFUIGyzUPIOtLk5wXw2gnQlci6w',
          'Content-Type': 'application/json'
        }
      }
    ).then(response => response.json())
      .then(results => { res.json(results) });

  });
  server.get('/api/fetch_linked_in_data_keystone', (req, res) => {
    fetch('https://api.linkedin.com/v2/companies/3008706/updates?format=json',
      {
        method: 'GET',
        mode: "cors",
        credentials: 'include',
        headers: {
          'Authorization': 'Bearer AQV8DnRkhwK1nBfMNvB1I57j9LtLxkM57RstKmKzQXVc9YQaMjOUMPqT5c59iKlWD-jTOFq7xRegipcL1yKugTSk_rbBJ1WAJDzwusurDXc0TXfwEFa8FEIXIj9FlCvVOvNEKff2rqKaYYVb6xHBhnbvrpJp2ef3-NqwWMOvwrahWd5sZp-8hKBaxN8TNukg5Y7FlLLVWQXfcNsJS7a8lHehWYqtT4OmQB5sj55GEH35icLvYjqdxdKxndRCI-xPZUOqK2mtLQQSM4tsA8K7Uisi1W7zSFCzrZY-xDiDGELy6OZ7dQP7iCA3FtRha-fZjzKgFUIGyzUPIOtLk5wXw2gnQlci6w',
          'Content-Type': 'application/json'
        }
      }
    ).then(response => response.json())
      .then(results => { res.json(results) });

  });

  server.get('/api/fetch_linked_in_data_tac_air', (req, res) => {
    fetch('https://api.linkedin.com/v2/companies/2624411/updates?format=json',
      {
        method: 'GET',
        mode: "cors",
        credentials: 'include',
        headers: {
          'Authorization': 'Bearer AQV8DnRkhwK1nBfMNvB1I57j9LtLxkM57RstKmKzQXVc9YQaMjOUMPqT5c59iKlWD-jTOFq7xRegipcL1yKugTSk_rbBJ1WAJDzwusurDXc0TXfwEFa8FEIXIj9FlCvVOvNEKff2rqKaYYVb6xHBhnbvrpJp2ef3-NqwWMOvwrahWd5sZp-8hKBaxN8TNukg5Y7FlLLVWQXfcNsJS7a8lHehWYqtT4OmQB5sj55GEH35icLvYjqdxdKxndRCI-xPZUOqK2mtLQQSM4tsA8K7Uisi1W7zSFCzrZY-xDiDGELy6OZ7dQP7iCA3FtRha-fZjzKgFUIGyzUPIOtLk5wXw2gnQlci6w',
          'Content-Type': 'application/json'
        }
      }
    ).then(response => response.json())
      .then(results => { res.json(results) });

  });
  server.get('/api/fetch_linked_in_data_thearnoldcos', (req, res) => {
    fetch('https://api.linkedin.com/v2/companies/315990/updates?format=json',
      {
        method: 'GET',
        mode: "cors",
        credentials: 'include',
        headers: {
          'Authorization': 'Bearer AQV8DnRkhwK1nBfMNvB1I57j9LtLxkM57RstKmKzQXVc9YQaMjOUMPqT5c59iKlWD-jTOFq7xRegipcL1yKugTSk_rbBJ1WAJDzwusurDXc0TXfwEFa8FEIXIj9FlCvVOvNEKff2rqKaYYVb6xHBhnbvrpJp2ef3-NqwWMOvwrahWd5sZp-8hKBaxN8TNukg5Y7FlLLVWQXfcNsJS7a8lHehWYqtT4OmQB5sj55GEH35icLvYjqdxdKxndRCI-xPZUOqK2mtLQQSM4tsA8K7Uisi1W7zSFCzrZY-xDiDGELy6OZ7dQP7iCA3FtRha-fZjzKgFUIGyzUPIOtLk5wXw2gnQlci6w',
          'Content-Type': 'application/json'
        }
      }
    ).then(response => response.json())
      .then(results => { res.json(results) });

  });


  server.post('/api/cc-market-talk-subscribe', (req, res) => {
    fetch(`https://api.constantcontact.com/v2/contacts?action_by=ACTION_BY_OWNER&api_key=${process.env.CC_API_KEY}`, {
      method: 'post',
      headers: {
        'Authorization': `Bearer ${process.env.CC_ACCESS_TOKEN}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(req.body)
    }).then((response) => {
      if (response.status !== 201) {
        res.json({
          msg: 'fail'
        })
      } else {
        res.json({
          msg: 'success'
        })
      }
    })
  });

  server.get('*', (req, res) => handler(req, res));

  server.use(handler).listen(PORT, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${PORT}`)
  })
});
